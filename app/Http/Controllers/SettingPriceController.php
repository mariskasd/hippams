<?php

namespace App\Http\Controllers;

use App\Models\SettingPrice;
use Illuminate\Http\Request;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SettingPriceController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$setting = SettingPrice::select("id", "min_harga", "max_harga", "min_denda", "max_denda")->orderBy('created_at', 'desc')->first();

		return view('settingprice.settingprice', compact(['setting']));
	}

	public function add(Request $request)
	{

		if ($request->id != null) {

			SettingPrice::where('id', $request->id)->update([
				'min_harga' => $request->min_harga,
				'max_harga' => $request->max_harga,
				'min_denda' => $request->min_denda,
				'max_denda' => $request->max_denda
			]);

		} else {

			$setting = new SettingPrice();
			$setting->min_harga = $request->min_harga;
			$setting->max_harga = $request->max_harga;
			$setting->min_denda = $request->min_denda;
			$setting->max_denda = $request->max_denda;

			$setting->save();

		}

		return response()->json(["message" => "success"], 200);
	}

}