<?php

namespace App\Http\Controllers;


use App\Models\Pelanggan;
use App\Models\Komplain;
use App\Models\Feedback;
use Carbon\Carbon;
use Illuminate\Http\Request;


class KomplainController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		$pelanggan = Pelanggan::pluck('id_pelanggan', 'id_pelanggan');

		if (auth()->user()->role == "Admin") {
			$komplain = Komplain::join('pelanggans', 'pelanggans.id_pelanggan', '=', 'komplains.pelanggan_id')
				->leftJoin('feedbacks', 'komplains.id_komplain', '=', 'feedbacks.komplain_id')
				->orderBy('komplains.updated_at', 'DESC')
				->get(['komplains.*', 'pelanggans.nama as pelanggan', 'pelanggans.id_pelanggan', 'feedbacks.*']);

		} else {

			$komplain = Komplain::join('pelanggans', 'pelanggans.id_pelanggan', '=', 'komplains.pelanggan_id')
				->leftJoin('feedbacks', 'komplains.id_komplain', '=', 'feedbacks.komplain_id')
				->where('pelanggans.id_pelanggan', '=', auth()->user()->id_pengguna)
				->orderBy('komplains.updated_at', 'DESC')
				->get(['komplains.*', 'pelanggans.nama as pelanggan', 'pelanggans.id_pelanggan', 'feedbacks.*']);

		}


		return view('komplain.komplain', compact(['pelanggan', 'komplain']));
	}

	public function feedback($id)
	{
		$pelanggan = Pelanggan::pluck('id_pelanggan', 'id_pelanggan');

		$komplain = Komplain::where('komplains.id_komplain', $id)
			->join('pelanggans', 'pelanggans.id_pelanggan', '=', 'komplains.pelanggan_id')
			->get(['komplains.*', 'pelanggans.nama as pelanggan', 'pelanggans.id_pelanggan'])
			->first();

		$feedback = Feedback::where('komplain_id', $id)->first();

		return view('komplain.feedback', compact(['pelanggan', 'komplain', 'feedback']));
	}

	public function add(Request $request)
	{
		$message = "";

		$filenameSimpan = "";

		$pelSeq = strtotime(Carbon::now());
		$idpel = "K" . $pelSeq . $request->pelanggan_id;

		// $latestPel = Komplain::orderBy('created_at', 'DESC')->count();

		// $pelSeq = str_pad($latestPel + 1, 3, "0", STR_PAD_LEFT);
		// $idpel = "K" . $pelSeq;

		if ($request->has('image')) {
			$filenameWithExt = $request->file('image')->getClientOriginalName();
			$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('image')->getClientOriginalExtension();
			$filenameSimpan = $filename . '_' . time() . '.' . $extension;
			$path = $request->file('image')->storeAs('public/data_meteran', $filenameSimpan);
		} else {
			$filenameSimpan = "";
		}

		$pel = new Komplain();
		$pel->id_komplain = $idpel;
		$pel->pelanggan_id = $request->pelanggan_id;
		$pel->category = $request->category;
		$pel->judul = $request->judul;
		$pel->deskripsi = $request->keterangan;
		$pel->foto = $filenameSimpan;
		$pel->status_komplain = "BARU";

		$pel->save();

		return redirect('/komplain');
	}

	public function updateOnProgress($id)
	{
		Komplain::where('id_komplain', $id)->update([
			'status_komplain' => "SELESAI"
		]);

		return redirect('/komplain');
	}

	public function addFeedback(Request $request)
	{


		$pelSeq = strtotime(Carbon::now());
		$idpel = "F" . $pelSeq;

		Komplain::where('id_komplain', $request->id_komplain)->update([
			'status_komplain' => "DIPROSES"
		]);

		$getKomplain = Komplain::where('id_komplain', $request->id_komplain)->first();

		$pel = new Feedback();
		$pel->id_feedback = $idpel;
		$pel->komplain_id = $request->id_komplain;
		$pel->deskripsi = $request->keterangan;

		if ($getKomplain->category == "Kerusakan") {
			$pel->tanggal_perbaikan = $request->tanggal_perbaikan;
		}
		;

		$pel->save();

		return redirect('/komplain');
	}

	public function editFeedback(Request $request)
	{
		$getKomplain = Komplain::where('id_komplain', $request->id_komplain)->first();

		if ($getKomplain->category == "Kerusakan") {

			Feedback::where('id_feedback', $request->id_feedback)->update([
				'tanggal_perbaikan' => $request->tanggal_perbaikan,
				'deskripsi' => $request->keterangan
			]);

		} else {
			Feedback::where('id_feedback', $request->id_feedback)->update([
				'deskripsi' => $request->keterangan
			]);
		}
		;

		return redirect('/komplain');
	}


}