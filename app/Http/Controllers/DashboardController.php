<?php

namespace App\Http\Controllers;

use App\Models\Meteran;
use App\Models\Komplain;
use App\Models\Pembayaran;
use App\Models\CatatMeter;
use App\Models\Petugas;
use App\Models\RT;
use App\Models\User;
use App\Models\WilayahPetugas;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$collection = new Collection;

		Carbon::setlocale("id");

		//get complain
		$komplain = Komplain::join('pelanggans', 'pelanggans.id_pelanggan', '=', 'komplains.pelanggan_id')
			->where('komplains.status_komplain', '=', 'BARU')
			->orderBy('komplains.updated_at', 'DESC')
			->get(['komplains.*', 'pelanggans.nama as pelanggan', 'pelanggans.id_pelanggan']);

		//get complain feedback
		$feedback = Komplain::join('pelanggans', 'pelanggans.id_pelanggan', '=', 'komplains.pelanggan_id')
			->join('feedbacks', 'feedbacks.komplain_id', '=', 'komplains.id_komplain')
			->where('komplains.pelanggan_id', '=', auth()->user()->id_pengguna)->get();


		//pie chart pemakaian bulan sebelumnya berdasarkan wilayah
		$pie = new Collection;
		$wilayah = RT::select("*")->orderBy('created_at', 'desc')->get();

		$previousMonth = Carbon::now()->subMonth();
		$startDate = $previousMonth->startOfMonth();
		$endDate = $previousMonth->endOfMonth();
		$pieMonth = $previousMonth->translatedFormat('F Y');
		$pieMonths = Carbon::parse($previousMonth)->translatedFormat('m');
		$pieYears = Carbon::parse($previousMonth)->translatedFormat('Y');

		$sumVal = 0;

		// get pie chart Admin pemakaian bulan lalu
		$pie = CatatMeter::join('meterans', 'meterans.id_meteran', '=', 'catat_meters.meteran_id')
			->join('rts', 'rts.id', '=', 'meterans.rt_id')
			->whereMonth('catat_meters.tanggal_pencatatan', $pieMonths)
			->whereYear('catat_meters.tanggal_pencatatan', $pieYears)
			->selectRaw("SUM(catat_meters.total_meteran_terpakai) as value,rts.nama_rt as name")
			->groupBy('rts.id')
			->groupBy('rts.nama_rt')
			->get();


		$yearNow = Carbon::now()->translatedFormat('Y');

		//get bar chart

		$bar = new Collection;
		$startDate;
		$endDate;
		$pemakaianYear = new Collection;

		//get bar chart pemakaian pelanggan (role petugas lap & pelanggan) 
		for ($i = 0; $i < 12; $i++) {

			if ($i == 0) {
				$startDate = Carbon::now()->copy()->startOfYear()->firstOfMonth()->addDay();
				$endDate = Carbon::now()->copy()->startOfYear()->endOfMonth();
			} else {
				$startDate = $startDate->copy()->addMonth()->startOfMonth()->addDay();
				$endDate = $startDate->copy()->endOfMonth();
			}

			$pemData = Pembayaran::where('tanggal_jatuh_tempo', '>=', $startDate)
				->where('tanggal_jatuh_tempo', '<=', $endDate)
				->sum('total_tagihan');

			if (auth()->user()->role == "Pembayaran") {
				$pemData = Pembayaran::where('tanggal_jatuh_tempo', '>=', $startDate)
					->where('tanggal_jatuh_tempo', '<=', $endDate)
					->where('petugas_id', "=", auth()->user()->id_pengguna)
					->sum('total_tagihan');
			}

			$bar->push((object) [
				'value' => $pemData,
				'i' => $i,
				'startDate' => $startDate,
				'endDate' => $endDate,
				'name' => $startDate->translatedFormat('F')
			]);


			$pemakaianPelanggan = 0;

			if (auth()->user()->role == "Lapangan") {
				$pemakaianPelanggan = CatatMeter::join('meterans', 'catat_meters.meteran_id', '=', 'meterans.id_meteran')
					->where('catat_meters.petugas_id', '=', auth()->user()->id_pengguna)
					->whereMonth('catat_meters.tanggal_pencatatan', '=', ($i + 1))
					->whereYear('catat_meters.tanggal_pencatatan', '=', Carbon::now()->format('Y'))
					->sum('catat_meters.total_meteran_terpakai');
			} else if (auth()->user()->role == "Pelanggan") {
				$pemakaianPelanggan = CatatMeter::join('meterans', 'catat_meters.meteran_id', '=', 'meterans.id_meteran')
					->whereMonth('catat_meters.tanggal_pencatatan', '=', ($i + 1))
					->whereYear('catat_meters.tanggal_pencatatan', '=', Carbon::now()->format('Y'))
					->where('meterans.id_pelanggan', '=', auth()->user()->id_pengguna)
					->sum('catat_meters.total_meteran_terpakai');
			}

			$pemakaianYear->push((object) [
				'value' => $pemakaianPelanggan,
				'i' => $i,
				'startDate' => $startDate,
				'endDate' => $endDate,
				'name' => $startDate->translatedFormat('F')
			]);

		}

		//get pembayaran belum lunas pelanggan
		$pembayaranNotDone = Pembayaran::join('catat_meters', 'catat_meters.id', '=', 'pembayarans.catat_meter_id')
			->join('meterans', 'meterans.id_meteran', '=', 'catat_meters.meteran_id')
			->where('meterans.id_pelanggan', '=', auth()->user()->id_pengguna)
			->where('pembayarans.status_bayar', '=', false)->get();

		$pencatatanNotDone = new Collection;

		$monthFilter = Carbon::now()->translatedFormat('m');
		$yearFilter = Carbon::now()->translatedFormat('Y');

		$wilayahPetugas = WilayahPetugas::join("rts", 'rts.id', '=', 'wilayahpetugas.rt_id')->where("petugas_id", "=", auth()->user()->id_pengguna)->get();

		foreach ($wilayahPetugas as $item) {

			if (auth()->user()->role == "Lapangan") {
				$totalMeteran = Meteran::where("rt_id", "=", $item->rt_id)->where("active", "=", 1)->count();

				//get rekap pencatatan belum dicatat/sudah
				$totalCatat = CatatMeter::where('petugas_id', '=', auth()->user()->id_pengguna)
					->join('meterans', 'catat_meters.meteran_id', '=', 'meterans.id_meteran')
					->where('meterans.rt_id', '=', $item->rt_id)
					->whereMonth('tanggal_pencatatan', '=', $monthFilter)
					->whereYear('tanggal_pencatatan', '=', $yearFilter)
					->count();

				$pencatatanNotDone->push((object) [
					'rt' => $item->nama_rt,
					'done' => $totalCatat,
					'notDone' => $totalMeteran - $totalCatat
				]);
			} else {
				$totalMeteran = Meteran::where("rt_id", "=", $item->rt_id)->count();

				//get rekap pembayaran belum dicatat/sudah
				$totalCatatDone = Pembayaran::where('pembayarans.petugas_id', '=', auth()->user()->id_pengguna)
					->join('catat_meters', 'catat_meters.id', '=', 'pembayarans.catat_meter_id')
					->join('meterans', 'catat_meters.meteran_id', '=', 'meterans.id_meteran')
					->where('meterans.rt_id', '=', $item->rt_id)
					->whereMonth('tanggal_jatuh_tempo', '=', $monthFilter)
					->whereYear('tanggal_jatuh_tempo', '=', $yearFilter)
					->where('status_bayar', '=', true)
					->count();

				$totalCatatNotDone = Pembayaran::where('pembayarans.petugas_id', '=', auth()->user()->id_pengguna)
					->join('catat_meters', 'catat_meters.id', '=', 'pembayarans.catat_meter_id')
					->join('meterans', 'catat_meters.meteran_id', '=', 'meterans.id_meteran')
					->where('meterans.rt_id', '=', $item->rt_id)
					->whereMonth('tanggal_jatuh_tempo', '=', $monthFilter)
					->whereYear('tanggal_jatuh_tempo', '=', $yearFilter)
					->where('status_bayar', '=', false)
					->count();

				$pencatatanNotDone->push((object) [
					'rt' => $item->nama_rt,
					'done' => $totalCatatDone,
					'notDone' => $totalCatatNotDone
				]);
			}
		}

		return view('dashboard.dashboardadmin', compact(['yearNow', 'pencatatanNotDone', 'pembayaranNotDone', 'feedback', 'pemakaianYear', 'collection', 'komplain', 'pie', 'pieMonth', 'sumVal', 'bar']));
	}

	public function updatePassUser()
	{
		$meteran = Meteran::where('active', 1)->get();
		$startDate = Carbon::now()->startOfMonth()->subMonth();
		$endDate = Carbon::now()->endOfMonth()->subMonth();
		foreach ($meteran as $item) {
			$totalTerpakai = rand(10, 35);
			$totalMeteran = $totalTerpakai;

			$getMeterBefore = CatatMeter::where('meteran_id', $item->id_meteran)->where('tanggal_pencatatan', '>=', $startDate)
				->where('tanggal_pencatatan', '<=', $endDate)->orderBy('tanggal_pencatatan', 'desc')->first();

			$petugas = Petugas::where('petugas.type', 'Lapangan')->join('wilayahpetugas', 'petugas.id_petugas', 'wilayahpetugas.petugas_id')->where('wilayahpetugas.rt_id', $item->rt_id)->first();

			if ($getMeterBefore != null) {
				$totalMeteran = $getMeterBefore->total_meteran + $totalTerpakai;
			}

			$cat = new CatatMeter();
			$cat->meteran_id = $item->id_meteran;
			$cat->petugas_id = $petugas->id_petugas;
			$cat->tanggal_pencatatan = Carbon::now();
			$cat->keterangan = '-';
			$cat->foto = '3e6689dd-2272-4c2a-922f-aa7f6ac92ea3_1689157069.jpg';
			$cat->total_meteran = (float) $totalMeteran;
			$cat->total_meteran_terpakai = (float) $totalTerpakai;

			$cat->save();

		}



	}
}