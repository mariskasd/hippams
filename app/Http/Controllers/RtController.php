<?php

namespace App\Http\Controllers;

use App\Models\Rw;
use App\Models\Rt;
use App\Models\WilayahPetugas;
use App\Models\Pelanggan;
use App\Models\Meteran;
use Illuminate\Http\Request;

class RtController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$rw = RW::pluck('nama_rw', 'id');

		$rt = RT::join('rws', 'rws.id', '=', 'rts.rw_id')
			->get(['rts.*', 'rws.nama_rw']);

		return view('rt.rt', compact(['rw', 'rt']));
	}

	public function add(Request $request)
	{

		$rt = new RT();
		$rt->nama_rt = $request->name;
		$rt->rw_id = $request->rw;

		$rt->save();

		return response()->json(["message" => "success"], 200);
	}

	public function edit($id, Request $request)
	{
		RT::where('id', $id)->update([
			'nama_rt' => $request->name,
			'rw_id' => $request->rw
		]);

		return response()->json(["message" => "success"], 200);
	}

	public function delete($id)
	{

		$checkMeteran = Meteran::where('rt_id', '=', $id)->first();
		$checkPelanggan = Pelanggan::where('rt_id', '=', $id)->first();
		$checkWilayahPet = WilayahPetugas::where('rt_id', '=', $id)->first();

		$message = "";

		if ($checkMeteran == null && $checkPelanggan == null && $checkWilayahPet == null) {
			RT::where('id', '=', $id)->delete();
			$message = "success";
		} else {
			$message = "Data tidak bisa dihapus karena sedang digunakan pada tabel lain";
		}

		return response()->json(["message" => $message], 200);
	}

}