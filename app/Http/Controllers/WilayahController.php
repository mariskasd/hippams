<?php

namespace App\Http\Controllers;

use App\Models\Wilayah;
use App\Models\Meteran;
use Illuminate\Http\Request;

class WilayahController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$wilayah = Wilayah::select("id", "nama_wilayah", "deskripsi")->orderBy('created_at', 'desc')->get();

		return view('wilayah.wilayah', compact(['wilayah']));
	}

	public function add(Request $request)
	{

		$wilayah = new Wilayah();
		$wilayah->nama_wilayah = $request->name;
		$wilayah->deskripsi = $request->deskripsi;

		$wilayah->save();

		return response()->json(["message" => "success"], 200);
	}

	public function edit($id, Request $request)
	{
		Wilayah::where('id', $id)->update([
			'nama_wilayah' => $request->name,
			'deskripsi' => $request->deskripsi
		]);

		return response()->json(["message" => "success"], 200);
	}

	public function delete($id)
	{
		$checkWilayah = Meteran::where('wilayah_id', '=', $id)->first();

		$message = "";

		if ($checkWilayah != null) {
			$message = "Data tidak bisa dihapus karena sedang digunakan pada tabel lain";
		} else {
			Wilayah::where('id', '=', $id)->delete();
			$message = "success";
		}


		return response()->json(["message" => $message], 200);
	}

}