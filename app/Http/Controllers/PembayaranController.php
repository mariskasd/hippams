<?php

namespace App\Http\Controllers;

use App\Exports\PembayaranExport;
use App\Models\Petugas;
use App\Models\Meteran;
use App\Models\Pembayaran;
use App\Models\Pelanggan;
use App\Models\Denda;
use App\Models\SisaBayar;
use App\Models\CatatMeter;
use App\Models\SettingPrice;
use App\Models\WilayahPetugas;
use App\Models\RT;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class PembayaranController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$petugas = Petugas::pluck('id_petugas', 'id_petugas');

		$pelanggan = Pelanggan::pluck('id_pelanggan', 'id_pelanggan');

		$userid = auth()->user()->id_pengguna;

		if (auth()->user()->role == "Lapangan") {
			$rt = WilayahPetugas::where('wilayahpetugas.petugas_id', $userid)
				->join('rts', 'rts.id', '=', 'wilayahpetugas.rt_id')
				->pluck('rts.nama_rt', 'rts.id');
		} else {
			$rt = RT::pluck('rts.nama_rt', 'rts.id');
		}

		$pembayaran = "";
		$idpelanggan = "";
		$idpetugas = "";
		$idrt = "";

		if (auth()->user()->role != "Pelanggan") {

			if ($request->start_date != null) {
				$startDate = $request->start_date;
			} else {
				$startDate = Carbon::now()->translatedFormat('Y-m');
			}

			$monthFilter = Carbon::parse($startDate)->translatedFormat('m');
			$yearFilter = Carbon::parse($startDate)->translatedFormat('Y');

			if (auth()->user()->role == "Admin") {
				$query = Pembayaran::join('catat_meters', 'catat_meters.id', '=', 'pembayarans.catat_meter_id')
					->join('petugas', 'pembayarans.petugas_id', '=', 'petugas.id_petugas')
					->join('meterans', 'meterans.id_meteran', '=', 'catat_meters.meteran_id')
					->join('pelanggans', 'pelanggans.id_pelanggan', '=', 'meterans.id_pelanggan')
					->whereMonth('pembayarans.tanggal_jatuh_tempo', '=', $monthFilter)
					->whereYear('pembayarans.tanggal_jatuh_tempo', '=', $yearFilter);
			} else {
				$query = Pembayaran::join('catat_meters', 'catat_meters.id', '=', 'pembayarans.catat_meter_id')
					->join('petugas', 'pembayarans.petugas_id', '=', 'petugas.id_petugas')
					->join('meterans', 'meterans.id_meteran', '=', 'catat_meters.meteran_id')
					->join('pelanggans', 'pelanggans.id_pelanggan', '=', 'meterans.id_pelanggan')
					->where('pembayarans.petugas_id', '=', auth()->user()->id_pengguna)
					->whereMonth('pembayarans.tanggal_jatuh_tempo', '=', $monthFilter)
					->whereYear('pembayarans.tanggal_jatuh_tempo', '=', $yearFilter);
			}

			if ($request->id_pelanggan != null && $request->id_pelanggan != "All") {

				$query->where('pelanggans.id_pelanggan', '=', $request->id_pelanggan);

				$idpelanggan = $request->id_pelanggan;

			} else {

				$idpelanggan = "All";
			}

			if ($request->id_petugas != null && $request->id_petugas != "All") {

				$query->where('petugas.id_petugas', '=', $request->id_petugas);

				$idpetugas = $request->id_petugas;
			} else {

				$idpetugas = "All";
			}

			if ($request->id_pelanggan != null && $request->id_pelanggan != "All") {

				$query->where('pelanggans.id_pelanggan', '=', $request->id_pelanggan);

				$idpelanggan = $request->id_pelanggan;

			} else {

				$idpelanggan = "All";
			}

			if ($request->id_rt != null && $request->id_rt != "All") {

				$query->where('meterans.rt_id', '=', $request->id_rt);

				$idrt = $request->id_rt;

			} else {

				$idrt = "All";
			}



			$pembayaran = $query->orderBy('pembayarans.tanggal_jatuh_tempo', 'DESC')
				->get(['pembayarans.*', 'catat_meters.*', 'meterans.id_meteran', 'pelanggans.nama as pelanggan', 'petugas.nama as petugas', 'pembayarans.petugas_id as petugas_idi']);


		} else {

			if ($request->start_date != null) {
				$startDate = $request->start_date;
			} else {
				$startDate = Carbon::now()->translatedFormat('Y-m');
			}

			$monthFilter = Carbon::parse($startDate)->translatedFormat('m');
			$yearFilter = Carbon::parse($startDate)->translatedFormat('Y');


			$pembayaran = Pembayaran::join('catat_meters', 'catat_meters.id', '=', 'pembayarans.catat_meter_id')
				->join('petugas', 'pembayarans.petugas_id', '=', 'petugas.id_petugas')
				->join('meterans', 'meterans.id_meteran', '=', 'catat_meters.meteran_id')
				->join('pelanggans', 'pelanggans.id_pelanggan', '=', 'meterans.id_pelanggan')
				->whereMonth('pembayarans.tanggal_jatuh_tempo', '=', $monthFilter)
				->whereYear('pembayarans.tanggal_jatuh_tempo', '=', $yearFilter)
				->where('pelanggans.id_pelanggan', '=', auth()->user()->id_pengguna)
				->get([
					'pembayarans.*',
					'catat_meters.*',
					'meterans.id_meteran',
					'pelanggans.nama as pelanggan',
					'petugas.nama as petugas',
					'pembayarans.petugas_id as petugas_idi'
				]);

		}

		$setting = SettingPrice::select("id", "min_harga", "max_harga", "min_denda", "max_denda")->orderBy('created_at', 'desc')->first();

		return view('pembayaran.pembayaran', compact(['pembayaran', 'petugas', 'pelanggan', 'rt', 'idpelanggan', 'idpetugas', 'idrt', 'startDate', 'setting']));
	}


	public function edit($id)
	{

		$pembayaran = Pembayaran::where('id_pembayaran', $id)->first();

		$catatmeters = CatatMeter::where('id', $pembayaran->catat_meter_id)->orderBy('created_at', 'desc')->first();

		$meterans = Meteran::where('id_meteran', $catatmeters->meteran_id)->first();

		$pelanggans = Pelanggan::where('id_pelanggan', $meterans->id_pelanggan)->first();

		$sisaBayar = SisaBayar::where('id_meteran', $catatmeters->meteran_id)
			->where('id_pembayaran', '!=', $id)
			->where('status_terpakai', '=', false)
			->where('created_at', '<', $pembayaran->tanggal_jatuh_tempo)
			->orderBy('created_at', 'desc')->sum('sisa_bayar');


		$sisaBayarSelesai = SisaBayar::where('id_pembayaran', '=', $id)
			->orderBy('created_at', 'desc')->sum('sisa_bayar');

		//get denda
		$denda = Denda::where('id_meteran', $catatmeters->meteran_id)
			->where('status_dibayar', '=', true)
			->orderBy('created_at', 'desc')->sum('denda');

		$setting = SettingPrice::select("id", "min_harga", "max_harga", "min_denda", "max_denda")->orderBy('created_at', 'desc')->first();

		if ($pembayaran->status_bayar == 0) {
			if (now() > $pembayaran->tanggal_jatuh_tempo) {

				$dendaNow = 0;

				$dayLate = now()->diffInDays($pembayaran->tanggal_jatuh_tempo);

				if ($dayLate >= 7) {
					$dendaNow = $setting->max_denda;
				} else {
					$dendaNow = $dayLate * $setting->min_denda;
				}

			} else {
				$dendaNow = 0;
				$dayLate = 0;
			}
		} else {
			$dendaGet = Denda::where('id_pembayaran', $pembayaran->id_pembayaran)->first();
			if ($dendaGet != null) {
				$dendaNow = $dendaGet->total_denda;
				$dayLate = $dendaGet->hari_keterlambatan;
			} else {
				$dendaNow = 0;
				$dayLate = 0;
			}
		}

		if ($pembayaran->status_bayar == 1) {
			$total = $pembayaran->total_tagihan;

			$checkKembalian = SisaBayar::where('id_pembayaran', $pembayaran->id_pembayaran)->first();
			$starts = Carbon::now()->startOfMonth()->subMonth();
			$ends = Carbon::now()->endOfMonth()->subMonth();

			$sisaBayarDone = SisaBayar::where('id_meteran', $catatmeters->meteran_id)
				->where('id_pembayaran', '!=', $id)
				->where('status_terpakai', '=', true)
				->where('created_at', '>=', $starts)
				->where('created_at', '<=', $ends)
				->orderBy('created_at', 'desc')->first();

			if ($checkKembalian == null) {
				$kembalian = 0;
			} else {
				$kembalian = $checkKembalian->sisa_bayar;
			}

			if ($sisaBayarDone == null) {
				$sisaBayar = 0;
			} else {
				$sisaBayar = $sisaBayarDone->sisa_bayar;
			}

		} else if ($sisaBayar < ($pembayaran->total_tagihan + $dendaNow)) {
			$total = $pembayaran->total_tagihan + $dendaNow - $sisaBayar;
			$kembalian = $pembayaran->total_bayar - $pembayaran->total_tagihan;
		} else {
			$total = 0;
			$kembalian = $sisaBayar - ($pembayaran->total_tagihan + $dendaNow);
		}

		$startDate = Carbon::now()->startOfMonth()->subMonth();
		$endDate = Carbon::now()->endOfMonth()->subMonth();


		$belumDibayar = new Collection;

		$lastPem = Pembayaran::join('catat_meters', 'catat_meters.id', '=', 'pembayarans.catat_meter_id')
			->where('catat_meters.meteran_id', '=', $catatmeters->meteran_id)
			->where('status_bayar', '=', false)->get(['pembayarans.*']);

		// $bayarBulanAwal = false;

		//get last status pembayaran 
		if ($lastPem != null) {

			foreach ($lastPem as $newData) {

				if ($newData->id_pembayaran != $id) {

					$formatingDate = $newData->created_at->translatedFormat('M Y');

					$belumDibayar->push((object) [
						'bulan' => $formatingDate,
						'value' => $newData->total_tagihan,
						'jatuh_tempo' => $newData->tanggal_jatuh_tempo
					]);
				}
			}
		}

		$tanggalFormating = Carbon::parse($pembayaran->tanggal_jatuh_tempo)->translatedFormat('M Y');


		return view('pembayaran.editpembayaran', compact(['pembayaran', 'pelanggans', 'dayLate', 'meterans', 'denda', 'sisaBayar', 'dendaNow', 'sisaBayarSelesai', 'dayLate', 'catatmeters', 'total', 'kembalian', 'belumDibayar', 'tanggalFormating', 'setting']));
	}

	public function bayarTagihan(Request $request)
	{
		$message = "";

		$pembayaran = Pembayaran::where('id_pembayaran', $request->id_pembayaran)->first();

		$catatmeters = CatatMeter::where('id', $pembayaran->catat_meter_id)->orderBy('created_at', 'desc')->first();

		if ($request->sisaGede == true) {
			$totBayar = $request->sisa_bayar - ($pembayaran->total_tagihan + $request->denda);

			Pembayaran::where('id_pembayaran', $request->id_pembayaran)->update([
				'status_bayar' => true,
				'deskripsi' => "LUNAS dengan SISTEM",
				'tanggal_bayar' => Carbon::now(),
				'total_bayar' => 0
			]);

			if ($request->denda != 0) {

				$denda = new Denda();
				$denda->id_pembayaran = $request->id_pembayaran;
				$denda->denda = (float) $request->denda;
				$denda->id_meteran = $catatmeters->meteran_id;
				$denda->status_dibayar = false;

				$denda->save();
			}

			Denda::where('id_pembayaran', $request->id_pembayaran)
				->where('status_dibayar', '=', false)->update([
						'status_dibayar' => true
					]);

			if ($totBayar != 0) {
				$sisBayarInput = new SisaBayar();
				$sisBayarInput->id_pembayaran = $request->id_pembayaran;
				$sisBayarInput->sisa_bayar = (float) $totBayar;
				$sisBayarInput->status_terpakai = false;
				$sisBayarInput->id_meteran = $catatmeters->meteran_id;

				$sisBayarInput->save();
			}
		} else {

			$totalFinal = $request->total_bayar;
			$setting = SettingPrice::select("id", "min_harga", "max_harga", "min_denda", "max_denda")->orderBy('created_at', 'desc')->first();

			if ($request->isTerlambat == 1) {
				$latePem = Pembayaran::join('catat_meters', 'catat_meters.id', '=', 'pembayarans.catat_meter_id')
					->where('pembayarans.id_pembayaran', '<>', $request->id_pembayaran)
					->where('catat_meters.meteran_id', '=', $catatmeters->meteran_id)
					->where('status_bayar', '=', false)->get(['pembayarans.*', 'catat_meters.meteran_id']);

				foreach ($latePem as $data) {
					if ($data->tanggal_jatuh_tempo < $pembayaran->tanggal_jatuh_tempo) {
						$setting = SettingPrice::select("id", "min_harga", "max_harga", "min_denda", "max_denda")->orderBy('created_at', 'desc')->first();
						$dayLate = now()->diffInDays($data->tanggal_jatuh_tempo);
						$dendaTotal = $setting->max_denda;

						if ($dayLate < 7) {
							$dendaTotal = $setting->min_denda * $dayLate;
						}

						Pembayaran::where('id_pembayaran', $data->id_pembayaran)->update([
							'status_bayar' => true,
							'deskripsi' => $request->keterangan,
							'tanggal_bayar' => Carbon::now(),
							'total_bayar' => $data->total_tagihan + $dendaTotal
						]);

						$totalFinal -= ($data->total_tagihan + $dendaTotal);

						$denda = new Denda();
						$denda->id_pembayaran = $data->id_pembayaran;
						$denda->denda = (float) $setting->min_denda;
						$denda->total_denda = (float) $dendaTotal;
						$denda->id_meteran = $data->meteran_id;
						$denda->hari_keterlambatan = $dayLate;
						$denda->status_dibayar = true;

						$denda->save();
					} else {
						Pembayaran::where('id_pembayaran', $data->id_pembayaran)->update([
							'status_bayar' => true,
							'deskripsi' => $request->keterangan,
							'tanggal_bayar' => Carbon::now(),
							'total_bayar' => $data->total_tagihan
						]);
						$totalFinal -= $data->total_tagihan;
					}
				}
			}

			Pembayaran::where('id_pembayaran', $request->id_pembayaran)->update([
				'status_bayar' => true,
				'deskripsi' => $request->keterangan,
				'tanggal_bayar' => Carbon::now(),
				'total_bayar' => $totalFinal
			]);



			if ($request->denda != 0) {

				$denda = new Denda();
				$denda->id_pembayaran = $request->id_pembayaran;
				$denda->denda = (float) $setting->min_denda;
				$denda->total_denda = (float) $request->denda;
				$denda->id_meteran = $catatmeters->meteran_id;
				$denda->hari_keterlambatan = $request->dayLate;
				$denda->status_dibayar = true;

				// $denda = new Denda();
				// $denda->id_pembayaran = $request->id_pembayaran;
				// $denda->denda = (float) $request->denda;
				// $denda->id_meteran = $catatmeters->meteran_id;
				// $denda->status_dibayar = true;

				$denda->save();
			}

			Denda::where('id_pembayaran', $request->id_pembayaran)
				->where('status_dibayar', '=', false)->update([
						'status_dibayar' => true
					]);

			$sisaBayars = SisaBayar::where('id_meteran', $catatmeters->meteran_id)->where('status_terpakai', '=', 0)
				->where('created_at', '<=', Carbon::now())->orderBy('created_at', 'desc')->first();

			if ($sisaBayars != null) {
				SisaBayar::where('id', $sisaBayars->id)->where('status_terpakai', '=', 0)->update([
					'status_terpakai' => 1
				]);
			}

			//check apakah ada sisa bayar 
			if ($request->kembalian == true) {
				$sisBayarInput = new SisaBayar();
				$sisBayarInput->id_pembayaran = $request->id_pembayaran;
				$sisBayarInput->sisa_bayar = (float) $request->kembalian;
				$sisBayarInput->id_meteran = $catatmeters->meteran_id;

				if ($request->useSisa == true) {
					$sisBayarInput->status_terpakai = 0;
				} else {
					$sisBayarInput->status_terpakai = 1;
				}

				$sisBayarInput->save();

				$status_terpakai = $request->useSisa == true ? 1 : 0;

				Pembayaran::where('id_pembayaran', $request->id_pembayaran)->update([
					'sisa_bayar_status' => $status_terpakai
				]);

			}

		}

		return redirect('/pembayaran');
	}

	public function buatPembayaran()
	{

		$startDate = Carbon::now()->startOfMonth();
		$endDate = Carbon::now()->endOfMonth();

		$petugas = Petugas::where("type", "=", "Lapangan")->get();

		$wilayahPetugas = new Collection;

		$countDisable = 0;

		foreach ($petugas as $pet) {

			//get wilayah petugas
			$wilPetugas = Petugas::join('wilayahpetugas', 'wilayahpetugas.petugas_id', '=', 'petugas.id_petugas')
				->join('rts', 'rts.id', '=', 'wilayahpetugas.rt_id')
				->where('petugas.id_petugas', '=', $pet->id_petugas)
				->select('rts.nama_rt as nama_rt')
				->get();

			$wilayah = "";

			if ($wilPetugas != null) {
				foreach ($wilPetugas as $wil) {
					$wilayah = $wilayah . "RT " . $wil->nama_rt . ",";
				}
			}

			$monthFilter = Carbon::now()->translatedFormat('m');
			$yearFilter = Carbon::now()->translatedFormat('Y');

			$arrayWil = Petugas::join('wilayahpetugas', 'wilayahpetugas.petugas_id', '=', 'petugas.id_petugas')
				->join('rts', 'rts.id', '=', 'wilayahpetugas.rt_id')
				->where('petugas.id_petugas', '=', $pet->id_petugas)
				->pluck('rts.id');

			$totalMeteran = Meteran::where("active", 1)->whereIn("rt_id", $arrayWil)->count();

			$catatMeter = CatatMeter::where('petugas_id', '=', $pet->id_petugas)
				->whereMonth('tanggal_pencatatan', '=', $monthFilter)
				->whereYear('tanggal_pencatatan', '=', $yearFilter)
				->join('meterans', 'catat_meters.meteran_id', '=', 'meterans.id_meteran')
				->where('active', 1)
				->whereIn('meterans.rt_id', $arrayWil)
				->count();

			//count pembayaran 
			$pembayaran = Pembayaran::join('catat_meters', 'catat_meters.id', '=', 'pembayarans.catat_meter_id')
				->where('catat_meters.petugas_id', '=', $pet->id_petugas)
				->whereMonth('tanggal_jatuh_tempo', '=', $monthFilter)
				->whereYear('tanggal_jatuh_tempo', '=', $yearFilter)
				->count();

			$wilayahPetugas->push((object) [
				'id_petugas' => $pet->id_petugas,
				'nama_petugas' => $pet->nama,
				'nama_rt' => $wilayah,
				'count' => $catatMeter,
				'countPembayaran' => $pembayaran,
				'done' => $catatMeter,
				'notDone' => $totalMeteran - $catatMeter
			]);

			if (($totalMeteran - $catatMeter) > 0) {
				$countDisable += 1;
			} elseif (($pembayaran >= $catatMeter) && ($totalMeteran - $catatMeter) == 0) {
				$countDisable += 1;
			}

		}


		return view('pembayaran.buatpembayaran', compact(['wilayahPetugas', 'startDate', 'countDisable']));
	}

	public function generateTagihan(Request $request)
	{

		$message = "";
		$startDate = Carbon::now()->startOfMonth();
		$endDate = Carbon::now()->endOfMonth();

		$dateToday = Carbon::now();


		$monthFilter = Carbon::parse($dateToday)->translatedFormat('m');
		$yearFilter = Carbon::parse($dateToday)->translatedFormat('Y');

		$counterId = 0;

		//loop per petugas
		foreach ($request->id_petugas as $id) {
			$petugasCatat = CatatMeter::where('petugas_id', $id)->whereMonth('tanggal_pencatatan', '=', $monthFilter)
				->whereYear('tanggal_pencatatan', '=', $yearFilter)->get();

			$startDate = Carbon::now()->startOfMonth()->subMonth();
			$endDate = Carbon::now()->endOfMonth()->subMonth();

			//loop catat_meter yang sudah dicatat
			foreach ($petugasCatat as $catatMeterId) {
				// print_r($catatMeterId);
				$catatMeter = CatatMeter::where('id', $catatMeterId->id)->orderBy('created_at', 'desc')->first();

				$pembayaran = Pembayaran::where('catat_meter_id', $catatMeterId->id)->orderBy('created_at', 'desc')->first();

				if ($catatMeter != null && $pembayaran == null) {

					if ($catatMeter->total_meteran != null && $catatMeter->foto != null) {
						$pelSeq = strtotime(Carbon::now()) + $counterId;
						$idpel = "T" . $pelSeq;

						$setting = SettingPrice::select("id", "min_harga", "max_harga", "min_denda", "max_denda")->orderBy('created_at', 'desc')->first();

						$hargaSatuan = 0;
						$meteranTerpakai = 0;

						//get selisih meteran 
						$catatLast = CatatMeter::where('meteran_id', '=', $catatMeter->meteran_id)
							->where('tanggal_pencatatan', '>=', $startDate)
							->where('tanggal_pencatatan', '<=', $endDate)->first();

						if ($catatLast != null) {
							//get selisih
							$selMet = $catatMeter->total_meteran_terpakai;

							if ($selMet > 30) {
								$totalTagihan = $selMet * (float) $setting->max_harga;
								$hargaSatuan = $setting->max_harga;
							} else {
								$totalTagihan = $selMet * (float) $setting->min_harga;
								$hargaSatuan = $setting->min_harga;
							}

							$meteranTerpakai = $selMet;

						} else {

							if ($catatMeter->total_meteran > 30) {
								$totalTagihan = $catatMeter->total_meteran * (float) $setting->max_harga;
								$hargaSatuan = $setting->max_harga;
							} else {
								$totalTagihan = $catatMeter->total_meteran * (float) $setting->min_harga;
								$hargaSatuan = $setting->min_harga;
							}

							$meteranTerpakai = $catatMeter->total_meteran_terpakai;
						}

						$petugasPem = WilayahPetugas::join("meterans", "meterans.rt_id", "=", "wilayahpetugas.rt_id")
							->join("petugas", "petugas.id_petugas", "=", "wilayahpetugas.petugas_id")
							->where("petugas.type", "=", "Pembayaran")
							->where('meterans.id_meteran', "=", $catatMeter->meteran_id)->first();

						$wilPem = new Pembayaran();
						$wilPem->id_pembayaran = $idpel;
						$wilPem->catat_meter_id = $catatMeterId->id;
						$wilPem->petugas_id = $petugasPem->petugas_id;
						$wilPem->total_tagihan = (float) $totalTagihan;
						$wilPem->status_bayar = false;
						$wilPem->tanggal_jatuh_tempo = Carbon::now()->endOfMonth();
						$wilPem->deskripsi = "";
						$wilPem->harga_satuan = $hargaSatuan;
						$wilPem->denda_setting = $setting->min_denda;
						$wilPem->sisa_bayar_status = false;
						$wilPem->total_meteran_terpakai = $meteranTerpakai;

						$wilPem->save();
						$message = "success";

					} else {
						$message = "Data belum lengkap, Tagihan tidak bisa di buat";
					}
				} else {
					if ($pembayaran != null) {
						$message = "Tagihan sudah pernah di buat";
					}

				}

				$counterId += 1;
			}
		}
		return redirect('/pembayaran');
	}

	public function export_excel(Request $request)
	{
		$filename = 'Pembayaran ' . $request->date . '.xlsx';
		if ($request->id_rt != null && $request->id_rt != "All") {
			$rt = RT::where('id', $request->id_rt)->first();
			$filename = 'Pembayaran ' . $request->date . ' | RT ' . $rt->nama_rt . '.xlsx';
		}
		return Excel::download(new PembayaranExport($request->id_rt, $request->date), $filename);
	}

}