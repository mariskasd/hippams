<?php

namespace App\Http\Controllers;

use App\Models\CatatMeter;
use App\Models\Pembayaran;
use App\Models\Petugas;
use App\Models\WilayahPetugas;
use App\Models\Rt;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PetugasController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$petugas = Petugas::select("*")->orderBy('created_at', 'desc')->get();

		return view('petugas.petugas', compact(['petugas']));
	}

	public function add(Request $request)
	{
		$message = "";
		$checkPhone = Petugas::where('no_telp', '=', $request->no_telp)->first();

		if ($checkPhone === null) {

			$codePet = "";

			if ($request->type === "Lapangan") {
				$codePet = "PL";
			} else if ($request->type === "Pembayaran") {
				$codePet = "PP";
			}

			$latestPet = Petugas::orderBy('created_at', 'DESC')->first();

			$intID = 0;

			if ($latestPet != null) {
				$latestID = substr($latestPet->id_petugas, 2);
				$intID = (int) $latestID;
			}

			$petSeq = str_pad($intID + 1, 3, "0", STR_PAD_LEFT);
			$idpetugas = $codePet . $petSeq;

			$pel = new Petugas();
			$pel->id_petugas = $idpetugas;
			$pel->nama = $request->name;
			$pel->no_telp = $request->no_telp;
			$pel->alamat = $request->alamat;
			$pel->type = $request->type;

			$pel->save();

			User::create([
				'name' => $request->name,
				'id_pengguna' => $idpetugas,
				'password' => Hash::make($idpetugas),
				'Role' => $request->type
			]);

			$message = "success";
		} else {
			$message = "No Telp sudah pernah didaftarkan sebelumnya";
		}

		return response()->json(["message" => $message], 200);
	}

	public function edit($id, Request $request)
	{
		Petugas::where('id_petugas', $id)->update([
			'nama' => $request->name,
			'no_telp' => $request->no_telp,
			'alamat' => $request->alamat,
			'type' => $request->type
		]);

		return response()->json(["message" => "success"], 200);
	}

	public function delete($id)
	{

		$checkWil = WilayahPetugas::where('petugas_id', '=', $id)->first();
		$checkPem = Pembayaran::where('petugas_id', '=', $id)->first();
		$checkCatat = CatatMeter::where('petugas_id', '=', $id)->first();

		$message = "";

		if ($checkWil == null && $checkPem == null && $checkCatat == null) {
			Petugas::where('id_petugas', '=', $id)->delete();
			$message = "success";
		} else {
			$message = "Data tidak bisa dihapus karena sedang digunakan pada tabel lain";
		}

		return response()->json(["message" => $message], 200);
	}

	public function viewWilayahPetugas($id)
	{
		$rt = RT::pluck('nama_rt', 'id');

		$petugas = Petugas::where('id_petugas', $id)->orderBy('created_at', 'desc')->first();

		$wilayahPetugas = WilayahPetugas::where('wilayahpetugas.petugas_id', $id)
			->join('rts', 'rts.id', '=', 'wilayahpetugas.rt_id')
			->get(['wilayahpetugas.*', 'rts.nama_rt']);

		return view('petugas.viewswilayahpetugas', compact(['wilayahPetugas', 'petugas', 'rt']));
	}

	public function addWilayahPetugas(Request $request)
	{
		$getDev = Petugas::where('id_petugas', '=', $request->petugas_id)->first();

		$checkExisting = WilayahPetugas::where('petugas_id', $request->petugas_id)
			->where('rt_id', $request->rt)
			->orderBy('created_at', 'desc')->first();

		$checkWilDev = WilayahPetugas::join('petugas', 'petugas.id_petugas', '=', 'wilayahpetugas.petugas_id')
			->where('wilayahpetugas.rt_id', $request->rt)
			->where('petugas.type', $getDev->type)
			->orderBy('wilayahpetugas.created_at', 'desc')->first();

		$message = "";

		if ($checkExisting === null && $checkWilDev === null) {

			$wilPet = new WilayahPetugas();
			$wilPet->petugas_id = $request->petugas_id;
			$wilPet->rt_id = $request->rt;

			$wilPet->save();
			$message = "success";

		} else {
			if ($checkExisting != null) {
				$message = "Wilayah petugas sudah pernah ditambahkan";
			} else {
				$message = "Wilayah tersebut sudah dipakai oleh petugas lain";
			}

		}

		return response()->json(["message" => $message], 200);
	}

	public function deleteWilayahPetugas($id)
	{
		WilayahPetugas::where('id', '=', $id)->delete();

		return response()->json(["message" => "success"], 200);
	}

}