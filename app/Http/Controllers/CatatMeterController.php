<?php

namespace App\Http\Controllers;

use App\Models\Pelanggan;
use App\Models\Meteran;
use App\Models\CatatMeter;
use App\Models\RT;
use App\Models\Petugas;
use App\Models\WilayahPetugas;
use App\Models\Pembayaran;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PencatatanExport;


class CatatMeterController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$meteran = Meteran::pluck('id_meteran', 'id_meteran');

		$petugas = Petugas::pluck('id_petugas', 'id_petugas');

		$pelanggan = Pelanggan::pluck('id_pelanggan', 'id_pelanggan');


		$userid = auth()->user()->id_pengguna;

		if (auth()->user()->role == "Lapangan") {
			$rt = WilayahPetugas::where('wilayahpetugas.petugas_id', $userid)
				->join('rts', 'rts.id', '=', 'wilayahpetugas.rt_id')
				->pluck('rts.nama_rt', 'rts.id');
		} else {
			$rt = RT::pluck('rts.nama_rt', 'rts.id');
		}


		$pencatatanMeteran = new Collection;
		$idpelanggan = "";
		$idpetugas = "";
		$idrt = "";

		if (auth()->user()->role == "Admin") {

			if ($request->start_date != null) {
				$startDate = $request->start_date;
			} else {
				$startDate = Carbon::now()->translatedFormat('Y-m');
			}

			$monthFilter = Carbon::parse($startDate)->translatedFormat('m');
			$yearFilter = Carbon::parse($startDate)->translatedFormat('Y');

			$pencatatanMeteran = new Collection;

			//get data catat meter 
			$query = Meteran::join('pelanggans', 'pelanggans.id_pelanggan', '=', 'meterans.id_pelanggan')
				->join('wilayahs', 'wilayahs.id', 'meterans.wilayah_id')
				->join('rts', 'rts.id', 'meterans.rt_id')
				->join('rws', 'rws.id', 'rts.rw_id')
				->join('dusuns', 'dusuns.id', 'rws.dusun_id')
				->join('wilayahpetugas', 'wilayahpetugas.rt_id', 'meterans.rt_id')
				->join('petugas', 'petugas.id_petugas', '=', 'wilayahpetugas.petugas_id')
				->leftJoin('catat_meters', function ($join) use ($monthFilter, $yearFilter) {
					$join->on('catat_meters.meteran_id', '=', 'meterans.id_meteran');
					$join->on(function ($query) use ($monthFilter, $yearFilter) {
						$query->whereMonth('catat_meters.tanggal_pencatatan', '=', $monthFilter);
						$query->whereYear('catat_meters.tanggal_pencatatan', '=', $yearFilter);
					});

				})
				->leftJoin('pembayarans', 'pembayarans.catat_meter_id', '=', 'catat_meters.id')
				->where('petugas.type', 'Lapangan')
				->where('meterans.active', 1);


			if ($request->id_pelanggan != null && $request->id_pelanggan != "All") {

				$query->where('pelanggans.id_pelanggan', '=', $request->id_pelanggan);

				$idpelanggan = $request->id_pelanggan;

			} else {

				$idpelanggan = "All";
			}

			if ($request->id_petugas != null && $request->id_petugas != "All") {

				$query->where('petugas.id_petugas', '=', $request->id_petugas);

				$idpetugas = $request->id_petugas;
			} else {

				$idpetugas = "All";
			}

			if ($request->rt != null && $request->rt != "All") {

				$query->where('meterans.rt_id', '=', $request->rt);

				$idrt = $request->rt;
			} else {

				$idrt = "All";
			}

			$pencatatanMeteran = $query->get(['petugas.nama as petugas', 'petugas.id_petugas', 'pelanggans.nama as pelanggan', 'meterans.*', 'wilayahs.*', 'rts.*', 'rws.*', 'dusuns.*', 'catat_meters.*', 'catat_meters.total_meteran_terpakai as terpakai', 'pembayarans.*']);
		} else if (auth()->user()->role == "Lapangan") {

			if ($request->start_date != null) {
				$startDate = $request->start_date;
			} else {
				$startDate = Carbon::now()->translatedFormat('Y-m');
			}

			$monthFilter = Carbon::parse($startDate)->translatedFormat('m');
			$yearFilter = Carbon::parse($startDate)->translatedFormat('Y');

			//get data meteran 
			$query = Meteran::join('pelanggans', 'pelanggans.id_pelanggan', '=', 'meterans.id_pelanggan')
				->join('wilayahs', 'wilayahs.id', 'meterans.wilayah_id')
				->join('rts', 'rts.id', 'meterans.rt_id')
				->join('rws', 'rws.id', 'rts.rw_id')
				->join('dusuns', 'dusuns.id', 'rws.dusun_id')
				->join('wilayahpetugas', 'wilayahpetugas.rt_id', 'meterans.rt_id')
				->join('petugas', 'petugas.id_petugas', '=', 'wilayahpetugas.petugas_id')
				->where('petugas.id_petugas', '=', $userid)
				->leftJoin('catat_meters', function ($join) use ($monthFilter, $yearFilter) {
					$join->on('catat_meters.meteran_id', '=', 'meterans.id_meteran');
					$join->on(function ($query) use ($monthFilter, $yearFilter) {
						$query->whereMonth('catat_meters.tanggal_pencatatan', '=', $monthFilter);
						$query->whereYear('catat_meters.tanggal_pencatatan', '=', $yearFilter);
					});

				})
				->leftJoin('pembayarans', 'pembayarans.catat_meter_id', '=', 'catat_meters.id')
				->where('petugas.type', 'Lapangan')
				->where('meterans.active', 1);


			if ($request->id_pelanggan != null && $request->id_pelanggan != "All") {

				$query->where('pelanggans.id_pelanggan', '=', $request->id_pelanggan);

				$idpelanggan = $request->id_pelanggan;

			} else {

				$idpelanggan = "All";
			}

			if ($request->rt != null && $request->rt != "All") {

				$query->where('meterans.rt_id', '=', $request->rt);

				$idrt = $request->rt;
			} else {

				$idrt = "All";
			}

			$pencatatanMeteran = $query->get(['petugas.nama as petugas', 'petugas.id_petugas', 'pelanggans.nama as pelanggan', 'meterans.*', 'wilayahs.*', 'rts.*', 'rws.*', 'dusuns.*', 'catat_meters.*', 'catat_meters.total_meteran_terpakai as terpakai', 'pembayarans.*']);
		} else {

			if ($request->start_date != null) {
				$startDate = $request->start_date;
			} else {
				$startDate = Carbon::now()->translatedFormat('Y-m');
			}

			$monthFilter = Carbon::parse($startDate)->translatedFormat('m');
			$yearFilter = Carbon::parse($startDate)->translatedFormat('Y');

			//get data meteran 
			$query = Meteran::join('pelanggans', 'pelanggans.id_pelanggan', '=', 'meterans.id_pelanggan')
				->where('meterans.id_pelanggan', '=', $userid)
				->join('wilayahs', 'wilayahs.id', 'meterans.wilayah_id')
				->join('rts', 'rts.id', 'meterans.rt_id')
				->join('rws', 'rws.id', 'rts.rw_id')
				->join('dusuns', 'dusuns.id', 'rws.dusun_id')
				->join('wilayahpetugas', 'wilayahpetugas.rt_id', 'meterans.rt_id')
				->join('petugas', 'petugas.id_petugas', '=', 'wilayahpetugas.petugas_id')
				->leftJoin('catat_meters', function ($join) use ($monthFilter, $yearFilter) {
					$join->on('catat_meters.meteran_id', '=', 'meterans.id_meteran');
					$join->on(function ($query) use ($monthFilter, $yearFilter) {
						$query->whereMonth('catat_meters.tanggal_pencatatan', '=', $monthFilter);
						$query->whereYear('catat_meters.tanggal_pencatatan', '=', $yearFilter);
					});
				})
				->leftJoin('pembayarans', 'pembayarans.catat_meter_id', '=', 'catat_meters.id')
				->where('petugas.type', 'Lapangan')
				->where('meterans.active', 1);


			$pencatatanMeteran = $query->get(['petugas.nama as petugas', 'petugas.id_petugas', 'pelanggans.nama as pelanggan', 'meterans.*', 'wilayahs.*', 'rts.*', 'rws.*', 'dusuns.*', 'catat_meters.*', 'catat_meters.total_meteran_terpakai as terpakai', 'pembayarans.*']);
		}

		return view('catatmeter.catatmeter', compact(['meteran', 'userid', 'pencatatanMeteran', 'petugas', 'pelanggan', 'startDate', 'idpelanggan', 'idpetugas', 'rt', 'idrt']));

	}

	public function editCatatMeter($id)
	{
		$meteran = Meteran::pluck('id_meteran', 'id_meteran');

		$petugas = Petugas::pluck('id_petugas', 'id_petugas');

		$pecatatanMeteran = CatatMeter::where('id', $id)->leftJoin('pembayarans', 'catat_meters.id', '=', 'pembayarans.catat_meter_id')->select('catat_meters.*', 'catat_meters.petugas_id as petugas_meter', 'pembayarans.*')->first();

		return view('catatmeter.editmeteran', compact(['meteran', 'pecatatanMeteran', 'petugas']));
	}

	public function add(Request $request)
	{
		$message = "";

		$filenameSimpan = "";
		$startDate = Carbon::now()->startOfMonth()->toDateString();
		$endDate = Carbon::now()->endOfMonth()->toDateString();

		//check catat meter 
		$catat = CatatMeter::where('meteran_id', '=', $request->meteran_id)
			->where('catat_meters.tanggal_pencatatan', '>=', $startDate)
			->where('catat_meters.tanggal_pencatatan', '<=', $endDate)->first();

		if ($catat == null) {

			if ($request->has('image')) {
				$filenameWithExt = $request->file('image')->getClientOriginalName();
				$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
				$extension = $request->file('image')->getClientOriginalExtension();
				$filenameSimpan = $filename . '_' . time() . '.' . $extension;
				$path = $request->file('image')->storeAs('public/data_meteran', $filenameSimpan);
			} else {
				$filenameSimpan = "tidak ada";
			}

			$startDate = Carbon::now()->startOfMonth()->subMonth();
			$endDate = Carbon::now()->endOfMonth()->subMonth();

			$totalTerpakai = $request->total_meteran;

			$getMeterBefore = CatatMeter::where('meteran_id', $request->meteran_id)->where('tanggal_pencatatan', '>=', $startDate)
				->where('tanggal_pencatatan', '<=', $endDate)->orderBy('tanggal_pencatatan', 'desc')->first();

			if ($getMeterBefore != null) {
				if ($request->total_meteran == 0) {
					if ($getMeterBefore->total_meteran == 0) {
						$totalTerpakai = $getMeterBefore->total_meteran_terpakai;
					} else {
						$totalTerpakai = $getMeterBefore->total_meteran_terpakai;
					}
				} else {
					if ($getMeterBefore->total_meteran == 0) {
						$totalTerpakai = $request->total_meteran;
					} else if ($request->total_meteran < $getMeterBefore->total_meteran) {
						$totalTerpakai = $request->total_meteran;
					} else {
						$totalTerpakai = $request->total_meteran - $getMeterBefore->total_meteran;
					}
				}
			}

			$cat = new CatatMeter();
			$cat->meteran_id = $request->meteran_id;
			$cat->petugas_id = $request->petugas_id;
			$cat->tanggal_pencatatan = Carbon::now();
			$cat->keterangan = $request->keterangan;
			$cat->foto = $filenameSimpan;
			$cat->total_meteran = (float) $request->total_meteran;
			$cat->total_meteran_terpakai = (float) $totalTerpakai;

			$cat->save();

		} else {
			$message = "Pencatatan sudah pernah dilakukan";
		}

		return redirect('/catat-meter');
	}

	public function edit(Request $request)
	{
		$message = "";

		$checkData = CatatMeter::where('id', $request->id)->orderBy('created_at', 'desc')->first();

		$startDate = Carbon::now()->startOfMonth()->subMonth();
		$endDate = Carbon::now()->endOfMonth()->subMonth();

		$totalTerpakai = $request->total_meteran;

		$getMeterBefore = CatatMeter::where('meteran_id', $request->meteran_id)->where('tanggal_pencatatan', '>=', $startDate)
			->where('tanggal_pencatatan', '<=', $endDate)->orderBy('tanggal_pencatatan', 'desc')->first();

		if ($getMeterBefore != null) {
			if ($request->total_meteran == 0) {
				if ($getMeterBefore->total_meteran == 0) {
					$totalTerpakai = $getMeterBefore->total_meteran_terpakai;
				} else {
					$totalTerpakai = $getMeterBefore->total_meteran_terpakai;
				}
			} else {
				if ($getMeterBefore->total_meteran == 0) {
					$totalTerpakai = $request->total_meteran;
				} else if ($request->total_meteran < $getMeterBefore->total_meteran) {
					$totalTerpakai = $request->total_meteran;
				} else {
					$totalTerpakai = $request->total_meteran - $getMeterBefore->total_meteran;
				}
			}
		}

		if ($totalTerpakai > 30) {
			$totalTagihan = $totalTerpakai * (float) 1000;
		} else {
			$totalTagihan = $totalTerpakai * (float) 750;
		}

		CatatMeter::where('id', $request->id)->update([
			'total_meteran' => (float) $request->total_meteran,
			'total_meteran_terpakai' => (float) $totalTerpakai
		]);

		//check pembayaran
		$pembayaran = Pembayaran::where('catat_meter_id', $request->id)->first();

		if ($pembayaran != null) {

			Pembayaran::where('catat_meter_id', $request->id)->update([
				'total_tagihan' => (float) $totalTagihan,
				'total_meteran_terpakai' => (float) $totalTerpakai
			]);

		}

		return redirect('/catat-meter');
	}

	public function checkPrevMeteran(Request $request)
	{
		$startDate = Carbon::now()->startOfMonth()->subMonth();
		$endDate = Carbon::now()->endOfMonth()->subMonth();

		$getMeterBefore = CatatMeter::where('meteran_id', $request->meteran_id)->where('tanggal_pencatatan', '>=', $startDate)
			->where('tanggal_pencatatan', '<=', $endDate)->orderBy('tanggal_pencatatan', 'desc')->first();

		if ($request->total_meteran < $getMeterBefore->total_meteran) {
			$message = "Total Meteran kurang dari bulan sebelumnya, jika meteran baru atau rusak maka centang pilihan Baru/Rusak untuk melanjutkan";
		} else {
			$message = "success";
		}

		return response()->json(["message" => $message], 200);
	}

	public function getPelanggan(Request $request)
	{
		$meteran = Meteran::where('id_meteran', '=', $request->meteran)->first();
		$message = "";

		$pelanggan = Pelanggan::where('id_pelanggan', '=', $meteran->id_pelanggan)->first();

		return response()->json($pelanggan);
	}

	public function export_excel(Request $request)
	{
		$filename = 'Pencatatan Meteran ' . $request->date . '.xlsx';
		if ($request->rt != null && $request->rt != "All") {
			$rt = RT::where('id', $request->rt)->first();
			$filename = 'Pencatatan Meteran ' . $request->date . ' | RT ' . $rt->nama_rt . '.xlsx';
		}
		return Excel::download(new PencatatanExport($request->date, $request->rt), $filename);
	}

}