<?php

namespace App\Http\Controllers;

use App\Models\Dusun;
use App\Models\RW;
use Illuminate\Http\Request;

class DusunController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$dusun = Dusun::select("id", "nama_dusun")->orderBy('created_at', 'desc')->get();

		return view('dusun.dusun', compact(['dusun']));
	}

	public function add(Request $request)
	{

		$dusun = new Dusun();
		$dusun->nama_dusun = $request->name;

		$dusun->save();

		return response()->json(["message" => "success"], 200);
	}

	public function edit($id, Request $request)
	{
		Dusun::where('id', $id)->update([
			'nama_dusun' => $request->name
		]);

		return response()->json(["message" => "success"], 200);
	}

	public function delete($id)
	{
		//check dusun pd RW

		$checkRW = RW::where('dusun_id', '=', $id)->first();

		$message = "";

		if ($checkRW != null) {
			$message = "Data tidak bisa dihapus karena sedang digunakan pada tabel lain";
		} else {
			Dusun::where('id', '=', $id)->delete();
			$message = "success";
		}


		return response()->json(["message" => $message], 200);
	}

}