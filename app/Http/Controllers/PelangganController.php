<?php

namespace App\Http\Controllers;

use App\Models\Dusun;
use App\Models\Komplain;
use App\Models\Rw;
use App\Models\Pelanggan;
use App\Models\Rt;
use App\Models\User;
use App\Models\Meteran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PelangganController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$dusun = Dusun::pluck('nama_dusun', 'id');

		$pelanggan = Pelanggan::join('rts', 'rts.id', '=', 'pelanggans.rt_id')
			->join('rws', 'rws.id', '=', 'rts.rw_id')
			->join('dusuns', 'dusuns.id', '=', 'rws.dusun_id')
			->get(['pelanggans.*', 'dusuns.nama_dusun', 'rws.nama_rw', 'rts.nama_rt', 'rws.dusun_id', 'rts.rw_id']);

		return view('pelanggan.pelanggan', compact(['pelanggan', 'dusun']));
	}

	public function add(Request $request)
	{
		$message = "";
		$checkPhone = Pelanggan::where('no_telp', '=', $request->no_telp)->first();

		if ($checkPhone === null) {

			$latestPel = Pelanggan::where('rt_id', '=', $request->rt)->orderBy('created_at', 'DESC')->first();
			$intID = 0;

			if ($latestPel != null) {
				$latestID = substr($latestPel->id_pelanggan, 4);
				$intID = (int) $latestID;
			}

			$rt = Rt::where('id', '=', $request->rt)->first();
			$rw = RW::where('id', '=', $request->rw)->first();

			$pelSeq = str_pad($intID + 1, 3, "0", STR_PAD_LEFT);
			$idpel = (string) $rw->nama_rw . $rt->nama_rt . $pelSeq;

			$pel = new Pelanggan();
			$pel->id_pelanggan = $idpel;
			$pel->nama = $request->name;
			$pel->no_telp = $request->no_telp;
			$pel->alamat = $request->alamat;
			$pel->rt_id = $request->rt;

			$pel->save();

			User::create([
				'name' => $request->name,
				'id_pengguna' => $idpel,
				'password' => Hash::make($idpel),
				'Role' => "Pelanggan"
			]);

			$message = "success";
		} else {
			$message = "No Telp sudah pernah didaftarkan sebelumnya";
		}

		return response()->json(["message" => $message], 200);
	}

	public function edit($id, Request $request)
	{
		Pelanggan::where('id_pelanggan', $id)->update([
			'nama' => $request->name,
			'no_telp' => $request->no_telp,
			'alamat' => $request->alamat,
			'rt_id' => $request->rt
		]);

		User::where('id_pengguna', $request->id)->update([
			'name' => $request->name
		]);

		return response()->json(["message" => "success"], 200);
	}

	public function delete($id)
	{
		$checkMeteran = Meteran::where('id_pelanggan', '=', $id)->first();
		$checkKomplain = Komplain::where('pelanggan_id', '=', $id)->first();

		$message = "";

		if ($checkMeteran == null && $checkKomplain == null) {
			User::where('id_pengguna', '=', $id)->delete();
			Pelanggan::where('id_pelanggan', '=', $id)->delete();
			$message = "success";
		} else {
			$message = "Data tidak bisa dihapus karena sedang digunakan pada tabel lain";
		}

		return response()->json(["message" => $message], 200);
	}

	public function getDataRW(Request $request)
	{
		$rw = RW::where('dusun_id', $request->get('id'))
			->pluck('nama_rw', 'id');

		return response()->json($rw);
	}

	public function getDataRT(Request $request)
	{
		$rt = RT::where('rw_id', $request->get('id'))
			->pluck('nama_rt', 'id');

		return response()->json($rt);
	}

}