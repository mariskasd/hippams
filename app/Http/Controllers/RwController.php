<?php

namespace App\Http\Controllers;

use App\Models\Dusun;
use App\Models\Rw;
use App\Models\RT;
use Illuminate\Http\Request;

class RwController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$dusun = Dusun::pluck('nama_dusun', 'id');

		$rw = RW::join('dusuns', 'rws.dusun_id', '=', 'dusuns.id')
			->get(['rws.*', 'dusuns.nama_dusun']);

		return view('rw.rw', compact(['rw', 'dusun']));
	}

	public function add(Request $request)
	{

		$rw = new RW();
		$rw->nama_rw = $request->name;
		$rw->dusun_id = $request->dusun;

		$rw->save();

		return response()->json(["message" => "success"], 200);
	}

	public function edit($id, Request $request)
	{
		RW::where('id', $id)->update([
			'nama_rw' => $request->name,
			'dusun_id' => $request->dusun
		]);

		return response()->json(["message" => "success"], 200);
	}

	public function delete($id)
	{
		$checkRT = RT::where('rw_id', '=', $id)->first();

		$message = "";

		if ($checkRT != null) {
			$message = "Data tidak bisa dihapus karena sedang digunakan pada tabel lain";
		} else {
			RW::where('id', '=', $id)->delete();
			$message = "success";
		}


		return response()->json(["message" => $message], 200);
	}

}