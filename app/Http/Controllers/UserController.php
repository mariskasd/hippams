<?php

namespace App\Http\Controllers;

use App\Models\Allowance;
use App\Models\Company;
use App\Models\Employee;
use App\Models\Package;
use App\Models\User;
use App\Models\Transaction;
use App\Models\EmployeeSalaryCut;
use App\Models\EmployeeAllowance;
use App\Models\SalaryCut;
use Illuminate\Http\Request;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$companyId = Auth::user()->company_id;

		$user = User::where('company_id', $companyId)->whereNotNull('employee_id')->pluck('employee_id')->toArray();
		$employee = Employee::whereIn('id', $user)->get();

		$companyDetail = Transaction::where('company_id', $companyId)->orderBy('created_at', 'desc')->first();

		$packages = Package::where('id', $companyDetail->packages_id)->first();

		$isFull = false;

		if($employee->count() >= $packages->limited_employee && $packages->limited_employee != 0){
			$isFull = true;
		}

		return view('user.user-management', compact(['employee', 'packages','isFull']));
	}

	public function add(Request $request)
	{
		$companyId = Auth::user()->company_id;

		$employee = new Employee();
		$employee->name = $request->name;
		$employee->email = $request->email;
		$employee->password = Hash::make("123456");
		$employee->employee_number = $request->employee_number;
		$employee->position = $request->position;
		$employee->npwp = $request->npwp;
		$employee->salary = $request->salary;
		$employee->address = $request->address;

		$employee->save();

		User::create([
			'name' => $request->name,
			'email' => $request->email,
			'password' => Hash::make("123456"),
			'company_id' => $companyId,
			'employee_id' => $employee->id
		]);

		$totalEmployee = User::where('company_id',$companyId)->whereNotNull('employee_id')->get()->count();
		
		$companyDetail = Transaction::where('company_id', $companyId)->orderBy('created_at', 'desc')->first();

		$packages = Package::where('id', $companyDetail->packages_id)->first();

		if($totalEmployee == $packages->limited_employee && $packages->limited_employee != 0){
			return response()->json(["message" => "Total karyawan sudah mencapai limit paket"], 200);
		}
		
		return response()->json(["message" => "success"], 200);
	}

	public function edit($id,Request $request)
	{
		$data = request()->all();

		Employee::where('id', $id)->update($data);

		User::where('employee_id',$id)->update([
			'name' => $request->name,
			'email' => $request->email
		]);

		return response()->json(["message" => "success"], 200);
	}

	public function delete($id)
	{
		Employee::where('id', $id)->delete();
		User::where('employee_id', $id)->delete();

		return response()->json(["message" => "success"], 200);
	}

	public function salaryCutUser($id){
		$companyId = Auth::user()->company_id;
		$employee = Employee::where('id',$id)->first();
		$potongan = EmployeeSalaryCut::where('employee_id',$id)->with('salaryCut')->get();
		$currentpotongan = EmployeeSalaryCut::where('employee_id',$id)->pluck('salarycuts_id')->toArray();
		$potonganList = SalaryCut::whereNotIn('id', $currentpotongan)->where('company_id',$companyId)->get();

		return view('salarycut.user-cut',compact(['potongan','employee','potonganList']));
	}

	public function addSalaryCutUser(Request $request)
	{
		EmployeeSalaryCut::create([
			'salarycuts_id' => $request->potongan,
			'employee_id' => $request->employee_id,
			'nominal' => $request->nominal
		]);

		return response()->json(["message" => "success"], 200);
	}

	public function editSalaryCutUser($id,Request $request)
	{
		EmployeeSalaryCut::where('id',$id)->update([
			'nominal' => $request->nominal
		]);

		return response()->json(["message" => "success"], 200);
	}

	public function deleteSalaryCutUser($id)
	{
		EmployeeSalaryCut::where('id', $id)->delete();

		return response()->json(["message" => "success"], 200);
	}

	public function allowanceUser($id){
		$companyId = Auth::user()->company_id;
		$employee = Employee::where('id',$id)->first();
		$tunjangan = EmployeeAllowance::where('employee_id',$id)->with('allowances')->get();
		$currenttunjangan = EmployeeAllowance::where('employee_id',$id)->pluck('allowances_id')->toArray();
		$tunjanganList = Allowance::whereNotIn('id', $currenttunjangan)->where('company_id',$companyId)->get();

		return view('allowance.user-allowance',compact(['tunjangan','employee','tunjanganList']));
	}

	public function addAllowanceUser(Request $request)
	{
		EmployeeAllowance::create([
			'allowances_id' => $request->tunjangan,
			'employee_id' => $request->employee_id,
			'nominal' => $request->nominal
		]);

		return response()->json(["message" => "success"], 200);
	}

	public function editAllowanceUser($id,Request $request)
	{
		EmployeeAllowance::where('id',$id)->update([
			'nominal' => $request->nominal
		]);

		return response()->json(["message" => "success"], 200);
	}

	public function deleteAllowanceUser($id)
	{
		EmployeeAllowance::where('id', $id)->delete();

		return response()->json(["message" => "success"], 200);
	}
	public function profile(){
		$users = Auth::user();
		$companyId = Auth::user()->company_id;
		$company = Company::where('id',$companyId)->first();
		return view('user.user-profile',compact(['company']));
	}
}
