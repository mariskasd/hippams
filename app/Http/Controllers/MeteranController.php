<?php

namespace App\Http\Controllers;

use App\Models\CatatMeter;
use App\Models\Denda;
use App\Models\Dusun;
use App\Models\Pelanggan;
use App\Models\SisaBayar;
use App\Models\Meteran;
use App\Models\Wilayah;
use Illuminate\Http\Request;


class MeteranController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index($id)
	{
		$wilayah = Wilayah::pluck('nama_wilayah', 'id');
		$dusun = Dusun::pluck('nama_dusun', 'id');
		$pelanggan = Pelanggan::where('id_pelanggan', '=', $id)->first();

		$meteran = Meteran::join('rts', 'rts.id', '=', 'meterans.rt_id')
			->join('rws', 'rws.id', '=', 'rts.rw_id')
			->join('dusuns', 'dusuns.id', '=', 'rws.dusun_id')
			->join('wilayahs', 'wilayahs.id', '=', 'meterans.wilayah_id')
			->where('meterans.id_pelanggan', '=', $id)
			->get(['meterans.*', 'dusuns.nama_dusun', 'rws.nama_rw', 'rts.nama_rt', 'rws.dusun_id', 'rts.rw_id', 'wilayahs.id', 'wilayahs.nama_wilayah']);

		return view('meteran.meteran', compact(['meteran', 'pelanggan', 'wilayah', 'dusun']));
	}

	public function add(Request $request)
	{
		$latestMet = Meteran::orderBy('created_at', 'DESC')->first();

		$intID = 0;

		if ($latestMet != null) {
			$latestID = substr($latestMet->id_meteran, 1);
			$intID = (int) $latestID;
		}

		$metSeq = str_pad($intID + 1, 5, "0", STR_PAD_LEFT);
		$idmet = 'M' . $metSeq;

		$message = "";
		$met = new Meteran();
		$met->id_pelanggan = $request->id_pelanggan;
		$met->id_meteran = $idmet;
		$met->catatan = $request->catatan;
		$met->lokasi = $request->alamat;
		$met->wilayah_id = $request->wilayah;
		$met->rt_id = $request->rt;
		$met->active = 1;

		$met->save();

		$message = "success";

		return response()->json(["message" => $message], 200);
	}

	public function edit($id, Request $request)
	{
		Meteran::where('id_meteran', $id)->update([
			'lokasi' => $request->alamat,
			'catatan' => $request->catatan,
			'active' => $request->active,
			'rt_id' => $request->rt,
			'wilayah_id' => $request->wilayah
		]);

		return response()->json(["message" => "success"], 200);
	}

	public function delete($id)
	{
		$checkMeteran = CatatMeter::where('meteran_id', '=', $id)->first();
		$checkSisa = SisaBayar::where('id_meteran', '=', $id)->first();
		$checkDenda = Denda::where('id_meteran', '=', $id)->first();

		$message = "";

		if ($checkMeteran == null && $checkSisa == null && $checkDenda == null) {
			Meteran::where('id_meteran', '=', $id)->delete();
			$message = "success";
		} else {
			$message = "Data tidak bisa dihapus karena sedang digunakan pada tabel lain";
		}

		return response()->json(["message" => $message], 200);
	}

}