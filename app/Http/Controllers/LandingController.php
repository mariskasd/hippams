<?php
namespace App\Http\Controllers;

use App\Models\Pengumuman;

class LandingController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pengumuman = Pengumuman::where("active", 1)->get();
		return view('landing.index', compact(['pengumuman']));
	}

}