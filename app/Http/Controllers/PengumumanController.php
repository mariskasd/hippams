<?php

namespace App\Http\Controllers;

use App\Models\Pengumuman;
use Illuminate\Http\Request;

class PengumumanController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pengumuman = Pengumuman::select("*")->orderBy('created_at', 'desc')->get();

		return view('pengumuman.pengumuman', compact(['pengumuman']));
	}

	public function add(Request $request)
	{
		$pengumuman = new Pengumuman();
		$pengumuman->deskripsi = $request->deskripsi;
		$pengumuman->judul = $request->judul;
		$pengumuman->active = $request->active;

		$pengumuman->save();

		return response()->json(["message" => "success"], 200);
	}

	public function edit($id, Request $request)
	{
		Pengumuman::where('id', $id)->update([
			'deskripsi' => $request->deskripsi,
			'judul' => $request->judul,
			'active' => $request->active
		]);

		return response()->json(["message" => "success"], 200);
	}

	public function delete($id)
	{
		Pengumuman::where('id', '=', $id)->delete();

		return response()->json(["message" => "success"], 200);
	}


}