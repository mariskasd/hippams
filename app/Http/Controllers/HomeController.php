<?php

namespace App\Http\Controllers;

use App\Models\Absence;
use App\Models\User;
use App\Models\Transaction;
use App\Models\Pemadaman;
use App\Models\WilayahPemadaman;
use App\Models\Package;
use App\Models\Komplain;
use App\Models\Wilayah;
use App\Models\Pembayaran;
use App\Models\RT;
use Illuminate\Support\Facades\Auth;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class HomeController extends Controller
{

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pemadaman = Pemadaman::select("*")->orderBy('created_at', 'desc')->get();

		$collection = new Collection;

		foreach($pemadaman as $id){

			$wilayahPemadaman =  WilayahPemadaman::where('wilayahpemadamans.pemadaman_id', '=' ,$id->id)
								->join('wilayahs', 'wilayahs.id', '=', 'wilayahpemadamans.wilayah_id')
								->get(['wilayahs.nama_wilayah as wil']);
			
			$wil = "";

			foreach($wilayahPemadaman as $wilpem){
				$wil = $wil.$wilpem->wil.",";
			}

			$collection->push((object)[
				'tanggal_mulai' => $id->tanggal_mulai,
				'tanggal_selesai' => $id->tanggal_selesai,
				'deskripsi' => $id->deskripsi,
				'wilayah' => $wil
			]);

		}

		//get complain
		$komplain = Komplain::join('pelanggans', 'pelanggans.id_pelanggan', '=','komplains.pelanggan_id')
			->where('komplains.status_komplain', '=', 'BARU')
			->orderBy('komplains.updated_at', 'DESC')
			->get(['komplains.*', 'pelanggans.nama as pelanggan', 'pelanggans.id_pelanggan']);


		//pie chart pemakaian bulan sebelumnya berdasarkan wilayah
		// $pie = new Collection;
		// $wilayah = RT::select("*")->orderBy('created_at', 'desc')->get();

		// foreach($wilayah as $wil){

		// 	$sumData = Pembayaran::join('petugas', 'petugas.id_petugas', '=', 'pembayarans.petugas_id')
		// 						 ->join('wilayahpetugas', 'wilayahpetugas.petugas_id','=','petugas.id_petugas')
		// 						 ->join('rts', 'rts.id', '=', 'wilayahpetugas.rt_id')
		// 						 ->where('rts.id', '=', $wil->id)->sum('pembayarans.total_meteran_terpakai');

		// 	$pie->push((object)[
		// 		'value' => $sumData,
		// 		'name' => $wil->nama_wilayah
		// 	]);
		// }
	
		return view('dashboard', compact(['collection', 'komplain']));
	}
}
