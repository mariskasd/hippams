<?php

namespace App\Http\Controllers;

use App\Models\Allowance;
use App\Models\Company;
use App\Models\Employee;
use App\Models\Package;
use App\Models\User;
use App\Models\Transaction;
use App\Models\EmployeeSalaryCut;
use App\Models\EmployeeAllowance;
use App\Models\SalaryCut;
use Illuminate\Http\Request;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Session;

class TransactionController extends Controller
{

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$companyId = Auth::user()->company_id;

		// $user = User::where('company_id', $companyId)->whereNotNull('employee_id')->pluck('employee_id')->toArray();
		// $employee = Employee::whereIn('id', $user)->get();

		$transaction = Transaction::where('company_id', $companyId)->orderBy('created_at', 'desc')->first();
		$packages = Package::where('id', $transaction->packages_id)->first();
		$allTransaction = Transaction::where('company_id', $companyId)->with('packages')->orderBy('created_at', 'desc')->get();

		return view('payment.billing', compact(['packages', 'transaction', 'allTransaction']));
	}

	public function payment(Request $request)
	{
		$date = Carbon::now();
        $date = $date->addDays(30);

		$companyId = Auth::user()->company_id;
		$packages = Package::where('id', $request->packages_id)->first();

		$salary = $packages->salary;
		$approval = $packages->approval;
		Session::put('salary', $salary);
		Session::put('approval', $approval);

		Transaction::create([
			'company_id' => $companyId,
			'packages_id' => $request->packages_id,
			'bank' => $request->bank,
			'price' => $packages->price,
			'expired_date' => $date
		]);

		return response()->json(["message" => "success"], 200);
	}
}
