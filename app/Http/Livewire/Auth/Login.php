<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;
use App\Models\User;
use Session;

class Login extends Component
{
    public $id_pengguna = '';
    public $password = '';
    public $remember_me = false;

    protected $rules = [
        'id_pengguna' => 'required',
        'password' => 'required',
    ];

    public function mount()
    {
        if (auth()->user()) {
            redirect('/dashboard');
        }
    }

    public function login()
    {
        $credentials = $this->validate();
        if (auth()->attempt(['id_pengguna' => $this->id_pengguna, 'password' => $this->password], $this->remember_me)) {
            $user = User::where(["id_pengguna" => $this->id_pengguna])->first();

            auth()->login($user, $this->remember_me);
            return redirect()->intended('/dashboard');


        } else {
            return $this->addError('loginGagal', trans('auth.failed'));
        }
    }

    public function render()
    {
        return view('livewire.auth.login');
    }
}