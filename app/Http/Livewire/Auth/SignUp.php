<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;
use App\Models\User;
use App\Models\Company;
use App\Models\Transaction;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class SignUp extends Component
{
    public $name = '';
    public $email = '';
    public $password = '';

    protected $rules = [
        'name' => 'required|min:3',
        'email' => 'required|email:rfc,dns|unique:users',
        'password' => 'required|min:6'
    ];

    public function mount()
    {
        if (auth()->user()) {
            redirect('/dashboard');
        }
    }

    public function register()
    {
        $this->validate();

        $company = new Company();
        $company->name = $this->name;
        $company->email = $this->email;
        $company->password = Hash::make($this->password);

        $company->save();

        $date = Carbon::now();
        $date = $date->addDays(7);

        $trans = new Transaction();
        $trans->company_id = $company->id;
        $trans->packages_id = 1;
        $trans->bank = "-";
        $trans->price = 0;
        $trans->expired_date = $date;

        $trans->save();

        $user = User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => Hash::make($this->password),
            'company_id' => $company->id
        ]);

        auth()->login($user);

        return redirect('/dashboard');
    }

    public function render()
    {
        return view('livewire.auth.sign-up');
    }
}
