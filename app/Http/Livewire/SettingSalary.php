<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SettingSalary extends Component
{
    public function render()
    {
        return view('livewire.setting-salary');
    }
}
