<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wilayah extends Model
{
    protected $fillable = [
        'nama_wilayah',
        'deskripsi'
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'wilayahs';
}