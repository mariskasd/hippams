<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SisaBayar extends Model
{
    protected $fillable = [
        'sisa_bayar',
        'status_terpakai'
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'sisabayars';
}
