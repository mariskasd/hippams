<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WilayahPetugas extends Model
{
    protected $fillable = [
        'rt_id',
        'petugas_id'
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'wilayahpetugas';
}
