<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatatMeter extends Model
{
    protected $fillable = [
        'meteran_id',
        'petugas_id',
        'total_meteran',
        'total_meteran_terpakai',
        'keterangan'
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'catat_meters';
}