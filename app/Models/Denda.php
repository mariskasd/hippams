<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Denda extends Model
{
    protected $fillable = [
        'denda',
        'status_bayar'
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'dendapembayarans';
}
