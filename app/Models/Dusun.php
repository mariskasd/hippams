<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dusun extends Model
{
    protected $fillable = [
        'nama_dusun',
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'dusuns';
}