<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rw extends Model
{
    protected $fillable = [
        'nama_rw',
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'rws';
}