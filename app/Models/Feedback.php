<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = [
        'id_feedback',
        'id_komplain',
        'antrian',
        'deskripsi',
        'tanggal_perbaikan'
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'feedbacks';
}