<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rt extends Model
{
    protected $fillable = [
        'nama_rt',
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'Rts';
}