<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengumuman extends Model
{
    protected $fillable = [
        'judul',
        'deskripsi',
        'active'
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'pengumumans';
}