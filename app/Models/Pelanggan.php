<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $fillable = [
        'id_pelanggan',
        'nama',
        'no_telp',
        'alamat'
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'pelanggans';
}