<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Petugas extends Model
{
    protected $fillable = [
        'id_petugas',
        'nama',
        'no_telp',
        'alamat',
        'type',
        'password'
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'petugas';
}