<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meteran extends Model
{
    protected $fillable = [
        'id_meteran',
        'catatan',
        'lokasi',
        'active'
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'meterans';
}