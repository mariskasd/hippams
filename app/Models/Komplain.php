<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Komplain extends Model
{
    protected $fillable = [
        'id_komplain',
        'pelanggan_id',
        'category',
        'judul',
        'deskripsi',
        'foto',
        'status_komplain'
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'komplains';
}