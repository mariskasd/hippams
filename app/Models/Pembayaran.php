<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $fillable = [
        'total_tagihan',
        'deskripsi',
        'tanggal_jatuh_tempo'
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'pembayarans';
}
