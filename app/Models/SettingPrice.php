<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingPrice extends Model
{
    protected $fillable = [
        'min_harga',
        'max_harga',
        'min_denda',
        'max_denda'
    ];

    protected $hidden = ["created_at", "updated_at"];

    protected $table = 'settingprice';
}