<?php

namespace App\Exports;

use App\Models\Meteran;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Collection;

class PencatatanExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    // use Exportable;
    public function __construct(string $date, string $rt)
    {
        $this->date = $date;
        $this->rt = $rt;
    }

    public function collection()
    {
        $monthFilter = Carbon::parse($this->date)->translatedFormat('m');
        $yearFilter = Carbon::parse($this->date)->translatedFormat('Y');

        $pencatatanMeteran = new Collection;

        //get data meteran 
        if ($this->date != null) {
            $startDate = $this->date;
        } else {
            $startDate = Carbon::now()->translatedFormat('Y-m');
        }

        $monthFilter = Carbon::parse($startDate)->translatedFormat('m');
        $yearFilter = Carbon::parse($startDate)->translatedFormat('Y');

        //get data catat meter 
        $query = Meteran::join('pelanggans', 'pelanggans.id_pelanggan', '=', 'meterans.id_pelanggan')
            ->join('wilayahpetugas', 'wilayahpetugas.rt_id', 'meterans.rt_id')
            ->join('petugas', 'petugas.id_petugas', '=', 'wilayahpetugas.petugas_id')
            ->leftJoin('catat_meters', function ($join) use ($monthFilter, $yearFilter) {
                $join->on('catat_meters.meteran_id', '=', 'meterans.id_meteran');
                $join->on(function ($query) use ($monthFilter, $yearFilter) {
                    $query->whereMonth('catat_meters.tanggal_pencatatan', '=', $monthFilter);
                    $query->whereYear('catat_meters.tanggal_pencatatan', '=', $yearFilter);
                });

            })
            ->leftJoin('pembayarans', 'pembayarans.catat_meter_id', '=', 'catat_meters.id')
            ->where('petugas.type', 'Lapangan')
            ->where('meterans.active', 1);


        if ($this->rt != null && $this->rt != "All") {

            $query->where('meterans.rt_id', '=', $this->rt);
        }

        $pencatatanMeteran = $query->get(['meterans.id_meteran', 'pelanggans.nama as pelanggan', 'pelanggans.id_pelanggan as id_pel', 'petugas.nama as petugas', 'petugas.id_petugas as id_pet', 'catat_meters.total_meteran', 'catat_meters.total_meteran_terpakai', 'catat_meters.keterangan', 'catat_meters.tanggal_pencatatan', 'catat_meters.id as id_catat']);

        $result = new Collection;
        foreach ($pencatatanMeteran as $item) {
            $result->push((object) [
                'id_meteran' => $item->id_meteran,
                'pelanggan' => $item->pelanggan . "(" . $item->id_pel . ")",
                'petugas' => $item->petugas . "(" . $item->id_pet . ")",
                'total_meteran' => $item->total_meteran == null ? "-" : $item->total_meteran,
                'total_pemakaian' => $item->total_meteran_terpakai == null ? "-" : $item->total_meteran_terpakai,
                'keterangan' => $item->keterangan == null ? "-" : $item->keterangan,
                'tanggal_pencatatan' => $item->tanggal_pencatatan == null ? "-" : Carbon::parse($item->tanggal_pencatatan)->translatedFormat('d-m-Y H:i:s'),
                'status' => $item->id_catat == null ? "BELUM DICATAT" : "SUDAH DICATAT"
            ]);
        }
        // return $pencatatanMeteran;
        return $result;
    }

    public function headings(): array
    {
        return ["ID Meteran", "Pemilik", "Petugas", "Jumlah", "Total Pemakaian", "Keterangan", "Tanggal Pencatatan", "Status"];
    }
}