<?php

namespace App\Exports;

use App\Models\Pembayaran;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class PembayaranExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct(string $idrt, string $date)
    {
        $this->id_rt = $idrt;
        $this->date = $date;
    }
    public function collection()
    {
        $monthFilter = Carbon::parse($this->date)->translatedFormat('m');
        $yearFilter = Carbon::parse($this->date)->translatedFormat('Y');

        $result = new Collection();

        $query = Pembayaran::join('catat_meters', 'catat_meters.id', '=', 'pembayarans.catat_meter_id')
            ->join('petugas', 'pembayarans.petugas_id', '=', 'petugas.id_petugas')
            ->join('meterans', 'meterans.id_meteran', '=', 'catat_meters.meteran_id')
            ->join('pelanggans', 'pelanggans.id_pelanggan', '=', 'meterans.id_pelanggan')
            ->whereMonth('pembayarans.tanggal_jatuh_tempo', '=', $monthFilter)
            ->whereYear('pembayarans.tanggal_jatuh_tempo', '=', $yearFilter);

        if ($this->id_rt != null && $this->id_rt != "All") {

            $query->where('meterans.rt_id', '=', $this->id_rt);

        }


        // if ($this->id_pelanggan != null && $this->id_pelanggan != "All") {

        //     $query->where('pelanggans.id_pelanggan', '=', $this->id_pelanggan);

        //     $idpelanggan = $this->id_pelanggan;

        // } else {

        //     $idpelanggan = "All";
        // }

        // if ($this->id_petugas != null && $this->id_petugas != "All") {

        //     $query->where('petugas.id_petugas', '=', $this->id_petugas);

        //     $idpetugas = $this->id_petugas;
        // } else {

        //     $idpetugas = "All";
        // }

        $pembayaran = $query->orderBy('pembayarans.tanggal_jatuh_tempo', 'DESC')
            ->get(['pembayarans.*', 'catat_meters.*', 'meterans.id_meteran', 'pelanggans.nama as pelanggan', 'pelanggans.id_pelanggan as id_pel', 'petugas.nama as petugas', 'petugas.id_petugas as id_pet']);
        ;

        foreach ($pembayaran as $item) {
            $result->push((object) [
                'id_tagihan' => $item->id_pembayaran,
                'id_meteran' => $item->id_meteran,
                'pelanggan' => $item->pelanggan . "(" . $item->id_pel . ")",
                'petugas' => $item->petugas . "(" . $item->id_pet . ")",
                'tagihan' => $item->total_tagihan,
                'keterangan' => $item->deskripsi == null ? "-" : $item->deskripsi,
                'tanggal_bayar' => $item->tanggal_bayar == null ? "-" : Carbon::parse($item->tanggal_bayar)->translatedFormat('d-m-Y H:i:s'),
                'tanggal' => Carbon::parse($item->tanggal_jatuh_tempo)->translatedFormat('d-m-Y H:i:s'),
                'status' => $item->status_bayar == 1 ? "LUNAS" : "BELUM LUNAS"
            ]);
        }

        return $result;
    }

    public function headings(): array
    {
        return ["ID Tagihan", "ID Meteran", "Pemilik", "Petugas", "Tagihan", "Keterangan", "Tanggal Bayar", "Tanggal Jatuh Tempo", "Status"];
    }
}