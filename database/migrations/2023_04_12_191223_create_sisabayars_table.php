<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSisabayarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sisabayars', function (Blueprint $table) {
            $table->id();
            $table->string('id_pembayaran');
            $table->string('id_meteran');
            $table->float('sisa_bayar');
            $table->boolean('status_terpakai');
            $table->timestamps();

            $table->foreign('id_pembayaran')->references('id_pembayaran')->on('pembayarans');
            $table->foreign('id_meteran')->references('id_meteran')->on('meterans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sisabayars');
    }
}