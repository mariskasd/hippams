<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWilayahpetugasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wilayahpetugas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rt_id');
            $table->string('petugas_id');
            $table->timestamps();

            $table->foreign('rt_id')->references('id')->on('rts');
            $table->foreign('petugas_id')->references('id_petugas')->on('petugas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wilayahpetugas');
    }
}