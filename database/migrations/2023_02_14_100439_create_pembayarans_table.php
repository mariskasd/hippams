<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePembayaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayarans', function (Blueprint $table) {
            $table->string('id_pembayaran')->primary();
            $table->unsignedBigInteger('catat_meter_id');
            $table->string('petugas_id');
            $table->float('harga_satuan');
            $table->float('denda_setting');
            $table->float('total_tagihan')->nullable();
            $table->float('total_meteran_terpakai')->nullable();
            $table->float('total_bayar')->nullable();
            $table->BOOLEAN('status_bayar')->nullable();
            $table->string('deskripsi')->nullable();
            $table->datetime('tanggal_bayar')->nullable();
            $table->datetime('tanggal_jatuh_tempo')->nullable();
            $table->BOOLEAN('sisa_bayar_status')->nullable();
            $table->timestamps();

            $table->foreign('catat_meter_id')->references('id')->on('catat_meters');
            $table->foreign('petugas_id')->references('id_petugas')->on('petugas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayarans');
    }
}