<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->string('id_feedback')->primary();
            $table->string('komplain_id');
            $table->string('deskripsi');
            $table->date('tanggal_perbaikan')->nullable();
            $table->timestamps();

            $table->foreign('komplain_id')->references('id_komplain')->on('komplains');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedbacks');
    }
}