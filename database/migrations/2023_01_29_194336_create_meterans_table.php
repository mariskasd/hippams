<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeteransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meterans', function (Blueprint $table) {
            $table->string('id_meteran')->primary();
            $table->string('id_pelanggan');
            $table->unsignedBigInteger('wilayah_id');
            $table->unsignedBigInteger('rt_id');
            $table->string('catatan');
            $table->string('lokasi');
            $table->BOOLEAN('active');
            $table->timestamps();

            $table->foreign('id_pelanggan')->references('id_pelanggan')->on('pelanggans');
            $table->foreign('wilayah_id')->references('id')->on('wilayahs');
            $table->foreign('rt_id')->references('id')->on('rts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meterans');
    }
}