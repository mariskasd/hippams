<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatatMetersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catat_meters', function (Blueprint $table) {
            $table->id();
            $table->string('meteran_id');
            $table->string('petugas_id');
            $table->datetime('tanggal_pencatatan');
            $table->float('total_meteran');
            $table->float('total_meteran_terpakai');
            $table->string('foto');
            $table->string('keterangan');
            $table->timestamps();

            $table->foreign('meteran_id')->references('id_meteran')->on('meterans');
            $table->foreign('petugas_id')->references('id_petugas')->on('petugas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catat_meters');
    }
}