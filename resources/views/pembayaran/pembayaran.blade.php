@extends('layouts.app')
@section('content')
<div>
    <div class="alert alert-secondary mx-4" role="alert">
        <p class="text-white text-xs mb-1">Anda dapat melakukan manajemen data Tagihan Pembayaran pada halaman ini</p>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-4">
                <div class="card-header pb-0">
                    <form  method="GET" action="{{ url('pembayaran') }}" enctype="multipart/form-data">
                         @csrf
                                 <div class="row">
                                        @if(Auth::user()->role != "Pelanggan")
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="name" class="form-control-label text-sm">RT<sup class="text-danger">*</sup></label>
                                                    <select name="id_rt" id="id_rt" class="form-control">
                                                        <option value="">== Pilih RT ==</option>

                                                        @if($idrt == "All")
                                                            <option value="All" selected>Semua</option>
                                                            @foreach ($rt as $id => $nama_rt)
                                                                <option value="{{ $id }}">{{ $nama_rt }}</option>
                                                            @endforeach
                                                        @else
                                                            <option value="All">Semua</option>
                                                            @foreach ($rt as $id => $nama_rt)
                                                                @if($idrt == $id)
                                                                    <option value="{{ $id }}" selected>{{ $nama_rt }}</option>
                                                                @else
                                                                     <option value="{{ $id }}">{{ $nama_rt }}</option>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="example-date-input" class="form-control-label text-sm">Bulan<sup class="text-danger">*</sup></label>
                                                <input class="form-control" type="month"  id="start_date" name="start_date" value="{{$startDate}}" max="{{ now()->toDateString() }}">
                                            </div>
                                        </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="d-flex flex-row justify-content-between">
                                        <div>
                                            <h5 class="mb-0"></h5>
                                        </div>
                                        <div>
                                            <button type="submit" class="btn bg-gradient-primary btn-sm mb-4" onclick="return validateSearch()" >Filter</button>
                                            @if(Auth::user()->role == "Admin")
                                            <button type="button" class="btn bg-gradient-primary btn-sm mb-4" onclick="return exportExcel()" >Download</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card2 pt-4 mb-4 mx-4">
                <div class="card-header pb-0">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h4 class="mb-0">Daftar Tagihan</h4>
                        </div>
                        @if(Auth::user()->role == "Admin" && $setting != null)
                        <a href="{{ route('buat-pembayaran') }}"  class="btn bg-gradient-primary btn-sm mb-4" type="button" >Buat Tagihan</a>
                        @endif
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-2">
                        <table class="table align-items-center mb-0 ms-2" id="tblTagihan">
                            <thead>
                                <tr>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        ID Tagihan
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        ID Meter
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Pemilik
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Petugas
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Tagihan
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Keterangan
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Jatuh Tempo
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Status
                                    </th>
                                    <th class="text-center text-uppercase  text-sm font-weight-bolder ">
                                        Aksi
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pembayaran as $item)
                                <tr>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->id_pembayaran}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->id_meteran}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->pelanggan}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->petugas}} - {{$item->petugas_idi}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ number_format($item->total_tagihan, 2, ',', '.') }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->deskripsi}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{\Carbon\Carbon::parse($item->tanggal_jatuh_tempo)->translatedFormat('d-m-Y H:i:s')}}</p>
                                    </td>
                                    <td>
                                        @if($item->status_bayar == 1)
                                            <p class="text-xs font-weight-bold mb-0"><b style="color:#4b912f;">LUNAS</b></p>
                                        @else
                                            <p class="text-xs font-weight-bold mb-0"><b style="color:#FF0000;">BELUM LUNAS</b></p>
                                        @endif
                                    </td>
                                    @if(Auth::user()->role != "Pelanggan")
                                    <td class="text-center">
                                      
                                        <a href="{{ route('edit-pembayaran', $item->id_pembayaran,'id') }}" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Edit" >
                                            <i class="fas fa-user-edit text-secondary"></i>
                                        </a>
                                        
                                    </td>
                                    @else
                                    <td class="text-center">
                                      
                                        <a href="{{ route('edit-pembayaran', $item->id_pembayaran,'id') }}" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Detail" >
                                            <i class="fas fa-eye text-secondary"></i>
                                        </a>
                                        
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        $('#tblTagihan').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
        });

        var now = new Date();
        var dateFormating = [
        now.getFullYear(),
        '-',
        now.getMonth() + 1 <10? '0'+ (now.getMonth()+1) : now.getMonth() +1].join('');

        let dateInput = document.getElementById("start_date");
        dateInput.max = dateFormating;

    });

    function exportExcel(){
        // let pelanggan_id = jQuery("#id_pelanggan").val();
        let rt_id = jQuery("#id_rt").val();
        let date = jQuery("#start_date").val();
        window.location="/pembayaran-data/export?id_rt="+ rt_id + "&date=" + date;
    }

    
</script>
@endsection