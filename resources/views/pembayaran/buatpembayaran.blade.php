@extends('layouts.app')
@section('content')
<div>
    <div class="alert alert-secondary mx-4" role="alert">
         <p class="text-white text-xs mb-1">Anda dapat melakukan manajemen data Pengumuman pada halaman ini</p>
    </div>

     <div class="row">
        <div class="col-12">
            <form  method="POST" action="{{ url('buat-tagihan') }}" enctype="multipart/form-data">
             @csrf
                <div class="card mb-4 mx-4">
                    <div class="card-header pb-0">
                         <div class="d-flex flex-row justify-content-between">
                            <div>
                                <h4 class="modal-title" id="exampleModalLongTitle">Buat Tagihan Bulan {{now()->translatedFormat('M Y')}}</h4>
                             </div>
                        </div>
                     </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="modal-body">
                            <hr>
                            <div class="row">
                                 <label for="vehicle1" class="text-sm">Pilih Petugas :</label><br>
                                        <div class="form-group">
                                             @foreach ($wilayahPetugas as $item)
                                                @if($item->notDone > 0)
                                                    <input type="checkbox" id="{{$item->nama_petugas}}" name="{{$item->nama_petugas}}" value="Bike" disabled>
                                                    <label for="vehicle1" class="text-sm"> {{$item->nama_petugas}} [ {{substr_replace($item->nama_rt, "", -1)}} ] ~ Belum Lengkap</label><br>
                                                @elseif(($item->countPembayaran >= $item->count) && $item->notDone == 0)
                                                    <input type="checkbox" id="{{$item->nama_petugas}}" name="{{$item->nama_petugas}}" value="Bike" disabled>
                                                     <label for="vehicle1" class="text-sm"> {{$item->nama_petugas}} [ {{ substr_replace($item->nama_rt, "", -1)}} ]  ~ Pembayaran sudah digenerate</label><br>
                                                @else
                                                    <input type="checkbox" id="id_petugas[]" name="id_petugas[]" value="{{$item->id_petugas}}" required>
                                                    <label for="vehicle1" class="text-sm"> {{$item->nama_petugas}} [ {{substr_replace($item->nama_rt, "", -1)}} ] ~ Lengkap</label><br>
                                                @endif
                                            @endforeach
                                </div>
                                <hr>
               
                            <div class="col-md-12">
                                    @if($countDisable == $wilayahPetugas->count())  
                                    <button type="submit" class="btn btn-primary pull-right" disabled readonly>Buat Data Tagihan</button>
                                    @else
                                    <button type="submit" class="btn btn-primary pull-right">Buat Data Tagihan</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                </div>
             </form>
    

        </div>
    </div>
 
    

</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
@endsection