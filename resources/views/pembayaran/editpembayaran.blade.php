@extends('layouts.app')
@section('content')
<div>
    <div class="alert alert-secondary mx-4" role="alert">
       <p class="text-white text-xs mb-1">Anda dapat melakukan edit pembayaran pada halaman ini</p>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-4">
                <div class="card-body px-0 pt-0 pb-2">
                <form  method="POST" action="{{ url('bayar') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalLongTitle">Pembayaran</h4>
                            </div>
                            <div class="modal-body">
                                @if($pembayaran->status_bayar == false)
                                    @if(Auth::user()->role != "Pelanggan")
                                        <div class="row">
                                                <div class="col-md-5">
                                                    <label for="name" class="form-control-label text-small">ID Pelanggan : {{$pelanggans->id_pelanggan}}</label>
                                                    <br>
                                                    <label for="name" class="form-control-label text-small">ID Meteran : {{$meterans->id_meteran}}</label>
                                                    <br>
                                                    <label for="name" class="form-control-label text-small">Pemilik : {{$pelanggans->nama}} ({{$meterans->catatan}})</label>
                                                    <br>
                                                    <hr> 
                                                    @if($sisaBayar < $total)
                                                    <div class="form-group">
                                                        <label for="name" class="form-control-label text-small">Total Bayar<sup class="text-danger">*</sup></label>
                                                        <input id="total_bayar" name="total_bayar" class="form-control" type="text" placeholder="Masukkan Total Bayar" oninput="return hideSave()" onkeypress="return validate(event)" >
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name" class="form-control-label text-small">Catatan<sup class="text-danger">*</sup></label>
                                                        <textarea id="keterangan" name="keterangan" class="form-control" type="text" placeholder="Masukan Catatan"></textarea>
                                                    </div>
                                                            <button type="button"  class="btn btn-primary" onclick="hitung()">Check Kembalian</button>
                                                            <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLongTitle">Tagihan Terlambat Bayar</h5>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                    @foreach($belumDibayar as $key=>$item)
                                                                        <p class="text-small font-weight-bold mb-0">{{$item->bulan}} - {{ number_format($item->value, 2, ',', '.') }}  <span id="id_{{$key}}">  </span></p>
                                                                        <br>
                                                                    @endforeach
                                                                    
                                                                    </div>
                                                            </div>
                                                        
                                                    
                                                    @endif
                                                    <input type="hidden" name="id_pembayaran" id="id_pembayaran" value="{{$pembayaran->id_pembayaran}}">
                                                    <input type="hidden" name="denda" id="denda" value="{{$dendaNow}}">
                                                    <input type="hidden" name="dayLate" id="dayLate" value="{{$dayLate}}">
                                                    <input type="hidden" name="sisa_bayar" id="sisa_bayar" value="{{$sisaBayar}}">
                                            </div>
                                            <div class="col-md-7 ">
                                                    <div class="form-group">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLongTitle">Rincian Pembayaran {{$tanggalFormating}}</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                            <div class="table-responsive p-2">
                                                                <table class="table align-items-center mb-0">
                                                                    <thead>
                                                                        
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <p class="text-small font-weight-bold mb-0"><b>ID pembayaran</b></p>
                                                                            </td>
                                                                            <td>
                                                                                <p class="text-small font-weight-bold mb-0"><b>{{$pembayaran->id_pembayaran}}</b></p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <p class="text-small mb-0">Harga satuan</p>
                                                                            </td>
                                                                            <td>
                                                                                <p class="text-small mb-0">{{ number_format($pembayaran->harga_satuan, 2, ',', '.') }}</p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <p class="text-small mb-0">Total pemakaian</p>
                                                                            </td>
                                                                            <td>
                                                                                <p class="text-small mb-0">{{$pembayaran->total_meteran_terpakai}}</p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <p class="text-small font-weight-bold mb-0"><b>Total tagihan</b></p>
                                                                            </td>
                                                                            <td>
                                                                                <p class="text-small font-weight-bold mb-0"><b> {{ number_format($pembayaran->total_tagihan, 2, ',', '.') }}</b></p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <p class="text-small mb-0">Denda</p>
                                                                            </td>
                                                                            <td>
                                                                                <p class="text-small mb-0">{{ number_format($pembayaran->denda_setting, 2, ',', '.') }}</p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <p class="text-small mb-0">Total terlambat bayar (hari)</p>
                                                                            </td>
                                                                            <td>
                                                                                <p class="text-small mb-0">{{$dayLate}}</p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <p class="text-small font-weight-bold mb-0"><b>Total denda yang harus dibayarkan</b></p>
                                                                            </td>
                                                                            <td>
                                                                                <p class="text-small font-weight-bold mb-0"><b>{{ number_format($dendaNow, 2, ',', '.') }}</b></p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <p class="text-small mb-0">Sisa bayar sebelumnya</p>
                                                                            </td>
                                                                            <td>
                                                                                <p class="text-small mb-0">{{ number_format($sisaBayar, 2, ',', '.') }}</p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <p class="text-small font-weight-bold mb-0"><b>Total</b></p>
                                                                                <p class="text-small mb-0">Total tagihan + total denda yang harus dibayarkan - sisa bayar sebelumnya</p>
                                                                                <p class="text-small">(dan ditambahkan dengan tagihan terlambat bayar)</p>
                                                                            </td>
                                                                            <td>
                                                                                <b><p class="text-small font-weight-bold mb-0" id="total_tag"></p></b>
                                                                                <input type="hidden" name="total" id="total">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <p class="text-small font-weight-bold mb-0"><b>Kembalian</b></p>
                                                                            </td>
                                                                            @if($sisaBayar > $total)
                                                                                <td>
                                                                                    <p class="text-small font-weight-bold mb-0" name="kembalianLbl" id="kembalianLbl"><b>{{$kembalian}}</b></p>
                                                                                    <input type="hidden" name="kembalian" id="kembalian" value="{{$kembalian}}">
                                                                                </td>
                                                                            @else
                                                                                <td>
                                                                                    <p class="text-small font-weight-bold mb-0" name="kembalianLbl" id="kembalianLbl"><b>0</b></p>
                                                                                    <input type="hidden" name="kembalian" id="kembalian" value="0">
                                                                                </td>
                                                                            @endif
                                                                            
                                                                        </tr>
                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                            <hr>
                                                            @if($sisaBayar > $total)
                                                            <input type="hidden" id="sisaGede" name="sisaGede" >
                                                            <p class="text-small font-weight-bold mb-0"><i style="color:red;">* semua kembalian yang berasal dari sisa bayar tidak dapat diuangkan dan hanya dapat digunakan pada pembayaran selanjutnya</i></p>
                                                            @else
                                                            <input type="checkbox" id="useSisa" name="useSisa" value="false">
                                                            <label for="vehicle1" id="useSisaLabel"> Gunakan kembalian sebagai sisa bayar bulan depan</label><br>
                                                            @endif
                                                        
                                                            </div>
                                                            <br>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    @else
                                    <div class="row">
                                       <div class="col-md-12 ">
                                            <label for="name" class="form-control-label text-small">ID Pelanggan : {{$pelanggans->id_pelanggan}}</label>
                                            <br>
                                            <label for="name" class="form-control-label text-small">ID Meteran : {{$meterans->id_meteran}}</label>
                                            <br>
                                            <label for="name" class="form-control-label text-small">Pemilik : {{$pelanggans->nama}} ({{$meterans->catatan}})</label>
                                            <br>
                                            <hr>
                                            <div class="form-group">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">Rincian Pembayaran {{$tanggalFormating}}</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                    <div class="table-responsive p-2">
                                                        <table class="table align-items-center mb-0">
                                                            <thead>
                                                                
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b>ID pembayaran</b></p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b>{{$pembayaran->id_pembayaran}}</b></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small mb-0">Harga satuan</p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small mb-0">{{ number_format($pembayaran->harga_satuan, 2, ',', '.') }}</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small mb-0">Total pemakaian</p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small mb-0">{{$pembayaran->total_meteran_terpakai}}</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b>Total tagihan</b></p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b> {{ number_format($pembayaran->total_tagihan, 2, ',', '.') }}</b></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small mb-0">Denda</p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small mb-0">{{ number_format($pembayaran->denda_setting, 2, ',', '.') }}</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small mb-0">Total terlambat bayar (hari)</p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small mb-0">{{$dayLate}}</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b>Total denda yang harus dibayarkan</b></p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b>{{ number_format($dendaNow, 2, ',', '.') }}</b></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small mb-0">Sisa bayar sebelumnya</p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small mb-0">{{ number_format($sisaBayar, 2, ',', '.') }}</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b>Total</b></p>
                                                                        <p class="text-small mb-0">Total tagihan + total denda yang harus dibayarkan - sisa bayar sebelumnya</p>
                                                                        <p class="text-small">(dan ditambahkan dengan tagihan terlambat bayar)</p>
                                                                    </td>
                                                                    <td>
                                                                        <b><p class="text-small font-weight-bold mb-0" id="total_tag"></p></b>
                                                                        <input type="hidden" name="total" id="total">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b>Kembalian</b></p>
                                                                    </td>
                                                                    @if($sisaBayar > $total)
                                                                        <td>
                                                                            <p class="text-small font-weight-bold mb-0" name="kembalianLbl" id="kembalianLbl"><b>{{$kembalian}}</b></p>
                                                                            <input type="hidden" name="kembalian" id="kembalian" value="{{$kembalian}}">
                                                                        </td>
                                                                    @else
                                                                        <td>
                                                                            <p class="text-small font-weight-bold mb-0" name="kembalianLbl" id="kembalianLbl"><b>0</b></p>
                                                                            <input type="hidden" name="kembalian" id="kembalian" value="0">
                                                                        </td>
                                                                    @endif
                                                                    
                                                                </tr>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                    <hr>
                                                    @if($sisaBayar > $total)
                                                    <input type="hidden" id="sisaGede" name="sisaGede" >
                                                    <p class="text-small font-weight-bold mb-0"><i style="color:red;">* semua kembalian yang berasal dari sisa bayar tidak dapat diuangkan dan hanya dapat digunakan pada pembayaran selanjutnya</i></p>
                                                    @else
                                                    <input type="checkbox" id="useSisa" name="useSisa" value="false">
                                                    <label for="vehicle1" id="useSisaLabel"> Gunakan kembalian sebagai sisa bayar bulan depan</label><br>
                                                    @endif
                                                
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @else
                                <div class="row">
                                       <div class="col-md-12 ">
                                            <label for="name" class="form-control-label text-small">ID Pelanggan : {{$pelanggans->id_pelanggan}}</label>
                                            <br>
                                            <label for="name" class="form-control-label text-small">ID Meteran : {{$meterans->id_meteran}}</label>
                                            <br>
                                            <label for="name" class="form-control-label text-small">Pemilik : {{$pelanggans->nama}} ({{$meterans->catatan}})</label>
                                            <br>
                                            <label for="name" class="form-control-label text-small">Tanggal Bayar : {{\Carbon\Carbon::parse($pembayaran->tanggal_bayar)->translatedFormat('d-m-Y H:i:s')}}</label>
                                            <br>
                                            <hr>
                                            <div class="form-group ">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="exampleModalLongTitle">Rincian Pembayaran {{$tanggalFormating}}</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                    <div class="table-responsive p-2">
                                                        <table class="table align-items-center mb-0">
                                                            <thead>
                                                                
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b>ID pembayaran</b></p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b>{{$pembayaran->id_pembayaran}}</b></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small mb-0">Harga satuan</p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small mb-0">{{ number_format($pembayaran->harga_satuan, 2, ',', '.') }}</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small mb-0">Total pemakaian</p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small mb-0">{{$pembayaran->total_meteran_terpakai}}</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b>Total tagihan</b></p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b> {{ number_format($pembayaran->total_tagihan, 2, ',', '.') }}</b></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small mb-0">Denda</p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small mb-0">{{ number_format($pembayaran->denda_setting, 2, ',', '.') }}</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small mb-0">Total terlambat bayar (hari)</p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small mb-0">{{$dayLate}}</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b>Total denda yang harus dibayarkan</b></p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b>{{ number_format($dendaNow, 2, ',', '.') }}</b></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small mb-0">Sisa bayar sebelumnya</p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small mb-0">{{ number_format($sisaBayar, 2, ',', '.') }}</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    <p class="text-small font-weight-bold mb-0"><b>Total</b></p>
                                                                        <p class="text-small">(Total tagihan + total denda yang harus dibayarkan - sisa bayar sebelumnya)</p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0" ><b>{{ number_format((($total+$dendaNow)-$sisaBayar), 2, ',', '.') }}</b></p>
                                                                        <input type="hidden" name="total" id="total" value="{{$total}}">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0"><b>Total Bayar</b></p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0" ><b>{{ number_format($pembayaran->total_bayar, 2, ',', '.') }}</b></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        @if($pembayaran->sisa_bayar_status == 1)
                                                                        <p class="text-small font-weight-bold mb-0"><b>Kembalian (digunakan untuk pembayaran bulan selanjutnya)</b></p>
                                                                        @else
                                                                        <p class="text-small font-weight-bold mb-0"><b>Kembalian</b></p>
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        <p class="text-small font-weight-bold mb-0" ><b>{{ number_format($kembalian, 2, ',', '.') }}</b></p>
                                                                    </td>
                                                                </tr>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                @endif
                            </div>
                            @if($pembayaran->status_bayar == false  && Auth::user()->role != "Pelanggan")
                                <div class="modal-footer">
                                    <input type="hidden" id="isTerlambat" name="isTerlambat" value="0">
                                    <!-- <button type="button" class="btn btn-primary" onclick="submit()">Simpan</wbutton> -->
                                    <button type="submit" id="saveBayar" class="btn btn-primary">Simpan</wbutton>
                                </div>
                            @endif
                            
                </div>
                </form>
                </div>
            </div>
        </div>
    </div>

</div>
<style>
    .text-small{
        font-size:12px;
    }
</style>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
            event.preventDefault();
            return false;
            }
        });
        $.ajaxSetup({
         headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

        jQuery("#useSisa").hide(); 
        jQuery("#useSisaLabel").hide(); 

        let total = jQuery("#total").val();
        let sisaBayar = jQuery("#sisa_bayar").val();
        jQuery("#saveBayar").hide();

        var sumTotalTerlambat = {!! $belumDibayar->toJson() !!};
        var totalTagihanNow = {!! $total !!};
        var openPembayaranNow = {!! $pembayaran->toJson() !!};
        var setting = {!! $setting->toJson() !!};

        let datePembayaran = new Date(openPembayaranNow.tanggal_jatuh_tempo);
        let dateNow = new Date();
        console.log(datePembayaran)
        let total_tagihan = 0;
        let now = new Date();
        jQuery.each( sumTotalTerlambat, function( key, value ) {
            total_tagihan += value.value;
            let tglTerlambat = new Date(value.jatuh_tempo);
            if((tglTerlambat !== now) && (tglTerlambat < datePembayaran) ){
                let Difference_In_Time = dateNow.getTime() - tglTerlambat.getTime();
                let Difference_In_Days = parseInt(Difference_In_Time / (1000 * 3600 * 24));
                if(Difference_In_Days < 7){
                    let denda = Difference_In_Days * setting.min_denda;
                    total_tagihan += denda;
                    jQuery('#id_' + key).show()
                    jQuery('#id_' + key).text('( + ' + denda + ' denda ) - Terlambat ' + Difference_In_Days + ' hari') 
                }else{
                    total_tagihan += setting.max_denda;
                    jQuery('#id_' + key).show()
                    jQuery('#id_' + key).text('( + ' + setting.max_denda + ' denda ) - Terlambat ' + Difference_In_Days + ' hari')
                }
            }else{
                jQuery('#id_' + key).hide()
            } 
        });

        if(sumTotalTerlambat.length > 0){
            jQuery("#isTerlambat").val(1);
        }else{
            jQuery("#isTerlambat").val(0);
        }

        let total_final = total_tagihan + totalTagihanNow;
        let newTotal = total_final.toLocaleString('id-ID', {currency: 'IDR' });
        jQuery("#total_tag").text(newTotal);
        jQuery("#total").val(parseFloat(total_final));

        if(sisaBayar > total){
            jQuery("#sisaGede").val(true);
        }else{
            jQuery("#sisaGede").val();
        }

    });

    function validate(evt){
        jQuery("#saveBayar").hide();
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode > 31 && charCode != 46 && (charCode < 48 || charCode >= 57))
        
        return false;
        
    }

    function hideSave(){
        jQuery("#saveBayar").hide();
    }

    function hitung(){
        let total = parseInt(jQuery("#total").val());
        let totalBayar = parseInt(jQuery("#total_bayar").val());
        console.log(total)
        console.log(totalBayar)

        if(totalBayar == "" || totalBayar == null){
            swal.fire("Gagal !", "Total Bayar belum diisi", "error");
        }
        else if(totalBayar < total){
            swal.fire("Gagal !", "Total Bayar tidak mencukupi untuk membayar tagihan", "error");
        }else{
            let kem = totalBayar - total;
            if(kem <= 0){
                jQuery("#useSisa").hide(); 
                jQuery("#useSisaLabel").hide(); 
            }else{
                jQuery("#useSisa").show(); 
                jQuery("#useSisaLabel").show(); 
            }
            jQuery("#kembalianLbl").text(kem);
            jQuery("#kembalian").val(kem);
            jQuery("#saveBayar").show();
        }   
    }

    
</script>
@endsection
