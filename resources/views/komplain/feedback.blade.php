@extends('layouts.app')
@section('content')
<div>
    <div class="alert alert-secondary mx-4" role="alert">
       <p class="text-white text-xs mb-1">Anda dapat melakukan feedback komplain pada halaman ini</p>
    </div>
    @if($feedback != null)  
    <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-4">
                <div class="card-body px-0 pt-0 pb-2">
                <form  method="POST" action="{{ url('edit-feedback') }}" enctype="multipart/form-data">
                 @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalLongTitle">Feedback</h4>
                                <!-- @if($komplain->status_komplain === "BARU")
                                <button class="btn bg-gradient-primary btn-sm mb-4" type="button"><a href="{{ route('update-feedback-progress', $komplain->id_komplain,'id') }}">Update Status</a></button>
                                @endif -->
                               
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Pelanggan</label>
                                            <input class="form-control" type="text" value="{{$komplain->pelanggan_id}} - {{$komplain->pelanggan}}" readonly >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Deskripsi<sup class="text-danger">*</sup></label>
                                            <textarea id="keterangan" name="keterangan" class="form-control" type="text" placeholder="Masukan Keterangan" readonly >{{$komplain->deskripsi}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Kategori</label>
                                            <input class="form-control" type="text" value="{{$komplain->category}}" readonly >
                                        </div>                                   
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Judul</label>
                                            <input class="form-control" type="text" value="{{$komplain->judul}}" readonly >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Lampiran :</label>
                                            <br>
                                            <img src="../storage/data_meteran/{{$komplain->foto}}" alt="..." class="w-10 border-radius-sm shadow-sm" >
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="id_komplain" name="id_komplain" value="{{$komplain->id_komplain}}">
                                <input type="hidden" id="id_feedback" name="id_feedback" value="{{$feedback->id_feedback}}">
                                <hr>
                                <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="form-control-label text-sm">No.Komplain<sup class="text-danger">*</sup></label>
                                                <input class="form-control" type="text" name="antrian" id="antrian" readonly value="{{$komplain->id_komplain}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="form-control-label text-sm">Keterangan<sup class="text-danger">*</sup></label>
                                                @if($komplain->status_komplain != "Selesai" && Auth::user()->role == "Admin")
                                                <textarea id="keterangan" name="keterangan" class="form-control" type="text" placeholder="Masukan Keterangan" required >{{$feedback->deskripsi}}</textarea>
                                                @else
                                                <textarea  class="form-control" type="text" readonly >{{$feedback->deskripsi}}</textarea>
                                                @endif
                                               
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col-md-6">
                                            @if($komplain->category == "Kerusakan")  
                                            <div class="form-group">
                                                <label for="example-date-input" class="form-control-label text-sm">Tanggal Perbaikan<sup class="text-danger">*</sup></label>
                                                @if($komplain->status_komplain != "Selesai" && Auth::user()->role == "Admin")
                                                <input class="form-control" type="datetime-local"  id="tanggal_perbaikan" name="tanggal_perbaikan" value="{{$feedback->tanggal_perbaikan}}" required>
                                                @else
                                                <input class="form-control" type="datetime-local"   value="{{$feedback->tanggal_perbaikan}}" readonly>
                                                @endif

                                                @if(Auth::user()->role == "Pelanggan")
                                                    <p class="text-danger" style="font-size:14px">*Hubungi Admin (+6281666767545) untuk konfirmasi atau apabila ingin mengganti tanggal perbaikan</p>
                                                @endif
                                            </div>
                                            @endif
                                        </div>
                                        @if($komplain->status_komplain != "SELESAI" && Auth::user()->role == "Admin")
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <br>
                                                    <button type="submit" class="btn btn-primary text-white">Simpan</button>

                                                    <button class="btn btn-danger text-white" type="button"><a class="text-white" href="{{ route('update-feedback-progress', $komplain->id_komplain,'id') }}">Tutup Komplain</a></button>
                                                </div>
                                            </div>
                                        @endif  
                                </div> 
                            </div>
                        </div>
                </form>
                </div>
            </div>
        </div>
    </div>
	@endif

    @if($feedback == null)  

    <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-4">
                <div class="card-body px-0 pt-0 pb-2">
                <form  method="POST" action="{{ url('add-feedback') }}" enctype="multipart/form-data">
                 @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalLongTitle">Feedback</h4>
                                <!-- @if($komplain->status_komplain === "BARU")
                                <button class="btn bg-gradient-primary btn-sm mb-4" type="button"><a href="{{ route('update-feedback-progress', $komplain->id_komplain,'id') }}">Update Status</a></button>
                                @endif -->
                               
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Pelanggan</label>
                                            <input class="form-control" type="text" value="{{$komplain->pelanggan_id}} - {{$komplain->pelanggan}}" readonly >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Deskripsi<sup class="text-danger">*</sup></label>
                                            <textarea class="form-control" type="text" placeholder="Masukan Keterangan" readonly >{{$komplain->deskripsi}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Kategori</label>
                                            <input class="form-control" id="kategori" name="kategori" type="text" value="{{$komplain->category}}" readonly >
                                        </div>                                   
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Judul</label>
                                            <input class="form-control" type="text" value="{{$komplain->judul}}" readonly >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Lampiran :</label><br>
                                            <img src="../storage/data_meteran/{{$komplain->foto}}" alt="..." class="w-10 border-radius-sm shadow-sm" >
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="id_komplain" name="id_komplain" value="{{$komplain->id_komplain}}">
                                <hr>
                                <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="form-control-label text-sm">No. Komplain<sup class="text-danger">*</sup></label>
                                                <input class="form-control" type="text" name="antrian" id="antrian" readonly value="{{$komplain->id_komplain}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name" class="form-control-label text-sm">Deskripsi<sup class="text-danger">*</sup></label>
                                                <textarea id="keterangan" name="keterangan" class="form-control" type="text" placeholder="Masukan Keterangan" ></textarea>
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col-md-6">
                                         @if($komplain->category == "Kerusakan")  
                                            <div class="form-group">
                                                <label for="example-date-input" class="form-control-label text-sm">Tanggal Perbaikan<sup class="text-danger">*</sup></label>
                                                <input class="form-control" type="datetime-local"  id="tanggal_perbaikan" name="tanggal_perbaikan">
                                            </div>
                                         @endif
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <br>
                                                @if(Auth::user()->role != "Pelanggan")
                                                <button type="submit" onclick="return validate()" class="btn btn-primary">Simpan</wbutton>
                                                @endif
                                            </div>
                                        </div>
                                </div> 
                            </div>
                        </div>
                </form>
                </div>
            </div>
        </div>
    </div>

	@endif

</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        $.ajaxSetup({
         headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

        getPelanggan();

        var now = new Date();
        var dateFormating = [
        now.getFullYear(),
        '-',
        now.getMonth() + 1 <10? '0'+ (now.getMonth()+1) : now.getMonth() +1,
        '-',
        now.getDate() <10? '0'+ now.getDate() : now.getDate(),
        'T',
        now.getHours() <10? '0'+ now.getHours() : now.getHours(),
        ':',
        now.getMinutes() <10? '0'+ now.getMinutes() : now.getMinutes(),
        ].join('');

        let dateInput = document.getElementById("tanggal_perbaikan");
        dateInput.min = dateFormating;

    });

    function validate(){
        let keterangan = jQuery("#keterangan").val();
        let tanggal_perbaikan = jQuery("#tanggal_perbaikan").val();
        let kategori = jQuery("#kategori").val();

        if(kategori == "Kerusakan"){
                    if ((keterangan == "" || keterangan == null) || (tanggal_perbaikan == "" || tanggal_perbaikan == null) ) {

                        swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");

                        return false;
                    }
        }else{

            if (keterangan == "" || keterangan == null) {

            swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");

            return false;
            }
        } 
    }

    function getPelanggan(){
        // jQuery('#meteran_id').on('change', function() {
            let url = '{{route("getPelanggan")}}';
            let meteran = jQuery("#meteran_id").val();

            console.log(meteran);

            var data = new FormData();
            data.append("meteran", meteran);

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                data:data,
                processData: false,
                contentType: false,
                success: function(response) {

                    jQuery("#pemilik").val(response.nama);
                    jQuery("#pelanggan_id").val(response.id_pelanggan);
                    
                },
                error: function(e) {
                    console.log(e);
                }
            });
        // });
    }
</script>
@endsection