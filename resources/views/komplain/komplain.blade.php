@extends('layouts.app')
@section('content')
<div>
    <div class="alert alert-secondary mx-4" role="alert">
       <p class="text-white text-xs mb-1">Anda dapat melakukan pengajuan komplain pada halaman ini</p>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-4">
                <div class="card-header pb-0">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h4 class="mb-0">Daftar Komplain</h4>
                        </div>
                        <button class="btn bg-gradient-primary btn-sm mb-4" type="button" onclick="addModal()">+&nbsp; Tambah</button>
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-2">
                        <table class="table align-items-center mb-0 ms-2" id="tblKomplain">
                            <thead>
                                <tr>
                                   <th class="text-uppercase text-sm font-weight-bolder  ps-2">
                                        Kategori
                                    </th>
                                    <th class="text-uppercase text-sm font-weight-bolder  ps-2">
                                        Judul
                                    </th>
                                    <th class="text-uppercase text-sm font-weight-bolder  ps-2">
                                        Pelanggan
                                    </th>
                                    <th class="text-uppercase text-sm font-weight-bolder  ps-2">
                                        Status
                                    </th>
                                    <th class="text-center text-uppercase text-sm font-weight-bolder ">
                                        Aksi
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($komplain as $item)
                                <tr>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->category}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->judul}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->id_pelanggan}} - {{$item->pelanggan}}</p>
                                    </td>
                                    @if($item->status_komplain === "BARU")         
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0"><b  style="color:#327da8;">{{$item->status_komplain}}</b></p>
                                        </td>  
                                    @elseif($item->status_komplain === "SELESAI")         
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0"><b style="color:#4b912f;">{{$item->status_komplain}}</b></p>
                                        </td>    
                                    @elseif($item->status_komplain === "DIPROSES")         
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0"><b style="color:#c9772e;">{{$item->status_komplain}}</b></p>
                                        </td> 
                                    @else
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0" >{{$item->status_komplain}}</p>
                                        </td> 
                                    @endif
                                    
                                    <td class="text-center">
                                        @if(Auth::user()->role != "Pelanggan" || (Auth::user()->role == "Pelanggan" && $item->status_komplain != "BARU"))
                                        <a href="{{ route('feedback', $item->id_komplain,'id') }}" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Feedback" >
                                            <i class="fas fa fa-comments text-secondary"></i>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="modal fade" id="modalKomplain" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    
            <div class="modal-dialog modal-dialog-centered" role="document">
                
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Komplain</h5>
                            <button type="button" class="btn close" onclick="addModal('close')">
                                <span aria-hidden="true">X</span>
                            </button>
                        </div>
                        <form  method="POST" action="{{ url('add-komplain') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name" class="form-control-label">Kategori<sup class="text-danger">*</sup></label>
                                        <select name="category" id="category" class="form-control" required>
                                            <option value="">== Pilih Kategori ==</option>
                                                <option value="Kerusakan">Kerusakan</option>
                                                <option value="Tagihan">Tagihan</option>
                                                <option value="Lain lain">Lain lain</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name" class="form-control-label">Pelanggan<sup class="text-danger">*</sup></label>
                                        @if(Auth::user()->role != "Pelanggan")
                                        <select name="pelanggan_id" id="pelanggan_id" class="form-control" required>
                                                <option value="">== Pilih Pelangan ==</option>
                                                @foreach ($pelanggan as $item => $id_pelanggan)
                                                    <option value="{{ $id_pelanggan }}">{{  $id_pelanggan }}</option>
                                                @endforeach
                                        </select>
                                        @else
                                            <input class="form-control" type="text" id="pelanggan_id" name="pelanggan_id" readonly>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-date-input" class="form-control-label">Judul<sup class="text-danger">*</sup></label>
                                        <input class="form-control" type="text" id="judul" name="judul" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name" class="form-control-label">Deskripsi<sup class="text-danger">*</sup></label>
                                        <textarea id="keterangan" name="keterangan" class="form-control" type="text" placeholder="Masukan Keterangan" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name" class="form-control-label">Lampiran<sup class="text-danger">*</sup></label>
                                        <input id="image" name="image" class="form-control" type="file" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" onclick="return validateRequired()">Simpan</wbutton>
                        </div>
                        
                    </form>
                    </div>
            </div>

        </div>


</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        $.ajaxSetup({
         headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

        $('#tblKomplain').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
        });

        

    });

    function addModal(close){
        if (close) {
            jQuery("#modalKomplain").modal('hide');
        }   else{
            jQuery("#category").val("");
            let id_pel = {!! json_encode(Auth::user()->id_pengguna) !!};
        let roles = {!! json_encode(Auth::user()->role) !!};

        if(roles == "Pelanggan"){
            console.log("here")
            jQuery("#pelanggan_id").val(id_pel);
        }else{
            jQuery("#pelanggan_id").val("");
        }
            
            jQuery("#judul").val("");
            jQuery("#keterangan").val("");
            jQuery("#image").val("");
            jQuery("#modalKomplain").modal('show');
        }
    }

    function validateRequired(){
        let cat = jQuery("#category").val();
        let pel = jQuery("#pelanggan_id").val();
        let judul = jQuery("#judul").val();
        let ket = jQuery("#keterangan").val();
        let img = jQuery("#image").val();

        if((cat == null || cat == "") || (pel == null || pel == "") || (judul == null || judul == "") || (ket == null || ket == "") || (img == null || img == "")){
            swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");
            return false;
        }
    }

    function getPelanggan(){
        // jQuery('#meteran_id').on('change', function() {
            let url = '{{route("getPelanggan")}}';
            let meteran = jQuery("#meteran_id").val();

            console.log(meteran);

            var data = new FormData();
            data.append("meteran", meteran);

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                data:data,
                processData: false,
                contentType: false,
                success: function(response) {

                    jQuery("#pemilik").val(response.nama);
                    jQuery("#pelanggan_id").val(response.id_pelanggan);
                    
                },
                error: function(e) {
                    console.log(e);
                }
            });
    }
</script>
@endsection