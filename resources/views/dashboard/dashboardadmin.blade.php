@extends('layouts.app')
@section('content')
<style>
    .bg {
        background-color: #EDF2F7;
    }

    .table-line thead {
        border-bottom: 2px solid #627594;
    }

    .card-head {
        padding: 1rem 1rem;
        font-weight: bold;
    }
</style>
@if(Auth::user()->role == "Admin")
<div class="alert alert-secondary mx-4" role="alert">
    <h3 class="text-white text-xs mb-1">Dashboard Admin</h3>
    <!-- <a class="text-white text-xs mb-1" href="{{ route('update-pass-user') }}">AddMeteran</a> -->
</div>
<div class=" mb-4 mx-4">
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="card mb-4 mx-4">
                <div class="card-header pb-0">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h5 class="mb-0">Pendapatan {{$yearNow}}</h5>
                        </div>

                    </div>
                </div>
                <hr>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="row ">
                        <canvas id="myPendapatan" style="width:100%;max-width:600px"></canvas>
                    </div>
                </div>
            </div>
                <div class="card mb-4 mx-4">
                    <div class="card-header pb-0">
                        <div class="d-flex flex-row justify-content-between">
                            <div>
                                <h5 class="mb-0">Komplain</h5>
                            </div>

                        </div>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <hr>
                        @if($komplain->count() > 0)
                        <div class="table-responsive p-2 ps-3">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-xs font-weight-bolder  ps-2">
                                            Kategori
                                        </th>
                                        <th class="text-uppercase text-xs font-weight-bolder  ps-2">
                                            Judul
                                        </th>
                                        <th class="text-uppercase text-xs font-weight-bolder  ps-2">
                                            Pelanggan
                                        </th>
                                        <th class="text-uppercase text-xs font-weight-bolder  ps-2">
                                            Tanggal Komplain
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($komplain as $item)
                                    <tr>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{$item->category}}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{$item->judul}}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{$item->id_pelanggan}} - {{$item->pelanggan}}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{\Carbon\Carbon::parse($item->created_at)->translatedFormat('d-m-Y H:i:s')}}</p>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <p class="text-center">
                            Tidak ada komplain
                        </p>
                        @endif
                    </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card mb-4 mx-4">
                <div class="card-header pb-0">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h5 class="modal-title" id="exampleModalLongTitle">Pemakaian {{$pieMonth}}</h5>
                        </div>

                    </div>
                </div>
                <hr>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="row ">
                        @if($pie->count() > 0)
                            <canvas id="myPiepemakaian" style="width:100%;height:500px"></canvas>
                        @else
                            <p class="text-center">
                                Tidak ada pemakaian
                            </p>
                        @endif  
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if(Auth::user()->role == "Pelanggan")
    <div class="alert alert-secondary mx-4" role="alert">
        <h3 class="text-white text-xs mb-1">Dashboard Pelanggan</h3>
    </div>
    <div class=" mb-4 mx-4">
        <br>
        <div class="row">
            <div class="col-md-6">
                <div class="card mb-4 mx-4">
                    <div class="card-header pb-0">
                        <div class="d-flex flex-row justify-content-between">
                            <div>
                                <h5 class="mb-0">Pemakaian {{$yearNow}}</h5>
                            </div>

                        </div>
                    </div>
                    <hr>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="row ">
                            <canvas id="myPemakaian" style="width:100%;max-width:600px"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card mb-4 mx-4">
                    <div class="card-header pb-0">
                        <div class="d-flex flex-row justify-content-between">
                            <div>
                                <h5 class="mb-0">Tagihan Belum Dibayar</h5>
                            </div>

                        </div>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <hr>
                        @if($pembayaranNotDone->count() > 0)
                        <ol class="font-weight-bold text-black">
                            @foreach($pembayaranNotDone as $item)
                            <li style="width: 94%;font-size:12px;">
                                <p class="font-weight-bold text-dark" style="font-size:14px;">Bulan {{\Carbon\Carbon::parse($item->tanggal_jatuh_tempo)->translatedFormat('M Y')}}</p>
                                <p style="font-size:14px;">Id Meteran :  <span class="font-weight-bold text-dark">{{$item->meteran_id}}</span></p>
                                <p style="font-size:14px;">Total Tagihan :  <span class="font-weight-bold text-dark">{{$item->total_tagihan}}</span></p>
                                <p style="font-size:14px;">Jatuh Tempo :  <span class="font-weight-bold text-dark">{{\Carbon\Carbon::parse($item->tanggal_jatuh_tempo)->translatedFormat('d-m-Y')}}</span></p>
                                <hr>
                            </li>
                            @endforeach
                        </ol>
                        @else
                        <p class="text-center">
                            Tidak ada tagihan yang belum dibayarkan
                        </p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card mb-4 mx-4">
                    <div class="card-header pb-0">
                        <div class="d-flex flex-row justify-content-between">
                            <div>
                                <h5 class="mb-0">Feedback Komplain</h5>
                            </div>

                        </div>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <hr>
                        @if($feedback->count() > 0)
                        <div class="table-responsive p-2">
                            <table class="table align-items-center mb-0 ms-2" style="width: 97%;">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                            Kategori
                                        </th>
                                        <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                            Judul
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($feedback as $item)
                                    <tr>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{$item->category}}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{$item->judul}}</p>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <p class="text-center">
                            Tidak ada feedback komplain
                        </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if(Auth::user()->role == "Lapangan")
    <div class="alert alert-secondary mx-4" role="alert">
        <h3 class="text-white text-xs mb-1">Dashboard Petugas Lapangan</h3>
    </div>
    <div class=" mb-4 mx-4">
        <br>
        <div class="row">
            <div class="col-md-6">
                <div class="card mb-4 mx-4">
                    <div class="card-header pb-0">
                        <div class="d-flex flex-row justify-content-between">
                            <div>
                                <h5 class="mb-0">Pemakaian {{$yearNow}}</h5>
                            </div>

                        </div>
                    </div>
                    <hr>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="row ">
                            <canvas id="myPemakaian" style="width:100%;max-width:600px"></canvas>


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card mb-4 mx-4">
                    <div class="card-header pb-0">
                        <div class="d-flex flex-row justify-content-between">
                            <div>
                                <h5 class="mb-0">Pencatatan {{\Carbon\Carbon::now()->translatedFormat('F Y')}}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <hr>
                        <ol class="font-weight-bold text-black">
                            @foreach($pencatatanNotDone as $item)
                            <li style="width: 94%;font-size:14px">
                                <p class="font-weight-bold text-dark" style="font-size:14px;">RT {{$item->rt}}</p>
                                <p style="font-size:14px;">Belum Dicatat : <span class="font-weight-bold text-dark">{{$item->notDone}}</span></p>
                                <p style="font-size:14px;">Selesai : <span class="font-weight-bold text-dark">{{$item->done}}</span></p>
                                <hr>
                            </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if(Auth::user()->role == "Pembayaran")
    <div class="alert alert-secondary mx-4" role="alert">
        <h3 class="text-white text-xs mb-1">Dashboard Petugas Pembayaran</h3>
    </div>
    <div class=" mb-4 mx-4">
        <br>
        <div class="row">
            <div class="col-md-6">
                <div class="card mb-4 mx-4">
                    <div class="card-head pb-0">
                        <div class="d-flex flex-row justify-content-between">
                            <div>
                                <h5 class="mb-0">Pendapatan {{$yearNow}}</h5>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="row ">
                            <canvas id="myPendapatan" style="width:100%;max-width:600px"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card mb-4 mx-4">
                    <div class="card-head pb-0">
                        <div class="d-flex flex-row justify-content-between">
                            <div>
                                <h5 class="mb-0">Pembayaran {{\Carbon\Carbon::now()->translatedFormat('F Y')}}</h5>
                            </div>

                        </div>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <hr>
                        <ol class="font-weight-bold text-black">
                            @foreach($pencatatanNotDone as $item)
                            <li style="width: 94%;">
                                <p class="font-weight-bold text-dark" style="font-size:14px;">RT {{$item->rt}}</p>
                                @if($item->notDone == 0 && $item->done == 0)
                                    <p style="font-size:14px;" class="text-center">Tagihan belum digenerate, silahkan hubungi Admin</p>
                                @else
                                    <p style="font-size:14px;" class="mb-0">Belum Lunas : <span class="font-weight-bold text-dark">{{$item->notDone}}</span></p>
                                    <p style="font-size:14px;">Lunas : <span class="font-weight-bold text-dark">{{$item->done}}</span></p>
                                @endif
                                <hr>
                            </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script src="../assets/js/plugins/chartjs.min.js"></script>
    <script src="../assets/js/plugins/Chart.extension.js"></script>


    <script type="text/javascript">
        $.noConflict();
        jQuery(document).ready(function($) {
            

        });

        //start chart pie pemakaian admin
        var posts = {!! $pie->toJson() !!};

        var copyItems = [];
        var copyValue = [];

        posts.forEach(function(post) {
            copyItems.push( 'RT ' + post['name']);
            copyValue.push(post['value']);
        });



        var xValues = copyItems;
        var yValues = copyValue;
        var barColors = [
            "#962409",
            "#0783d3",
            "#2a23f7",
            "#a438e2",
            "#5a4d06",
            "#34982e",
            "#9134f1",
            "#712e48",
            "#ccfdd1",
            "#ed825b",
            "#76e68b",
            "#11aad2",
            "#56d6dd",
            "#737f4d",
            "#60f6dd",
            "#a9136c",
            "#c19a43",
            "#ab1729",
            "#6aa7e5",
            "#873897",
            "#72b513",
            "#1548d6",
            "#7ddeda",
            "#861e93",
            "#7134fb",
            "#b09548",
            "#c36a5b",
            "#7d6af6",
            "#866b62",
            "#912058",
            "#6974ee",
            "#bb492e"
        ];

        new Chart("myPiepemakaian", {
            type: "pie",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: true,
                }
            }
        });
        //endchart pie

        //start chart bar pendapatan admin dan petugas
        var bars = {!! $bar->toJson() !!};

        var copyBarItem = [];
        var copyBarValue = [];

        bars.forEach(function(bar) {
            copyBarItem.push( bar['name']);
            copyBarValue.push(bar['value']);
        });

        var xValues = copyBarItem;
        var yValues = copyBarValue;
        var barColors = ["orange", "green", "blue", "black", "yellow", "pink"];

        new Chart("myPendapatan", {
            type: "bar",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                legend: {
                    display: false
                },
                title: {
                    display: true
                }
            }
        });
        //end chart bar pendapatan admin

        //start chart bar pemakaian pelanggan dan petugas
        var pemakaianYear = {!! $pemakaianYear->toJson() !!};
        console.log(pemakaianYear)

        var pemakaianYearItem = [];
        var pemakaianYearValue = [];

        pemakaianYear.forEach(function(bar) {
            pemakaianYearItem.push(bar['name']);
            pemakaianYearValue.push(bar['value']);
        });

        new Chart("myPemakaian", {
            type: "bar",
            data: {
                labels: pemakaianYearItem,
                datasets: [{
                    backgroundColor: barColors,
                    data: pemakaianYearValue
                }]
            },
            options: {
                legend: {
                    display: false
                },
                title: {
                    display: true
                }
            }
        });
    //start chart bar pemakaian pelanggan dan petugas
    </script>
    @endsection
