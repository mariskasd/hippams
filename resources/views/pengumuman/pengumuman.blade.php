@extends('layouts.app')
@section('content')
<div>
    <div class="alert alert-secondary mx-4" role="alert">
        <p class="text-white text-xs mb-1">Anda dapat melakukan manajemen data Pengumuman pada halaman ini</p>
    </div>

     <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-3">
                <div class="card-header pb- 0">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                             <h4 class="mb-0">Daftar Pengumuman</h4>
                        </div>
                        @if(Auth::user()->role != "Pelanggan")
                         <button class="btn bg-gradient-primary p-2 mb-4 ms-1" type="button" onclick="addModal()">+&nbsp; Tambah</button>
                        @endif
                    </div>
                 </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-2">
                         <table class="table align-items-center mb-0 ms-2" id="tblPengumuman">
                            <thead>
                                <tr>
                                     <th class="text-uppercase text-sm font-weight-bolder  ps-2">
                                        Judul
                                    </th>
                                    <th class="text-uppercase text-sm font-weight-bolder  ps-2">
                                        Deskripsi
                                    </th>
                                    @if(Auth::user()->role != "Pelanggan")
                                        <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                            Tampilkan
                                        </th>
                                        <th class="text-center text-uppercase  text-sm font-weight-bolder ">
                                            Aksi
                                        </th>
                                    @endif
                                </tr>
                             </thead>
                            <tbody>
                                @foreach($pengumuman as $item)
                                 <tr>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0 desc-long">{{$item->judul}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0 desc-long">{{$item->deskripsi}}</p>
                                    </td>
                                    @if(Auth::user()->role != "Pelanggan")
                                        <td>
                                            @if($item->active == 1)
                                                <p class="text-xs font-weight-bold mb-0">Ya</p>
                                            @else
                                                <p class="text-xs font-weight-bold mb-0">Tidak</p>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Edit" onclick="addModal(null,'{{$item}}')">
                                                <i class="fas fa-user-edit text-secondary"></i>
                                            </a>
                                            <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Hapus" onclick="deleteData('{{$item->id}}')">
                                                <i class="cursor-pointer fas fa-trash text-secondary"></i>
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalPengumuman" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Pengumuman</h5>
                    <button type="button" class="btn close" onclick="addModal('close')">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Judul<sup class="text-danger">*</sup></label>
                                <input type="text" id="judul" name="judul" class="form-control" type="text" placeholder="Masukkan Judul" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Deskripsi<sup class="text-danger">*</sup></label>
                                <textarea id="deskripsi" name="deskripsi" class="form-control" type="text" placeholder="Masukkan Deskripsi" rows="7"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Tampilkan Pengumuman<sup class="text-danger">*</sup></label><br>
                                <input type="checkbox" id="active" name="active" value="false">
                                 <label for="vehicle1"> Ya</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submit()">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
.desc-long {
    display: inline-block;
    width: 180px;
    white-space: nowrap;
    overflow: hidden !important;
    text-overflow: ellipsis;
}
</style>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        $('#tblPengumuman').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
        });

    });

    var idEdit = 0;

    function addModal(close, data) {
        if (close) {
            idEdit = 0;
            jQuery("#judul").val("");
            jQuery("#deskripsi").val("");
            jQuery("#active").val("");
            jQuery("#modalPengumuman").modal('hide');
        } else {
            idEdit = 0;
            jQuery("#modalPengumuman").modal('show');
            if (data) {
                let edit = JSON.parse(data);
                jQuery("#judul").val(edit.judul);
                jQuery("#deskripsi").val(edit.deskripsi);
                jQuery("#active").prop('checked',edit.active);
                idEdit = edit.id;
            }
        }
    }

    function submit() {
        let judul = jQuery("#judul").val();
        let deskripsi = jQuery("#deskripsi").val();
        let active = jQuery("#active").is(':checked');

            if ((judul == "" || judul == null)
            || (deskripsi == "" || deskripsi == null)) {
                swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");
            } else {
                var data = new FormData();
                data.append("judul", judul);
                data.append("deskripsi", deskripsi);
                if(active == true){
                data.append("active", 1);
                }
                else{
                data.append("active", 0);
                }

                let url = '{{route("newPengumuman")}}';
                if (idEdit != 0) {
                    url = '{{route("editPengumuman" , ":id")}}';
                    url = url.replace(":id", idEdit);
                }

                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    async: false,
                    cache: false,
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(data) {
                        let message = "Data Berhasil Disimpan!";
                        if(data.message  !== "success"){
                            message = "Data Berhasil Disimpan \t (" + data.message + ")";
                        }
                        swal.fire("Sukses!", message, "success").then((value) => {
                            location.reload();
                        });
                    },
                    error: function(e) {
                        console.log(e);
                        swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                    }
                });
            }

        
    }

    function deleteData(idData) {
        
        swal.fire({
            text: "Apakah anda yakin akan menghapus data ini?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.isConfirmed) {
                let url = '{{route("hapusPengumuman" , ":id")}}';
                url = url.replace(":id", idData);
                
                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        swal.fire({
                            title: "Sukses!",
                            text: "Data Berhasil Dihapus!",
                            icon: "success"
                        }).then((result) => {
                            location.reload();
                        });
                    },
                    error: function(e) {
                        console.log(e);
                        swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                    }
                });
            }
        });
    }
</script>
@endsection