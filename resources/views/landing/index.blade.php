<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>HIPPAMS</title>
    <link rel="shortcut icon" href="../assets/landing-style/image/logo/hanuman.jpg" type="image/jpg">
    <!-- Bootstrap , fonts & icons  -->
    <link rel="stylesheet" href="../assets/landing-style/css/bootstrap.css">
    <link rel="stylesheet" href="../assets/landing-style/fonts/icon-font/css/style.css">
    <link rel="stylesheet" href="../assets/landing-style/fonts/typography-font/typo.css">
    <link rel="stylesheet" href="../assets/landing-style/fonts/fontawesome-5/css/all.css">
    <link href="https://fonts.googleapis.com/css2?family=Karla:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Gothic+A1:wght@400;500;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700;800;900&display=swap" rel="stylesheet">
    <!-- Plugin'stylesheets  -->
    <link rel="stylesheet" href="../assets/landing-style/plugins/aos/aos.min.css">
    <!-- Vendor stylesheets  -->
    <link rel="stylesheet" href="../assets/landing-style/css/main.css">
    <!-- Custom stylesheet -->
    <style>
        .break-word {
            word-break: break-all;
        }

        .price-body {
            font-size: 12px;
        }


        .sm-col-reverse {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            width: 100%;
        }



        .desc-announce {
            overflow: hidden;
            text-overflow: ellipsis;
            -webkit-line-clamp: 5;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            white-space: initial;
        }

        .pricing-area-l14 .popular-pricing.popular-pricing-2:before {
            height: 3px;
            top: -3px;
            background: #2596be !important;
        }

        .read-more {
            cursor: pointer;
            color: #2596be;
            font-size: 12px;
        }

        .footer-landing {
            background-color: #2596be;
            color: white;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .hero-image-container {
            width: 50%;
            margin-left: 1rem;
        }

        .hero-image-container img {
            width: 90%;
            margin-left: 5rem;
        }

        .text-container-hero {
            width: 40%;
            padding: 4.5rem 0;
        }

        .hero-container {
            margin-top: 10rem;
        }

        @media (max-width: 600px) {

            .sm-col-reverse {
                flex-direction: column-reverse;
                gap: 5rem;
            }

            .hero-image-container {
                width: 100%;
                margin-left: 0;
            }

            .hero-image-container img {
                width: 20rem;
                margin-left: 0;

            }

            .text-container-hero {
                width: 100%;
                padding: 0;
            }

            .res-pengumuman-container {
                padding: 1rem;
            }
        }
    </style>
</head>

<body data-theme-mode-panel-active data-theme="light" style="font-family: 'Mazzard H';">
    <div class="site-wrapper overflow-hidden position-relative">
        <!--Site Header Area -->
        <header class="site-header site-header--menu-right landing-14-menu site-header--absolute site-header--sticky">
            <div class="container">
                <nav class="navbar site-navbar">
                    <!-- Brand Logo-->
                    <div class="brand-logo pb-2">
                        <a href="#">
                            <!-- light version logo (logo must be black)-->
                            <img src="../assets/landing-style/image/logo/hanuman.png" alt="" class="light-version-logo" style="width: 70px; height: 70px;margin-top:5px;">
                            <!-- Dark version logo (logo must be White)-->
                            <img src="../assets/landing-style/image/logo/logo-white.png" style="width: 70px; height: 70px;margin-top:5px;" alt="" class="dark-version-logo">
                        </a>
                    </div>
                    <div class="menu-block-wrapper">
                        <div class="menu-overlay"></div>
                        <nav class="menu-block" id="append-menu-header">
                            <div class="mobile-menu-head">
                                <div class="go-back">
                                    <i class="fa fa-angle-left"></i>
                                </div>
                                <div class="ms-3 d-flex align-items-center">
                                    <div>
                                        <img src="../assets/landing-style/image/logo/hanuman.png" alt="" class="light-version-logo" style="width: 40px; height: 40px;">
                                        <!-- Dark version logo (logo must be White)-->
                                        <img src="../assets/landing-style/image/logo/logo-white.png" style="width:40px; height:40px;" alt="" class="dark-version-logo">
                                    </div>
                                    <p class="m-3">HIPPAMS Tirta Lestari</p>
                                </div>
                                <div class="current-menu-title"></div>
                                <div class="mobile-menu-close">&times;</div>
                            </div>
                            <ul class="site-menu-main">
                                <li class="nav-item">
                                    <!-- Pengumuman -->
                                    <a href="#announce" class="nav-link-item"><b style="border-bottom: 2px solid #2596be;color:#2596be">Pengumuman</b></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="mobile-menu-trigger">
                        <span></span>
                    </div>
                </nav>
            </div>
        </header>
        <div class="hero-container position-relative z-index-1 overflow-hidden">
            <div class="container">
                <div class="sm-col-reverse">
                    <div class="justify-content-center align-items-center text-container-hero" data-aos="fade-right" data-aos-duration="800" data-aos-once="true">
                        <div class="content">
                            <h2>Air Bersih</h2>
                            <h2>Untuk hidup yang lebih sehat</h2>
                            <p>Kesehatan dan kesejahteraan diukur dari kebersihan air yang digunakan</p>
                            <a href="{{ route('login') }}" class="btn focus-reset mt-3">LOGIN</a>
                        </div>
                    </div>
                    <div class="hero-image-container" data-aos="fade-left" data-aos-duration="800" data-aos-once="true">
                        <img src="../assets/landing-style/image/ill.svg" alt="">
                    </div>
                </div>
            </div>
            <div class="bg-shape-14" style="color:blue;"></div>
        </div>
        <div class="features-area-l-14">
            <div class="container bg-shape-img-2 position-relative">
                <div class="row features-area-l-14 justify-content-center mx-0">
                    <div class="text-center my-5">
                        <h3>Tentang Kami</h3>
                    </div>
                    <div class="col-lg-6 col-md-8 col-sm-10 px-lg-6" data-aos="fade-right" data-aos-duration="800" data-aos-once="true">
                        <div class="card p-2 " style="height: 100%;">
                            <div class="mx-auto py-4">
                                <img src="../assets/landing-style/image/bill.svg" alt="" width="50" height="50">
                            </div>
                            <div>
                                <div class=" pl-sm-14 pl-5 pb-3 text-center">
                                    <h5 style="color: #000;">
                                        Informasi Tagihan Pembayaran Pelanggan</h5>
                                    <p class="mb-0" style="color: #8c8c8c;">
                                        Sistem online berbasis website agar bisa diakses kapan pun dan dimana pun.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-8 col-sm-10" data-aos="fade-left" data-aos-duration="800" data-aos-once="true">
                        <div class="card p-2" style="height: 100%;">
                            <div class="mx-auto py-3">
                                <img src="../assets/landing-style/image/announce.svg" alt="" width="70" height="70">
                            </div>
                            <div>
                                <div class=" pl-sm-14 pl-5 pb-3 text-center">
                                    <h5 style="color: #000;">
                                        Pengumuman dan Layanan Pengaduan</h5>
                                    <p class="mb-0" style="color: #8c8c8c;">
                                        Salah satu fitur yang dapat digunakan untuk mendapatkan informasi layanan HIPPAMS
                                        dan fitur pengaduan pelanggan
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pricing-area-l14 pt-5 position-relative overflow-hidden z-index-1" id="announce">
            <div class="container">
                <div class="row justify-content-center mb-5">
                    <div class="col-12 col-xl-6 col-lg-7 col-md-10 text-center" data-aos="fade-down" data-aos-duration="800" data-aos-once="true">
                        <div class="section-heading-6">
                            <h3>Pengumuman</h3>
                        </div>
                    </div>
                </div>
                @if($pengumuman->count() > 0)
                <div class="row justify-content-center res-pengumuman-container m-0" id="table-price-value" data-pricing-dynamic data-value-active="monthly">
                    @foreach($pengumuman as $item)
                    <div class="col-lg-3 col-md-6 border p-0 mt-3 mx-3 ">
                        <div class="single-price bg-default popular-pricing popular-pricing-2 text-center position-relative  pt-2 pb-10 mb-lg-0" data-aos="fade-up" data-aos-duration="800" data-aos-once="true">
                            <div class="price-top justify-content-between  p-2">
                                <span>
                                    <h5>{{$item->judul}}</h5>
                                </span>
                            </div>
                            <div class="main-price pb-8 border-bottom border-2">
                            </div>
                            <div class="price-body p-2 mb-2">
                                <div class="desc-announce">{{$item->deskripsi}}</div>
                            </div>

                            <div class="text-center read-more mb-1" onclick="openModal(true,{{$item}})">
                                Selengkapnya>>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @else
                <p class="text-center">
                    Tidak ada Pengumuman
                </p>
                @endif
            </div>
        </div>
        <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="titleAnnounce">Judul</h5>
                        <button type="button" class="border-0 bg-transparent" onclick="openModal(false)">
                            <span aria-hidden="true" class="text-end">X</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <span id="descAnnounce"></span>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer-area-l-12 mb-0 pb-0 position-relative">
            <div class="footer-landing text-center">
                HIPPAMS Tirta Lestari (081223555666)
            </div>
        </footer>
    </div>
    <script src="../assets/landing-style/js/vendor.min.js"></script>
    <script src="../assets/landing-style/plugins/aos/aos.min.js"></script>
    <script src="../assets/landing-style/plugins/menu/menu.js"></script>
    <script src="../assets/landing-style/js/custom.js"></script>

    <script>
        function openModal(type, data) {
            if (type == true) {
                jQuery("#modalDetail").modal('show');
                jQuery("#titleAnnounce").text(data.judul);
                jQuery("#descAnnounce").text(data.deskripsi);
            } else {
                jQuery("#modalDetail").modal('hide');
                jQuery("#titleAnnounce").text('');
                jQuery("#descAnnounce").text('');
            }
        }
    </script>
</body>

</html>