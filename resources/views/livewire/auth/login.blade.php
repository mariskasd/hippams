<section>
    <div class="page-header section-height-100">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">
                    <div class="card card-plain">
                        <div class="mx-auto">
                            <img src="../assets/landing-style/image/logo/hanuman.png" alt="" class="light-version-logo" style="width: 80px; height: 80px;">
                        </div>

                        <div class="card-header pb-0 text-center bg-transparent">

                            <h6>Silahkan login untuk memulai</h6>
                        </div>
                        <div class="card-body">
                            <form wire:submit.prevent="login" action="#" method="POST" role="form text-left">
                                <div class="mb-3">
                                    <label for="id_pengguna">{{ __('ID Pengguna') }}</label>
                                    <div class="@error('id_pengguna')border border-danger rounded-3 @enderror">
                                        <input wire:model="id_pengguna" id="id_pengguna" type="text" class="form-control" placeholder="ID Pengguna" aria-label="ID Pengguna" aria-describedby="id_pengguna-addon" ">
                                    </div>
                                    @error('id_pengguna') <div class=" text-danger">{{ $message }}
                                    </div> @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="password">{{ __('Password') }}</label>
                                    <div class="@error('password')border border-danger rounded-3 @enderror">
                                        <input wire:model="password" id="password" type="password" class="form-control" placeholder="password" aria-label="Password" aria-describedby="password-addon">
                                    </div>
                                    @error('password') <div class="text-danger">{{ $message }}</div> @enderror
                                </div>

                                @error('loginGagal') <div class="text-danger">{{ $message }}</div> @enderror
                                <div class="text-center">
                                    <button type="submit" class="btn bg-gradient-primary w-100 mt-4 mb-0">LOGIN</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
                        <div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6" style="background-image:url('../assets/img/curved-images/a.png')"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>