  <section class="h-100-vh">
      <div class="page-header align-items-center section-height-100 pt-5 pb-5"
          style="background-image: url('../assets/img/curved-images/bgreg.jpg');">
          <span class="mask bg-gradient opacity-6"></span>
          <!-- <div class="container">
              <div class="row justify-content-center">
                  <div class="col-lg-5 text-center mx-auto">
                      <h1 class="text-white mb-2 mt-5">{{ __('Welcome!') }}</h1>
                      <p class="text-lead text-white">
                          {{ __('Use these awesome forms to login or create new account in your
                          project for free.') }}
                      </p>
                  </div>
              </div>
          </div> -->
      <div class="container">
          <div class="row">
              <div class="col-xl-4 col-lg-5 col-md-7 mx-auto">
                  <div class="card z-index-0">
                      <div class="card-header text-center pt-4">
                          <h5 class="text-primary">{{ __('Daftar') }}</h5>
                      </div>
                      <div class="card-body">

                          <form wire:submit.prevent="register" action="#" method="POST" role="form text-left">
                              <div class="mb-3">
                                  <div class="@error('name') border border-danger rounded-3  @enderror">
                                      <input wire:model="name" type="text" class="form-control" placeholder="Nama Perusahaan"
                                          aria-label="Name" aria-describedby="email-addon">
                                  </div>
                                  @error('name') <div class="text-danger">{{ $message }}</div> @enderror
                              </div>
                              <div class="mb-3">
                                  <div class="@error('email') border border-danger rounded-3 @enderror">
                                      <input wire:model="email" type="email" class="form-control" placeholder="Email"
                                          aria-label="Email" aria-describedby="email-addon">
                                  </div>
                                  @error('email') <div class="text-danger">{{ $message }}</div> @enderror
                              </div>
                              <div class="mb-3">
                                  <div class="@error('password') border border-danger rounded-3 @enderror">
                                      <input wire:model="password" type="password" class="form-control"
                                          placeholder="Password" aria-label="Password"
                                          aria-describedby="password-addon">
                                  </div>
                                  @error('password') <div class="text-danger">{{ $message }}</div> @enderror
                              </div>
                              <div class="text-center">
                                  <button type="submit" class="btn btn-primary w-100 my-4 mb-2">Sign up</button>
                              </div>
                              <p class="text-sm mt-3 mb-0">{{ __('Sudah punya akun? ') }}<a
                                      href="{{ route('login') }}"
                                      class="text-pimary font-weight-bolder">{{ __('Login') }}</a>
                              </p>
                          </form>

                      </div>
                  </div>
              </div>
          </div>
      </div>
      </div>

  </section>
