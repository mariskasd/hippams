<main class="main-content bg mt-1 border-radius-lg">
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <!-- <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-md"><a class="opacity-5 text-dark" href="javascript:;">{{ Route::currentRouteName() == 'karyawan' ? 'MASTER' : 'MENU' }}</a>
                    </li>
                    <li class="breadcrumb-item text-sm text-dark active text-capitalize" aria-current="page">
                        {{ str_replace('-', ' ', Route::currentRouteName()) }}
                    </li>
                </ol> -->
                
                <p class="mb-0 align-middle text-center text-xs font-weight-bold mt-2 ">{{date('d M Y')}}</p>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4 d-flex justify-content-end" id="navbar">
                <!-- <div class="nav-item d-flex align-self-end">
                    <a href="https://www.creative-tim.com/product/soft-ui-dashboard-laravel" target="_blank"
                        class="btn btn-primary active mb-0 text-white" role="button" aria-pressed="true">
                        Download
                    </a>
                </div>
                <div class="ms-md-3 pe-md-3 d-flex align-items-center">
                    <div class="input-group">
                        <span class="input-group-text text-body"><i class="fas fa-search" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" placeholder="Type here...">
                    </div>
                </div> -->
                <ul class="navbar-nav justify-content-between">

                    <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
                        <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                            <div class="sidenav-toggler-inner">
                                <i class="sidenav-toggler-line"></i>
                                <i class="sidenav-toggler-line"></i>
                                <i class="sidenav-toggler-line"></i>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item dropdown pe-2 d-flex align-items-center">
                        <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-user me-sm-1 {{ in_array(request()->route()->getName(),['profile', 'my-profile']) ? 'text-white' : '' }}"></i>
                            <span class="d-sm-inline d-none {{ in_array(request()->route()->getName(),['profile', 'my-profile']) ? 'text-white' : '' }}">{{Auth::user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu overflow-hidden  dropdown-menu-end me-sm-n4" aria-labelledby="dropdownMenuButton">
                          
                            <li class="mb-2">
                                <a class="dropdown-item border-radius-md" wire:click="logout">
                                    <livewire:auth.logout />
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>