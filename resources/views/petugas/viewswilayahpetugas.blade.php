@extends('layouts.app')
@section('content')
<div>
    <div class="alert alert-secondary mx-4" role="alert">
        <h5 class="text-white text-md mb-1">Informasi Petugas</h5>
        <hr class="color-white">
        <p class="text-white text-sm mb-1">Nama Petugas :  <span class="font-weight-bold">{{$petugas->nama}}</span></p>
        <p class="text-white text-sm mb-1">Divisi : <span class="font-weight-bold">{{$petugas->type}}</span></p>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-4">
                <div class="card-header pb-0">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h4 class="mb-0">Daftar Area Petugas</h4>
                        </div>
                        <button class="btn bg-gradient-primary btn-sm mb-4" type="button" onclick="addModal()">+&nbsp; Tambah Area Petugas</button>
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-2">
                        <table class="table align-items-center mb-0 ms-2" id="tblArea">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-sm font-weight-bolder  ps-2">
                                        RT
                                    </th>
                                    <th class="text-center text-uppercase text-sm font-weight-bolder ">
                                        Aksi
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($wilayahPetugas as $item)
                                <tr>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->nama_rt}}</p>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Hapus" onclick="deleteData('{{$item->id}}')">
                                            <i class="cursor-pointer fas fa-trash text-secondary"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    </div> <div class="modal fade" id="modalArea" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Area Petugas</h5>
                    <button type="button" class="btn close" onclick="addModal('close')">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">RT<sup class="text-danger">*</sup></label>
                                <select name="rt" id="rt" class="form-control">
                                    <option value="">== Pilih RT ==</option>
                                    @foreach ($rt as $id => $nama_rt)
                                        <option value="{{ $id }}">{{ $nama_rt }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="petugas_id" id="petugas_id" value="{{$petugas->id_petugas}}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submit()">Simpan</button>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        $('#tblArea').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
        });

    });

    var idEdit = 0;

    function addModal(close, data) {
        if (close) {
            idEdit = 0;
            //jQuery("#rt").val("");
            jQuery("#modalArea").modal('hide');
        } else {
            idEdit = 0;
            jQuery("#modalArea").modal('show');
            if (data) {
                let edit = JSON.parse(data);
                jQuery("#rt").val(edit.rt_id);
                idEdit = edit.id;
            }
        }
    }

    function submit() {
        let rt = jQuery("#rt").val();
        let petugas_id = jQuery("#petugas_id").val();


        if ((rt == "" || rt == null)) {
            swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");
        } else {
            var data = new FormData();
            data.append("rt", rt);
            data.append("petugas_id", petugas_id);

            let url = '{{route("add-wilayah-petugas")}}';
            if (idEdit != 0) {
                url = '{{route("edit-wilayah-petugas" , ":id")}}';
                url = url.replace(":id", idEdit);
            }

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                processData: false,
                contentType: false,
                data: data,
                success: function(data) {
                        console.log(data);
                        let message = "Data Berhasil Disimpan!";
                        if(data.message  !== "success"){
                            swal.fire("Gagal !", data.message, "error").then((value) => {
                            });
                        }else{
                            swal.fire("Sukses!", message, "success").then((value) => {
                                location.reload();
                            });
                        }
                },
                error: function(e) {
                    console.log(e);
                    swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                }
            });
        }
    }

    function deleteData(idData) {
        
        swal.fire({
            text: "Apakah anda yakin akan menghapus data ini?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.isConfirmed) {
                let url = '{{route("delete-wilayah-petugas" , ":id")}}';
                url = url.replace(":id", idData);
                

                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        swal.fire({
                            title: "Sukses!",
                            text: "Data Berhasil Dihapus!",
                            icon: "success"
                        }).then((result) => {
                            location.reload();
                        });
                    },
                    error: function(e) {
                        console.log(e);
                        swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                    }
                });
            }
        });
    }
</script>
@endsection