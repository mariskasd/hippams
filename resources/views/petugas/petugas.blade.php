@extends('layouts.app')
@section('content')
<div>
    <div class="alert alert-secondary mx-4" role="alert">
        <p class="text-white text-xs mb-1">Anda dapat melakukan manajemen data Petugas pada halaman ini</p>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-4">
                <div class="card-header pb-0">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h4 class="mb-0">Daftar Petugas</h4>
                        </div>
                        <button class="btn bg-gradient-primary btn-xs mb-4" type="button" onclick="addModal()">+&nbsp; Tambah</button>
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-2">
                        <table class="table align-items-center mb-0 ms-2" id="tblPetugas">
                            <thead>
                                <tr>
                                   <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        ID Petugas
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Nama
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Divisi
                                    </th>
                                    <th class="text-center text-uppercase  text-sm font-weight-bolder ">
                                        Aksi
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($petugas as $item)
                                <tr>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->id_petugas}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->nama}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->type}}</p>
                                    </td>
                                    <td class="text-center">
                                       <a href="{{ route('wilayah-petugas', $item->id_petugas,'id') }}" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Lihat Wilayah Petugas" >
                                            <i class="fas fa-search text-secondary"></i>
                                        </a>
                                        <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Edit" onclick="addModal(null,'{{$item}}')">
                                            <i class="fas fa-user-edit text-secondary"></i>
                                        </a>
                                        <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Hapus" onclick="deleteData('{{$item->id_petugas}}')">
                                            <i class="cursor-pointer fas fa-trash text-secondary"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalPetugas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Petugas</h5>
                    <button type="button" class="btn close" onclick="addModal('close')">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" id="lblIdPetugas" name="lblIdPetugas" class="form-control-label">ID Petugas</label>
                                <input id="id_petugas" name="id_petugas" class="form-control" type="text" readonly >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Nama Petugas<sup class="text-danger">*</sup></label>
                                <input id="name" name="name" class="form-control" type="text" placeholder="Masukkan Nama Pelanggan" onkeypress="return validate(event)">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">No Telp<sup class="text-danger">*</sup></label>
                                <input id="no_telp" name="no_telp" class="form-control" type="text" placeholder="Masukkan No Telp" onkeypress="return validatePhone(event)" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Divisi<sup class="text-danger">*</sup></label>
                                <select name="tipe" id="tipe" class="form-control">
                                    <option value="">== Pilih Divisi ==</option>
                                    <option value="Lapangan">Lapangan</option>
                                    <option value="Pembayaran">Pembayaran</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Alamat<sup class="text-danger">*</sup></label>
                                <textarea id="alamat" name="alamat" class="form-control" type="text" placeholder="Masukkan Alamat" ></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submit()">Simpan</button>
                </div>
            </div>
        </div>
    </div>


</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>


<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        $.ajaxSetup({
         headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

        $('#tblPetugas').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
        });

    });

    var idEdit = 0;

    
    function validate(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode

        if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
        
        return false;
        
    }

    function validatePhone(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode > 31 && (charCode < 48 || charCode > 57))
        
        return false;
    }

    function addModal(close, data) {
        if (close) {
            idEdit = 0;
            jQuery("#name").val("");
            jQuery("#no_telp").val("");
            jQuery("#alamat").val("");

            
            jQuery("#modalPetugas").modal('hide');
        } else {
            idEdit = 0;
            jQuery("#modalPetugas").modal('show');
            if (data) {
                jQuery("#id_petugas").show();
                jQuery("#lblIdPetugas").show();

                let edit = JSON.parse(data);
                jQuery("#name").val(edit.nama);
                jQuery("#no_telp").val(edit.no_telp);
                jQuery("#alamat").val(edit.alamat);
                jQuery("#id_petugas").val(edit.id_petugas);
                jQuery("#tipe").val(edit.type);


               jQuery("#tipe").attr("disabled", "disabled"); 

                idEdit = edit.id_petugas;
            }else{
                jQuery("#id_petugas").hide();
                jQuery("#lblIdPetugas").hide();

                jQuery("#name").val("");
                jQuery("#no_telp").val("");
                jQuery("#alamat").val("");

            }
        }
    }

    function submit() {
        let idPel = jQuery("#id_petugas").val();
        let names = jQuery("#name").val();
        let no_telp = jQuery("#no_telp").val();
        let alamat = jQuery("#alamat").val();
        let tipe = jQuery("#tipe").val();

        let reg = /[0-9]/g;
        let result = names.match(reg);

        if (result != null) {
            swal.fire("Gagal !", "Nama Petugas Tidak Boleh Mengandung Angka", "error");
        }else{

            let reg = /[A-Za-z]/g;
            let result = no_telp.match(reg);

            if (result != null) {
                swal.fire("Gagal !", "No.Telp Tidak Boleh Mengandung Angka", "error");
            }else{

                if(idPel == "" || idPel == null){
                    if ((names == "" || names == null)  || (no_telp == "" || no_telp == null)
                    || (alamat == "" || alamat == null) || (tipe == "" || tipe == null)) {
                    swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");
                    } else {
                        var data = new FormData();
                        data.append("name", names);
                        data.append("no_telp", no_telp);
                        data.append("alamat", alamat);
                        data.append("type", tipe);

                        let url = '{{route("newPetugas")}}';
                        if (idEdit != 0) {
                            url = '{{route("editPetugas" , ":id")}}';
                            url = url.replace(":id", idEdit);
                        }

                        console.log(url);

                        jQuery.ajax({
                            headers: {
                                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                            },
                            url: url,
                            method: "POST",
                            async: false,
                            cache: false,
                            processData: false,
                            contentType: false,
                            data: data,
                            success: function(data) {
                                console.log(data);
                                let message = "Data Berhasil Disimpan!";
                                if(data.message  !== "success"){
                                    swal.fire("Gagal !", data.message, "error").then((value) => {
                                    });
                                }else{
                                    swal.fire("Sukses!", message, "success").then((value) => {
                                        location.reload();
                                    });
                                }
                                
                            },
                            error: function(e) {
                                console.log(e);
                                swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                            }
                        });
                    }
                }else{
                    if ((names == "" || names == null)  || (no_telp == "" || no_telp == null)
                    || (alamat == "" || alamat == null) || (tipe == "" || tipe == null)) {
                    swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");
                    } else {
                        var data = new FormData();
                        data.append("name", names);
                        data.append("no_telp", no_telp);
                        data.append("alamat", alamat);
                        data.append("type", tipe);

                        let url = '{{route("newPetugas")}}';
                        if (idEdit != 0) {
                            url = '{{route("editPetugas" , ":id")}}';
                            url = url.replace(":id", idEdit);
                        }

                        console.log(url);

                        jQuery.ajax({
                            headers: {
                                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                            },
                            url: url,
                            method: "POST",
                            async: false,
                            cache: false,
                            processData: false,
                            contentType: false,
                            data: data,
                            success: function(data) {
                                console.log(data);
                                let message = "Data Berhasil Disimpan!";
                                if(data.message  !== "success"){
                                    swal.fire("Gagal !", data.message, "error").then((value) => {
                                    });
                                }else{
                                    swal.fire("Sukses!", message, "success").then((value) => {
                                        location.reload();
                                    });
                                }
                                
                            },
                            error: function(e) {
                                console.log(e);
                                swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                            }
                        });
                    }
                }

            }

        }

        
    }

    function deleteData(idData) {
        
        swal.fire({
            text: "Apakah anda yakin akan menghapus data ini?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.isConfirmed) {
                let url = '{{route("hapusPetugas" , ":id")}}';
                url = url.replace(":id", idData);
                
                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        let message = "Data Berhasil Dihapus!";
                        if(data.message  !== "success"){
                            swal.fire("Gagal !", data.message, "error");
                        }else{
                            swal.fire("Sukses!", message, "success").then((value) => {
                                location.reload();
                            });
                        }
                    },
                    error: function(e) {
                        console.log(e);
                        swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                    }
                });
            }
        });
    }

    function getPassword() {
           let url = '{{route("getPassword")}}';

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                processData: false,
                contentType: false,
                success: function(response) {
                    jQuery("#password").val(response);
                    
                },
                error: function(e) {
                    console.log(e);
                }
            });
    }
</script>
@endsection