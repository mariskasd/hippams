@extends('layouts.app')
@section('content')
<div>
    <div class="alert alert-secondary mx-4" role="alert">
        <p class="text-white text-xs mb-1">Anda dapat melakukan manajemen data Rukun Tetangga(RT) pada halaman ini</p>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-4">
                <div class="card-header pb-0">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h4 class="mb-0">Daftar RT</h4>
                        </div>
                        <button class="btn bg-gradient-primary btn-sm mb-4" type="button" onclick="addModal()">+&nbsp; Tambah</button>
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-2">
                        <table class="table align-items-center mb-0 ms-2" id="tblRT">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-sm font-weight-bolder ps-2">
                                        RT
                                    </th>
                                    <th class="text-uppercase text-sm font-weight-bolder ps-2">
                                        RW
                                    </th>
                                    <th class="text-center text-uppercase text-sm font-weight-bolder ">
                                        Aksi
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($rt as $item)
                                <tr>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->nama_rt}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->nama_rw}}</p>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Edit" onclick="addModal(null,'{{$item}}')">
                                            <i class="fas fa-user-edit text-secondary"></i>
                                        </a>
                                        <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Hapus" onclick="deleteData('{{$item->id}}')">
                                            <i class="cursor-pointer fas fa-trash text-secondary"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalRT" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Rukun Tetangga</h5>
                    <button type="button" class="btn close" onclick="addModal('close')">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">RW<sup class="text-danger">*</sup></label>
                                <select name="rw" id="rw" class="form-control">
                                    <option value="">== Pilih RW ==</option>
                                    @foreach ($rw as $id => $nama_rw)
                                        <option value="{{ $id }}">{{ $nama_rw }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">RT<sup class="text-danger">*</sup></label>
                                <input id="name" name="name" class="form-control" type="text" placeholder="Masukkan Nama RT" onkeypress="return validate(event)" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submit()">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        $('#tblRT').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
        });
    });

    var idEdit = 0;

    function validate(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode > 31 && (charCode < 48 || charCode > 57))
        
        return false;
        
    }

    function addModal(close, data) {
        if (close) {
            idEdit = 0;
            jQuery("#name").val("");
            jQuery("#modalRT").modal('hide');
        } else {
            idEdit = 0;
            jQuery("#modalRT").modal('show');
            if (data) {
                let edit = JSON.parse(data);
                jQuery("#name").val(edit.nama_rt);
                jQuery("#rw").val(edit.rw_id);
                idEdit = edit.id;
            }
        }
    }

    function submit() {
        let names = jQuery("#name").val();
        let rw = jQuery("#rw").val();

        let reg = /[A-Za-z]/g;
        let result = names.match(reg);

        if(result != null){

            swal.fire("Gagal !", "Nama RT Tidak Boleh Mengandung Huruf", "error");

        }else{
            if ((names == "" || names == null) || (rw == "" || rw == null)) {
                swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");
            } else {
                var data = new FormData();
                data.append("name", names);
                data.append("rw", rw);

                let url = '{{route("newRt")}}';
                if (idEdit != 0) {
                    url = '{{route("editRt" , ":id")}}';
                    url = url.replace(":id", idEdit);
                }

                console.log(url);

                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    async: false,
                    cache: false,
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(data) {
                        let message = "Data Berhasil Disimpan!";
                        if(data.message  !== "success"){
                            message = "Data Berhasil Disimpan \t (" + data.message + ")";
                        }
                        swal.fire("Sukses!", message, "success").then((value) => {
                            location.reload();
                        });
                    },
                    error: function(e) {
                        console.log(e);
                        swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                    }
                });
            }
        }
    }

    function deleteData(idData) {
        
        swal.fire({
            text: "Apakah anda yakin akan menghapus data ini?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.isConfirmed) {
                let url = '{{route("hapusRt" , ":id")}}';
                url = url.replace(":id", idData);
                

                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        let message = "Data Berhasil Dihapus!";
                        if(data.message  !== "success"){
                            swal.fire("Gagal !", data.message, "error");
                        }else{
                            swal.fire("Sukses!", message, "success").then((value) => {
                                location.reload();
                            });
                        }
                    },
                    error: function(e) {
                        console.log(e);
                        swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                    }
                });
            }
        });
    }
</script>
@endsection