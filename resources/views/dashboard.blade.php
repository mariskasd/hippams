@extends('layouts.app')
@section('content')
<div class="card mb-4 mx-4">
    <br>
    <div class="row">
        <!-- <div class="col-6">
            <div class="card mb-4 mx-4">
                <div class="card-header pb-0">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h5 class="mb-0">Komplain</h5>
                        </div>
                        
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <hr>
                    <div class="table-responsive p-2">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                   <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Kategori
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Judul
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Pelanggan
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Tanggal Komplain
                                    </th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($komplain as $item)
                                <tr>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->category}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->judul}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->id_pelanggan}} - {{$item->pelanggan}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->created_at}}</p>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="col-6">
                <div class="card mb-4 mx-4">
                    <div class="card-header pb-0">
                        <div class="d-flex flex-row justify-content-between">
                            <div>
                                <h5 class="modal-title" id="exampleModalLongTitle">Jadwal Pemadaman</h5>
                            </div>
                            
                        </div>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="modal-body">
                            <hr>
                            <div class="row ">
                                @foreach($collection as $item)
                                  <div class="col-md-6 ">
                                                                  <div class="form-group ">
                                                                      <div class="modal-content">
                                                                          <div class="modal-header">
                                                                                  <h5 class="modal-title" id="exampleModalLongTitle"><b>{{ \Carbon\Carbon::parse($item->tanggal_mulai)->translatedFormat('d M Y')}}</b></h5>
                                                                          </div>
                                                                          <div class="modal-body">
                                                                          <label for="name" class="form-control-label">Tanggal Mulai : {{ \Carbon\Carbon::parse($item->tanggal_mulai)->translatedFormat('d M Y H:i')}}</label>
                                                                          <br>
                                                                          <label for="name" class="form-control-label">Tanggal Selesai : {{ \Carbon\Carbon::parse($item->tanggal_selesai)->translatedFormat('d M Y H:i')}}</label>
                                                                          <br>
                                                                          <label for="name" class="form-control-label">Deskripsi : {{ $item->deskripsi }}</label>
                                                                          <br>
                                                                          <label for="name" class="form-control-label">Wilayah : {{ $item->wilayah }}</label>
                                                                          </div>
                                                                          <br>
                                                                      </div>
                                                                  </div>
                                  </div>
                                @endforeach
                            </div>
                        </div>
                </div>
        </div>
    </div>

</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<!-- https://code.jquery.com/jquery-3.5.1.js -->

<!-- https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js -->


<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        $('#tblUser').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
        });

    });

</script>
@endsection

<!-- @push('js') -->

<!-- @endpush -->