@extends('layouts.app')
@section('content')
<div>
    <div class="alert alert-secondary mx-4" role="alert">
         <h5 class="text-white text-md mb-1">Informasi Data Pelanggan</h5>
        <hr>
        <p class="text-white text-sm mb-1">ID : <span class="font-weight-bold ">{{$pelanggan->id_pelanggan}}</span></p>
        <p class="text-white text-sm mb-1">Pemilik : <span class="font-weight-bold">{{$pelanggan->nama}}</span></p>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-4">
                <div class="card-header pb-0">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h4 class="mb-0">Daftar Meteran</h4>
                        </div>
                        <button class="btn bg-gradient-primary btn-sm mb-4" type="button" onclick="addModal()">+&nbsp; Tambah Meteran Pelanggan</button>
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-2">
                        <table class="table align-items-center mb-0 ms-2" id="tblMeteran">
                            <thead>
                                <tr>
                                   <th class="text-uppercase text-sm font-weight-bolder  ps-2">
                                        ID Meteran
                                    </th>
                                    <th class="text-uppercase text-sm font-weight-bolder  ps-2">
                                        Wilayah
                                    </th>
                                    <th class="text-uppercase text-sm font-weight-bolder  ps-2">
                                        Alamat
                                    </th>
                                    <th class="text-uppercase text-sm font-weight-bolder  ps-2">
                                        Catatan
                                    </th>
                                    <th class="text-uppercase text-sm font-weight-bolder  ps-2">
                                        Status
                                    </th>
                                    <th class="text-center text-uppercase text-sm font-weight-bolder ">
                                        Aksi
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($meteran as $item)
                                <tr>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->id_meteran}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->nama_wilayah}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->lokasi}}, RT/RW {{$item->nama_rt}}/{{$item->nama_rw}} , Dusun {{$item->nama_dusun}} </p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->catatan}}</p>
                                    </td>
                                    <td>
                                        @if($item->active == 1)
                                            <p class="text-xs font-weight-bold mb-0"><b style="color:#4b912f;">AKTIF</b></p>
                                        @else
                                            <p class="text-xs font-weight-bold mb-0"><b style="color:#FF0000;">TIDAK AKTIF</b></p>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Edit" onclick="addModal(null,'{{$item}}')">
                                            <i class="fas fa-user-edit text-secondary"></i>
                                        </a>
                                        <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Hapus" onclick="deleteData('{{$item->id_meteran}}')">
                                            <i class="cursor-pointer fas fa-trash text-secondary"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="modalMeteran" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Meteran Pelanggan</h5>
                    <button type="button" class="btn close" onclick="addModal('close')">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="status_meteran">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Status Meteran<sup class="text-danger">*</sup></label><br>
                                <input type="checkbox" id="active" name="active" value="false">
                                 <label for="vehicle1"> Aktif</label><br>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Wilayah<sup class="text-danger">*</sup></label>
                                <select name="wilayah" id="wilayah" class="form-control">
                                    <option value="">== Pilih Wilayah ==</option>
                                    @foreach ($wilayah as $id => $nama_wilayah)
                                        <option value="{{ $id }}">{{ $nama_wilayah }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Nama Dusun<sup class="text-danger">*</sup></label>
                                <select name="dusun" id="dusun" class="form-control">
                                    <option value="">== Pilih Dusun ==</option>
                                    @foreach ($dusun as $id => $nama_dusun)
                                        <option value="{{ $id }}">{{ $nama_dusun }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="form-control-label">RW<sup class="text-danger">*</sup></label>
                                <select name="rw" id="rw" class="form-control">
                                    <option value="">== Pilih RW ==</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="form-control-label">RT<sup class="text-danger">*</sup></label>
                                <select name="rt" id="rt" class="form-control">
                                    <option value="">== Pilih RT ==</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Alamat<sup class="text-danger">*</sup></label>
                                <textarea id="alamat" name="alamat" class="form-control" type="text" placeholder="Masukkan Alamat" ></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Catatan<sup class="text-danger">*</sup></label>
                                <textarea id="catatan" name="catatan" class="form-control" type="text" placeholder="Masukan Catatan" ></textarea>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <input type="hidden" value="{{$pelanggan->id_pelanggan}}" name="id_pelanggan" id="id_pelanggan">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submit()">Simpan</button>
                </div>
            </div>
        </div>
    </div>


</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        $.ajaxSetup({
         headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

        $('#tblMeteran').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
        });

        $('#dusun').on('change', function () {
            console.log(1);
            var data = new FormData();
            data.append("id", $(this).val());

            let url = '{{route("dropdown-rw")}}';

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                processData: false,
                contentType: false,
                data: data,
                success: function(response) {
                    console.log(response);
                    $('#rw').empty();
                    $('#rt').empty();

                    $('#rw').append(new Option("== Pilih RW ==", ""));
                    $('#rt').append(new Option("== Pilih RT ==", ""));

                    $.each(response, function (id, name) {
                        $('#rw').append(new Option(name, id))
                    })
                },
                error: function(e) {
                    console.log(e);
                    swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                }
            });

        });

        $('#rw').on('change', function () {
            var data = new FormData();
            data.append("id", $(this).val());

            let url = '{{route("dropdown-rt")}}';

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                processData: false,
                contentType: false,
                data: data,
                success: function(response) {
                    console.log(response);
                    $('#rt').empty();

                    $('#rt').append(new Option("== Pilih RT ==", ""));

                    $.each(response, function (id, name) {
                        $('#rt').append(new Option(name, id))
                    })
                },
                error: function(e) {
                    console.log(e);
                    swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                }
            });

        });

    });

    var idEdit = 0;

    function addModal(close, data) {
        if (close) {
            idEdit = 0;
            jQuery("#alamat").val("");
            jQuery("#catatan").val("");

            jQuery('#rw').empty();
            jQuery('#dusun').empty();
            
            jQuery("#modalMeteran").modal('hide');
        } else {
            idEdit = 0;
            jQuery("#modalMeteran").modal('show');
            if (data) {

                let edit = JSON.parse(data);
                jQuery("#catatan").val(edit.catatan);
                jQuery("#alamat").val(edit.lokasi);
                jQuery("#dusun").val(edit.dusun_id);
                jQuery("#wilayah").val(edit.wilayah_id);
                jQuery("#active").prop('checked',edit.active);
                jQuery("#status_meteran").show();

                bindingRW(edit.dusun_id,edit.rw_id);
                bindingRT(edit.rw_id,edit.rt_id);

               // jQuery("#email").attr("disabled", "disabled"); 

                idEdit = edit.id_meteran;
            }else{
                jQuery("#status_meteran").hide();
                jQuery("#catatan").val("");
                jQuery("#alamat").val("");

                jQuery('#rt').empty();
                jQuery('#rw').empty();
            }
        }
    }

    function submit() {
        let idPel = jQuery("#id_pelanggan").val();
        let alamat = jQuery("#alamat").val();
        let rt = jQuery("#rt").val();
        let catatan = jQuery("#catatan").val();
        let wilayah = jQuery("#wilayah").val();
        let active = jQuery("#active").is(':checked');

            if ((catatan == "" || catatan == null)  || ( wilayah == "" || wilayah == null)
             || (alamat == "" || alamat == null) || (rt == "" || rt == null)) {
                swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");
            } else {
                var data = new FormData();
                data.append("id_pelanggan", idPel);
                data.append("catatan", catatan);
                data.append("alamat", alamat);
                data.append("rt", rt);
                data.append("wilayah", wilayah);
                if(active == true){
                data.append("active", 1);
                }
                else{
                data.append("active", 0);
                }

                let url = '{{route("newMeteran")}}';
                if (idEdit != 0) {
                    url = '{{route("editMeteran" , ":id")}}';
                    url = url.replace(":id", idEdit);
                }

                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    async: false,
                    cache: false,
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(data) {
                        console.log(data);
                        let message = "Data Berhasil Disimpan!";
                        if(data.message  !== "success"){
                            swal.fire("Gagal !", data.message, "error").then((value) => {
                            });
                        }else{
                            swal.fire("Sukses!", message, "success").then((value) => {
                                location.reload();
                            });
                        }
                        
                    },
                    error: function(e) {
                        console.log(e);
                        swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                    }
                });
            }
        
    }

    function deleteData(idData) {
        
        swal.fire({
            text: "Apakah anda yakin akan menghapus data ini?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.isConfirmed) {
                let url = '{{route("hapusMeteran" , ":id")}}';
                url = url.replace(":id", idData);
                
                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        let message = "Data Berhasil Dihapus!";
                        if(data.message  !== "success"){
                            swal.fire("Gagal !", data.message, "error");
                        }else{
                            swal.fire("Sukses!", message, "success").then((value) => {
                                location.reload();
                            });
                        }
                    },
                    error: function(e) {
                        console.log(e);
                        swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                    }
                });
            }
        });
    }

    function bindingRW(id, rw_id){
            var data = new FormData();
            data.append("id", id);

            let url = '{{route("dropdown-rw")}}';

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                processData: false,
                contentType: false,
                data: data,
                success: function(response) {
                    console.log(response);

                    jQuery('#rw').empty();
                    jQuery('#rt').empty();

                    jQuery('#rw').append(new Option("== Pilih RW ==", ""));
                    jQuery('#rt').append(new Option("== Pilih RT ==", ""));

                    jQuery.each(response, function (id, name) {
                        jQuery('#rw').append(new Option(name, id))
                    })

                    jQuery("#rw").val(rw_id);
                },
                error: function(e) {
                    console.log(e);
                    swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                }
            });
    }

    function bindingRT(id,rt_id){
            var data = new FormData();
            data.append("id", id);

            let url = '{{route("dropdown-rt")}}';

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                processData: false,
                contentType: false,
                data: data,
                success: function(response) {
                    
                    jQuery('#rt').empty();

                    jQuery('#rt').append(new Option("== Pilih RT ==", ""));

                    jQuery.each(response, function (id, name) {
                        jQuery('#rt').append(new Option(name, id))
                    })

                    jQuery("#rt").val(rt_id);
                },
                error: function(e) {
                    console.log(e);
                    swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                }
            });
    }
</script>
@endsection