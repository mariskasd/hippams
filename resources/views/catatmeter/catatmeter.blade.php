@extends('layouts.app')
@section('content')
<style>
    .detail-meteran{
        font-size:14px;
    }
    .link-meteran{
        color: #2596be;
        text-decoration:underline #2596be;
        cursor: pointer;
    }
</style>
<div>
    <div class="alert alert-secondary mx-4" role="alert">
       <p class="text-white text-xs mb-1">Anda dapat melakukan pencatatan meteran pada halaman ini</p>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-4">
                <div class="card-header pb-0">
                    <form  method="GET" action="{{ url('catat-meter') }}" enctype="multipart/form-data">
                         @csrf
                                 <div class="row">
                                        @if(Auth::user()->role != "Pelanggan")
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="name" class="form-control-label text-sm">RT<sup class="text-danger">*</sup></label>
                                                    <select name="rt" id="rt" class="form-control">
                                                        <option value="">== Pilih RT ==</option>

                                                        @if($idrt == "All")
                                                            <option value="All" selected>Semua</option>
                                                            @foreach ($rt as $id => $nama_rt)
                                                                <option value="{{ $id }}">{{ $nama_rt }}</option>
                                                            @endforeach
                                                        @else
                                                            <option value="All">Semua</option>
                                                            @foreach ($rt as $id => $nama_rt)
                                                                @if($idrt == $id)
                                                                    <option value="{{ $id }}" selected>{{ $nama_rt }}</option>
                                                                @else
                                                                     <option value="{{ $id }}">{{ $nama_rt }}</option>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        @endif
                                        
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="example-date-input" class="form-control-label text-sm">Bulan<sup class="text-danger">*</sup></label>
                                                <input class="form-control" type="month"  id="start_date" name="start_date" value="{{$startDate}}" max="{{ now()->toDateString() }}">
                                            </div>
                                        </div>
                                        
                                </div>
                                <div class="row mt-3">
                                    <div class="d-flex flex-row justify-content-between float-right">
                                        <div>
                                            <h5 class="mb-0"></h5>
                                        </div>
                                        <div>
                                            <button type="submit" class="btn bg-gradient-primary btn-sm mb-4" onclick="return validateSearch()" >Filter</button>
                                            @if(Auth::user()->role == "Admin")
                                                <button type="button" class="btn bg-gradient-primary btn-sm mb-4" onclick="return exportExcel()" >Download</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card2 pt-4 mb-4 mx-4">
                <div class="card-header pb-0">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h4 class="mb-0">Daftar Catatan Meteran</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-2">
                        <table class="table align-items-center mb-0 ms-2" id="tblCatat">
                            <thead>
                                <tr>
                                   <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        ID Meteran
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Pemilik
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Petugas
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Jumlah
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Total Pemakaian
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Keterangan
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Tanggal
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Status
                                    </th>
                                    <th class="text-center text-uppercase  text-sm font-weight-bolder ">
                                        Aksi
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pencatatanMeteran as $item)
                                <tr>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0 link-meteran" onclick="detailMeteran('{{$item}}')"> {{$item->id_meteran}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->pelanggan}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->petugas}} - {{$item->id_petugas}}</p>
                                    </td>
                                    <td>
                                        @if($item->id != null)
                                            <p class="text-xs font-weight-bold mb-0">{{$item->total_meteran}}</p>
                                        @else
                                            <p class="text-xs font-weight-bold mb-0">0</p>
                                        @endif
                                    </td>
                                    <td>
                                        @if($item->id != null)
                                            <p class="text-xs font-weight-bold mb-0">{{$item->terpakai}}</p>
                                        @else
                                            <p class="text-xs font-weight-bold mb-0">0</p>
                                        @endif
                                    </td>
                                    <td>
                                        @if($item->id != null)
                                            <p class="text-xs font-weight-bold mb-0">{{$item->keterangan}}</p>
                                        @else
                                            <p class="text-xs font-weight-bold mb-0">-</p>
                                        @endif
                                       
                                    </td>
                                    <td>
                                        @if($item->id != null)
                                            <p class="text-xs font-weight-bold mb-0">{{\Carbon\Carbon::parse($item->tanggal_pencatatan)->translatedFormat('d-m-Y H:i:s')}}</p>
                                        @else
                                            <p class="text-xs font-weight-bold mb-0">-</p>
                                        @endif
                                       
                                    </td>
                                    <td>
                                        @if($item->id != null)
                                            <p class="text-xs font-weight-bold mb-0"><b style="color:#4b912f;">SUDAH DICATAT<b></p>
                                        @else
                                            <p class="text-xs font-weight-bold mb-0"><b style="color:#FF0000;">BELUM DICATAT<b></p>
                                        @endif
                                       
                                    </td>
                                    @if(Auth::user()->role != "Pelanggan")
                                    <td class="text-center">
                                        @if($item->id != null)
                                            @if(Auth::user()->role == "Lapangan")
                                                @if($item->id_pembayaran == null)
                                                    <a href="{{ route('view-catat-meter', $item->id,'id') }}" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Edit" >
                                                        <i class="fas fa-user-edit text-secondary"></i>
                                                    </a>
                                                @endif
                                            @else
                                                <a href="{{ route('view-catat-meter', $item->id,'id') }}" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Edit" >
                                                    <i class="fas fa-user-edit text-secondary"></i>
                                                </a>
                                            @endif
                                        @else
                                            <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Add" onclick="addModal(null,'{{ $item->id_meteran }}','{{ $item->id_petugas }}' )">
                                                <i class="fas fa-user-plus text-secondary"></i>
                                            </a>
                                        @endif
                                    </td>
                                    @else
                                        @if($item->id != null)
                                            <td class="text-center">
                                                <a href="{{ route('view-catat-meter', $item->id,'id') }}" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Detail" >
                                                    <i class="fas fa-eye text-secondary"></i>
                                                </a>
                                            </td>
                                        @endif
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalMeteran" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Detail Meteran</h5>
                    <button type="button" class="btn close" onclick="detailMeteran('','close')">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row detail-meteran">
                        <div class="col-md-2">
                            <b>Wilayah</b>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-9">
                            <span id="wilMet"></span>
                        </div>
                    </div>
                    <div class="row detail-meteran">
                        <div class="col-md-2">
                            <b>Lokasi</b>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-9">
                            <span id="locMet"></span>
                        </div>
                    </div>
                    <div class="row detail-meteran">
                        <div class="col-md-2">
                            <b>Catatan</b>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-9">
                            <span id="catMet"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form  method="POST" id="add_meteran_form" action="{{ url('add-catat-meter') }}" enctype="multipart/form-data">
               @csrf
                <div class="modal fade" id="modalCatat" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Meteran Pelanggan</h5>
                                    <button type="button" class="btn close" onclick="addModal('close')">
                                        <span aria-hidden="true">X</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name" class="form-control-label text-sm">Meteran<sup class="text-danger">*</sup></label>
                                                <input name="meteran_id" id="meteran_id" class="form-control" type="text" readonly/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name" class="form-control-label text-sm">Pemilik<sup class="text-danger">*</sup></label>
                                                <input id="pemilik" name="pemilik" class="form-control" type="text" readonly >
                                                <input id="pelanggan_id" name="pelanggan_id" class="form-control" type="hidden" readonly >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name" class="form-control-label text-sm">Petugas<sup class="text-danger">*</sup></label>
                                                <input name="petugas_id" id="petugas_id" class="form-control" type="text" readonly/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name" class="form-control-label text-sm">Status Meteran</label><br>
                                                <div class="ms-2"><input type="checkbox" id="isNew" name="isNew" value="false">
                                                <label for="vehicle1"> Baru/Rusak</label></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name" class="form-control-label text-sm">Total Meteran<sup class="text-danger">*</sup></label>
                                                <input id="total_meteran" name="total_meteran" class="form-control" type="text" placeholder="Masukkan Total Meteran" required onkeypress="return validate(event)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name" class="form-control-label text-sm">Keterangan<sup class="text-danger">*</sup></label>
                                                <textarea id="keterangan" name="keterangan" class="form-control" type="text" placeholder="Masukan Keterangan" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name" class="form-control-label text-sm">Foto Meteran<sup class="text-danger">*</sup></label>
                                                <input id="image" name="image" class="form-control" type="file" required>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary" onclick="return validateRequired(event)">Simpan</wbutton>
                                </div>
                            </div>
                    
                    </div>
                </div>
    </form>


</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        $.ajaxSetup({
         headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

        $('#tblCatat').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
        });

        var now = new Date();
        var dateFormating = [
        now.getFullYear(),
        '-',
        now.getMonth() + 1 <10? '0'+ (now.getMonth()+1) : now.getMonth() +1].join('');

        let dateInput = document.getElementById("start_date");
        dateInput.max = dateFormating;

    });

    function detailMeteran(val,type){
        if(type == 'close'){
            jQuery("#modalMeteran").modal('hide');
        }else{
            let obj = JSON.parse(val)
            jQuery("#modalMeteran").modal('show');
            jQuery("#wilMet").text(obj.nama_wilayah);
            jQuery("#locMet").text(obj.lokasi + ', RT ' + obj.nama_rt + '/RW ' + obj.nama_rw + ', Dusun ' + obj.nama_dusun);
            jQuery("#catMet").text(obj.catatan);
        }
    }

    function validateSearch(){
        let startDate = jQuery("#start_date").val();
        // let endDate = jQuery("#end_date").val();

        if ((startDate == "" || startDate == null)) {

            swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");

            return false;
        }

        if(endDate < startDate){
            swal.fire("Gagal !", "Tanggal Selesai harus Lebih Besar dari Tanggal Mulai", "error");

            return false;
        }
    }

    function exportExcel(){
        let rt_id = jQuery("#rt").val();
        let date = jQuery("#start_date").val();
        window.location="/catat-meter/export?rt="+ rt_id + "&date=" + date;
    }

    function validate(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode > 31 && charCode != 46 && (charCode < 48 || charCode > 57))
        
        return false;
        
    }

    var idEdit = 0;

    function addModal(close, data, dataPetugas) {
        if (close) {
            idEdit = 0;
            jQuery("#meteran_id").val("");
            jQuery("#petugas_id").val("");
            jQuery("#total_meteran").val("");
            jQuery("#keterangan").val("");
            
            jQuery("#modalCatat").modal('hide');
        } else {
            idEdit = 0;
            jQuery("#modalCatat").modal('show');
            if (data) {

                //let edit = JSON.parse(data);
                //console.log(edit);
                jQuery("#meteran_id").val(data);
                jQuery("#petugas_id").val(dataPetugas);
                getPelanggan();
            }else{
                jQuery("#meteran_id").val("");
                jQuery("#petugas_id").val("");
                jQuery("#tanggal_pencatatan").val("");
                jQuery("#total_meteran").val("");
                jQuery("#keterangan").val("");
            }
        }
    }

    function validateRequired(event){
        let meteran_id = jQuery("#meteran_id").val();
        let pelanggan_id = jQuery("#pelanggan_id").val();
        let petugas_id = jQuery("#petugas_id").val();
        let total_meteran  = jQuery("#total_meteran").val();
        let keterangan = jQuery("#keterangan").val();
        let file = jQuery("#image").val();
        let newMeteran = jQuery("#isNew").is(':checked');
        console.log(newMeteran)

        if((meteran_id == null || meteran_id == "") || (pelanggan_id == null || pelanggan_id == "") || 
        (petugas_id == null || petugas_id == "") || (total_meteran == null || total_meteran == "") || 
        (keterangan == null || keterangan == "")|| (file == null || file == "")){
            swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");
            return false;
        }else{
            if(newMeteran == false){
                let url = '{{route("catat-meter-check")}}';
                var data = new FormData();
                data.append("total_meteran", total_meteran);
                data.append("meteran_id", meteran_id);
                event.preventDefault();
                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(data) {
                        if(data.message  !== "success"){
                            swal.fire("Gagal !", data.message, "error");
                            event.preventDefault();
                            return false;
                        }else{
                            document.getElementById("add_meteran_form").submit();
                            return true;
                        }
                    },
                    error: function(e) {
                        console.log(e);
                        swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                        event.preventDefault();
                        return false;
                    }
                });
            }
        }
    }

    function getPelanggan(){
        // jQuery('#meteran_id').on('change', function() {
            let url = '{{route("getPelanggan")}}';
            let meteran = jQuery("#meteran_id").val();

            console.log(meteran);

            var data = new FormData();
            data.append("meteran", meteran);

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                data:data,
                processData: false,
                contentType: false,
                success: function(response) {

                    jQuery("#pemilik").val(response.nama);
                    jQuery("#pelanggan_id").val(response.id_pelanggan);
                    
                },
                error: function(e) {
                    console.log(e);
                }
            });
        // });
    }

    
</script>
@endsection