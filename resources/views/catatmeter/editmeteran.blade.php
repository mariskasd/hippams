@extends('layouts.app')
@section('content')
<div>
    <div class="alert alert-secondary mx-4" role="alert">
       <p class="text-white text-xs mb-1">Anda dapat melakukan pencatatan meteran pada halaman ini</p>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-4">
                <div class="card-body px-0 pt-0 pb-2">
                <form  method="POST" id="edit_meteran_form" action="{{ url('edit-catat-meter') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalLongTitle">Pecatatan Meteran</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Meteran<sup class="text-danger">*</sup></label>
                                            <input id="meteran_id" name="meteran_id" class="form-control" type="text" value="{{$pecatatanMeteran->meteran_id}}" readonly >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Pemilik<sup class="text-danger">*</sup></label>
                                            <input id="pemilik" name="pemilik" class="form-control" type="text" readonly >
                                            <input id="pelanggan_id" name="pelanggan_id" class="form-control" type="hidden" readonly >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Petugas<sup class="text-danger">*</sup></label>
                                            <input id="petugas_id" name="petugas_id" class="form-control" type="text" value="{{$pecatatanMeteran->petugas_meter}}" readonly >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="example-date-input" class="form-control-label text-sm">Tanggal Pencatatan<sup class="text-danger">*</sup></label>
                                            <input class="form-control" type="datetime" value="{{$pecatatanMeteran->tanggal_pencatatan}}" id="tanggal_pencatatan" readonly name="tanggal_pencatatan">
                                        </div>
                                    </div>
                                    @if(Auth::user()->role != "Pelanggan")
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="name" class="form-control-label text-sm">Status Meteran</label><br>
                                                    <div class="ms-2"><input type="checkbox" id="isNew" name="isNew" value="false">
                                                    <label for="vehicle1"> Baru/Rusak</label></div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for="name" class="form-control-label text-sm">Total Meteran<sup class="text-danger">*</sup></label>
                                                    @if(Auth::user()->role != "Pelanggan")
                                                        <input id="total_meteran" name="total_meteran" class="form-control" type="text" placeholder="Masukkan Total Meteran" value="{{$pecatatanMeteran->total_meteran}}" onkeypress="return validate(event)" >
                                                    @else
                                                        <input id="total_meteran" name="total_meteran" class="form-control" type="text" placeholder="Masukkan Total Meteran" value="{{$pecatatanMeteran->total_meteran}}" disabled >
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Total Meteran<sup class="text-danger">*</sup></label>
                                            @if(Auth::user()->role != "Pelanggan")
                                                <input id="total_meteran" name="total_meteran" class="form-control" type="text" placeholder="Masukkan Total Meteran" value="{{$pecatatanMeteran->total_meteran}}" onkeypress="return validate(event)" >
                                            @else
                                                <input id="total_meteran" name="total_meteran" class="form-control" type="text" placeholder="Masukkan Total Meteran" value="{{$pecatatanMeteran->total_meteran}}" disabled >
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Keterangan<sup class="text-danger">*</sup></label>
                                            <textarea id="keterangan" name="keterangan" class="form-control" type="text" placeholder="Masukan Keterangan" rows="6" readonly >{{$pecatatanMeteran->keterangan}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Foto Meteran<sup class="text-danger">*</sup></label>
                                            <br>
                                            <img src="../storage/data_meteran/{{$pecatatanMeteran->foto}}" alt="..." class="w-50 border-radius-sm shadow-sm" >
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="id" id="id" value="{{$pecatatanMeteran->id}}">
                            </div>
                            <div class="modal-footer">
                                <!-- <button type="button" class="btn btn-primary" onclick="submit()">Simpan</wbutton> -->
                                @if((Auth::user()->role == "Admin" && $pecatatanMeteran->status_bayar == 0 )|| (Auth::user()->role == "Lapangan" && $pecatatanMeteran->id_pembayaran == null))
                                <button type="submit" class="btn btn-primary" onclick="return validateRequired(event)">Simpan</wbutton>
                                @endif
                            </div>
                        </div>
                </form>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<!-- https://code.jquery.com/jquery-3.5.1.js -->

<!-- https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js -->

<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        $.ajaxSetup({
         headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

        $('#tblUser').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
        });

        getPelanggan();

    });

    function validateRequired(event){
        let pencatatan = {!! $pecatatanMeteran->toJson() !!};
        let totalMeteran = jQuery("#total_meteran").val();
        let newMeteran = jQuery("#isNew").is(':checked');

        if (totalMeteran == "" || totalMeteran == null) {

            swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");

            return false;
        }else{
            if(newMeteran == false){
                let url = '{{route("catat-meter-check")}}';
                var data = new FormData();
                data.append("total_meteran", totalMeteran);
                data.append("meteran_id", pencatatan.meteran_id);
                event.preventDefault();
                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(data) {
                        if(data.message  !== "success"){
                            swal.fire("Gagal !", data.message, "error");
                            event.preventDefault();
                            return false;
                        }else{
                            document.getElementById("edit_meteran_form").submit();
                            return true;
                        }
                    },
                    error: function(e) {
                        console.log(e);
                        swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                        event.preventDefault();
                        return false;
                    }
                });
            }
        }
    }

    function validate(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode > 31 && charCode != 46 && (charCode < 48 || charCode > 57))
        
        return false;
        
    }

    function getPelanggan(){
        // jQuery('#meteran_id').on('change', function() {
            let url = '{{route("getPelanggan")}}';
            let meteran = jQuery("#meteran_id").val();

            console.log(meteran);

            var data = new FormData();
            data.append("meteran", meteran);

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                data:data,
                processData: false,
                contentType: false,
                success: function(response) {

                    jQuery("#pemilik").val(response.nama);
                    jQuery("#pelanggan_id").val(response.id_pelanggan);
                    
                },
                error: function(e) {
                    console.log(e);
                }
            });
        // });
    }
    
    // $(document).ready(function() {
    // $.noConflict();
    //     $("#tblUser").Datatable();
    //     console.log("tes");
    // });
</script>
@endsection

<!-- @push('js') -->

<!-- @endpush -->