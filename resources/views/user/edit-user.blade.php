@extends('layouts.app')
@section('content')
<div class="container-fluid py-4">
    <div class="col mt-4">
        <div class="card h-100">
            <div class="card-header pb-0 px-3">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="mb-0">Edit Karyawan</h6>
                    </div>
                    <!-- <div class="col-md-6 d-flex justify-content-end align-items-center">
                        <i class="far fa-calendar-alt me-2"></i>
                        <small>23 - 30 March 2020</small>
                    </div> -->
                </div>
            </div>
            <div class="card-body pt-4 p-5">
                <form action="" method="" id="form-user" role="form text-left">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="employeenumber" class="form-control-label">No Karyawan</label>
                                <input id="employeenumber" name="employeenumber" class="form-control" type="text" placeholder="Masukkan No Karyawan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Nama Lengkap</label>
                                <input id="name" name="name" class="form-control" type="text" placeholder="Masukkan Nama Lengkap Karyawan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="position" class="form-control-label">Jabatan</label>
                                <input id="position" name="position" class="form-control" type="text" placeholder="Masukkan Jabatan Karyawan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email" class="form-control-label">Email</label>
                                <input id="email" class="form-control" type="text" placeholder="Masukkan Email Karyawan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone" class="form-control-label">Telepon</label>
                                <input id="phone" class="form-control" type="text" placeholder="Masukkan Telepon Karyawan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address" class="form-control-label">Alamat</label>
                                <input id="address" class="form-control" type="text" placeholder="Masukkan Alamat Karyawan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="npwp" class="form-control-label">NPWP</label>
                                <input id="npwp" class="form-control" type="text" placeholder="Masukkan NPWP Karyawan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="salary" class="form-control-label">Gaji Pokok</label>
                                <input id="salary" class="form-control" type="text" placeholder="Masukkan Gaji Karyawan">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h6>Potongan</h6>
                    <div class="row">
                        <div class="col-md-4">
                            <select class="form-control" name="cut" id="cut">
                                <option value="">--Pilih Potongan--</option>
                                <option value="BPJS">BPJS</option>
                                <option value="BPJSTK">BPJSTK</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input id="nominal" name="nominal" class="form-control" type="text" placeholder="Masukkan Nominal Potongan">
                        </div>
                        <div class="col-md-2">
                            <div class="form-check form-switch ps-0">
                                <input class="form-check-input ms-auto" type="checkbox" id="flexSwitchCheckDefault">
                                <label class="form-check-label text-body ms-3 text-truncate mb-0" for="flexSwitchCheckDefault">Persen</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-warning btn-sm">Hapus</button>
                        </div>
                        <button type="button" class="btn btn-sm ms-2 mt-3 w-80">+ Tambah</button>
                    </div>
                    <hr>
                    <h6>Tunjangan</h6>
                    <div class="row">
                        <div class="col-md-4">
                            <select class="form-control" name="allowance" id="allowance">
                                <option value="">--Pilih Tunjangan--</option>
                                <option value="Tunjangan Anak">Tunjangan Anak</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input id="nominal" name="nominal" class="form-control" type="text" placeholder="Masukkan Nominal Tunjangan">
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-warning btn-sm">Hapus</button>
                        </div>
                        <button type="button" class="btn btn-sm ms-2 mt-3 w-65">+ Tambah</button>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-end">
                        <button type="button" type="submit" class="btn bg-gradient-dark btn-md w-100 mt-4 mb-4">{{ 'SIMPAN' }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection