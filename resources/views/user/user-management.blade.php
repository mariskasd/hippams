@extends('layouts.app')
@section('content')
<div>
    <div class="alert alert-secondary mx-4" role="alert">
        <p class="text-white text-xs mb-1">Anda dapat melakukan manajemen data karyawan pada halaman ini</p>
        <p class="text-white text-xs mb-1">Maksimal jumlah karyawan : {{$packages->limited_employee == 0 ? 'Tidak terbatas' : $packages->limited_employee}} (Paket {{$packages->name}})</p>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-4">
                <div class="card-header pb-0">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h5 class="mb-0"></h5>
                        </div>
                        @if($isFull == false)
                            <button class="btn bg-gradient-primary btn-sm mb-4" type="button" onclick="addModal()">+&nbsp; Tambah</button>
                        @endif
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-2">
                        <table class="table align-items-center mb-0" id="tblUser">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        ID Karyawan
                                    </th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        NPWP
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Nama
                                    </th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Jabatan
                                    </th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Email
                                    </th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Gaji/bulan
                                    </th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Aksi
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employee as $item)
                                <tr>
                                    <td class="ps-4">
                                        <p class="text-xs font-weight-bold mb-0">{{$item->employee_number}}</p>
                                    </td>
                                    <td class="text-center">
                                        <p class="text-xs font-weight-bold mb-0">{{$item->npwp}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->name}}</p>
                                    </td>
                                    <td class="text-center">
                                        <p class="text-xs font-weight-bold mb-0">{{$item->position}}</p>
                                    </td>
                                    <td class="text-center">
                                        <p class="text-xs font-weight-bold mb-0">{{$item->email}}</p>
                                    </td>
                                    <td class="text-center">
                                        <span class="text-secondary text-xs font-weight-bold">Rp {{$item->salary}}</span>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Edit" onclick="addModal(null,'{{$item}}')">
                                            <i class="fas fa-user-edit text-secondary"></i>
                                        </a>
                                        <a href="{{route('potongankaryawan', ['id'=>$item->id])}}" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Potongan">
                                            <i class="fas fa-cut text-secondary"></i>
                                        </a>
                                        <a href="{{route('tunjangankaryawan', ['id'=>$item->id])}}"href="{{route('potongankaryawan', ['id'=>$item->id])}}" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Tunjangan">
                                            <i class="fas fa-download text-secondary"></i>
                                        </a>
                                        <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Hapus" onclick="deleteData('{{$item->id}}')">
                                            <i class="cursor-pointer fas fa-trash text-secondary"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Karyawan</h5>
                    <button type="button" class="btn close" onclick="addModal('close')">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="employeenumber" class="form-control-label">No Karyawan<sup class="text-danger">*</sup></label>
                                <input id="employeenumber" name="employeenumber" class="form-control" type="text" placeholder="Masukkan No Karyawan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Nama Lengkap<sup class="text-danger">*</sup></label>
                                <input id="name" name="name" class="form-control" type="text" placeholder="Masukkan Nama Lengkap Karyawan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="position" class="form-control-label">Jabatan<sup class="text-danger">*</sup></label>
                                <input id="position" name="position" class="form-control" type="text" placeholder="Masukkan Jabatan Karyawan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email" class="form-control-label">Email<sup class="text-danger">*</sup></label>
                                <input id="email" class="form-control" type="text" placeholder="Masukkan Email Karyawan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address" class="form-control-label">Alamat<sup class="text-danger">*</sup></label>
                                <input id="address" class="form-control" type="text" placeholder="Masukkan Alamat Karyawan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="npwp" class="form-control-label">NPWP</label>
                                <input id="npwp" class="form-control" type="text" placeholder="Masukkan NPWP Karyawan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="salary" class="form-control-label">Gaji Pokok<sup class="text-danger">*</sup></label>
                                <input id="salary" class="form-control" type="text" placeholder="Masukkan Gaji Karyawan">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submit()">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<!-- https://code.jquery.com/jquery-3.5.1.js -->

<!-- https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js -->

<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        $('#tblUser').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
        });
    });

    var idEdit = 0;

    function addModal(close, data) {
        if (close) {
            idEdit = 0;
            jQuery("#employeenumber").val("");
            jQuery("#name").val("");
            jQuery("#position").val("");
            jQuery("#email").val("");
            jQuery("#address").val("");
            jQuery("#npwp").val("");
            jQuery("#salary").val("");
            jQuery("#modalUser").modal('hide');
        } else {
            idEdit = 0;
            jQuery("#modalUser").modal('show');
            if (data) {
                let edit = JSON.parse(data);
                jQuery("#employeenumber").val(edit.employee_number);
                jQuery("#name").val(edit.name);
                jQuery("#position").val(edit.position);
                jQuery("#email").val(edit.email);
                jQuery("#address").val(edit.address);
                jQuery("#npwp").val(edit.npwp);
                jQuery("#salary").val(edit.salary);
                idEdit = edit.id;
            }
        }
    }

    function submit() {
        let names = jQuery("#name").val();
        let employee_number = jQuery("#employeenumber").val();
        let position = jQuery("#position").val();
        let email = jQuery("#email").val();
        let phone = jQuery("#phone").val();
        let address = jQuery("#address").val();
        let npwp = jQuery("#npwp").val();
        let salary = jQuery("#salary").val();
        if ((names == "" || names == null) || (employee_number == "" || employee_number == null) ||
            (position == "" || position == null) || (email == "" || email == null) ||
            (address == "" || address == null) || (salary == "" || salary == null)) {
            swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");
        } else {
            var data = new FormData();
            data.append("name", names);
            data.append("email", email);
            data.append("employee_number", employee_number);
            data.append("position", position);
            data.append("address", address);
            data.append("npwp", npwp);
            data.append("salary", salary);

            let url = '{{route("newkaryawan")}}';
            if (idEdit != 0) {
                url = '{{route("editkaryawan" , ":id")}}';
                url = url.replace(":id", idEdit);
            }

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                processData: false,
                contentType: false,
                data: data,
                success: function(data) {
                    let message = "Data Berhasil Disimpan!";
                    if(data.message  !== "success"){
                        message = "Data Berhasil Disimpan \t (" + data.message + ")";
                    }
                    swal.fire("Sukses!", message, "success").then((value) => {
                        location.reload();
                    });
                },
                error: function(e) {
                    swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                }
            });
        }
    }

    function deleteData(idData) {
        swal.fire({
            text: "Apakah anda yakin akan menghapus data ini?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.isConfirmed) {
                let url = '{{route("hapuskaryawan" , ":id")}}';
                url = url.replace(":id", idData);

                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        swal.fire({
                            title: "Sukses!",
                            text: "Data Berhasil Dihapus!",
                            icon: "success"
                        }).then((result) => {
                            location.reload();
                        });
                    },
                    error: function(e) {
                        swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                    }
                });
            }
        });
    }
    // $(document).ready(function() {
    // $.noConflict();
    //     $("#tblUser").Datatable();
    //     console.log("tes");
    // });
</script>
@endsection

<!-- @push('js') -->

<!-- @endpush -->