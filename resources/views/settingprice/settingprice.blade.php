@extends('layouts.app')
@section('content')
<div>
    <div class="alert alert-secondary mx-4" role="alert">
       <p class="text-white text-xs mb-1">Anda dapat melakukan edit harga pada halaman ini</p>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-4">
                <div class="card-body px-0 pt-0 pb-2">
                <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalLongTitle">Pengaturan Harga</h4>
                            </div>
                            <div class="modal-body">
                               <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Harga per m<sup>3</sup> pemakaian sampai dengan 30m<sup>3</sup><sup class="text-danger">*</sup></label>
                                            <input id="min_harga" name="min_harga" class="form-control" type="text" placeholder="Masukkan Harga" onkeypress="return validate(event)" value="{{$setting->min_harga}}"  >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Harga per m<sup>3</sup> pemakaian diatas 30m<sup>3</sup><sup class="text-danger">*</sup></label>
                                            <input id="max_harga" name="max_harga" class="form-control" type="text" placeholder="Masukkan Harga"  onkeypress="return validate(event)" value="{{$setting->max_harga}}"  >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Denda keterlambatan perhari<sup class="text-danger">*</sup></label>
                                            <input id="min_denda" name="min_denda" class="form-control" type="text" placeholder="Masukkan Denda" onkeypress="return validate(event)" value="{{$setting->min_denda}}" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label text-sm">Denda keterlambatan perhari (max dalam 7 hari)<sup class="text-danger">*</sup></label>
                                            <input id="max_denda" name="max_denda" class="form-control" type="text" placeholder="Masukkan Denda" onkeypress="return validate(event)" value="{{$setting->max_denda}}" >
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="id" id="id" value="{{$setting->id}}" >
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" onclick="submit()">Simpan</wbutton>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        
    });

    function validate(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode > 31 && charCode != 46 && (charCode < 48 || charCode >= 57))
        
        return false;
        
    }

    var idEdit = 0;

    function submit() {
        let minHarga = jQuery("#min_harga").val();
        let maxHarga = jQuery("#max_harga").val();
        let minDenda = jQuery("#min_denda").val();
        let maxDenda = jQuery("#max_denda").val();

        if (minHarga == "" || minHarga == null || maxHarga == "" || maxHarga == null || minDenda == "" || minDenda == null || maxDenda == "" || maxDenda == null) {

            swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");

            return false;
        }   else {
                idEdit = jQuery("#id").val();
                var data = new FormData();
                data.append("min_harga", minHarga);
                data.append("max_harga", maxHarga);
                data.append("min_denda", minDenda);
                data.append("max_denda", maxDenda);
                data.append("id", idEdit);

                let url = '{{route("new-price")}}';

                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    async: false,
                    cache: false,
                    processData: false,
                    contentType: false,
                    data: data,
                    success: function(data) {
                        let message = "Data Berhasil Disimpan!";
                        if(data.message  !== "success"){
                            message = "Data Berhasil Disimpan \t (" + data.message + ")";
                        }
                        swal.fire("Sukses!", message, "success").then((value) => {
                            location.reload();
                        });
                    },
                    error: function(e) {
                        console.log(e);
                        swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                    }
                });
            }
        
    }

    
</script>
@endsection
