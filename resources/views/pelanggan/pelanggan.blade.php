@extends('layouts.app')
@section('content')
<div>
    <div class="alert alert-secondary mx-4" role="alert">
        <p class="text-white text-xs mb-1">Anda dapat melakukan manajemen data Pelanggan pada halaman ini</p>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card mb-4 mx-4">
                <div class="card-header pb-0">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h4 class="mb-0">Daftar Pelanggan</h4>
                        </div>
                        <button class="btn bg-gradient-primary px-3 py-2" type="button" onclick="addModal()">+&nbsp; Tambah</button>
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-2">
                        <table class="table align-items-center mb-0 ms-2" id="tblUser">
                            <thead>
                                <tr>
                                   <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        ID Pelanggan
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        Nama
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        ALAMAT
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        RT
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        RW
                                    </th>
                                    <th class="text-uppercase  text-sm font-weight-bolder  ps-2">
                                        DUSUN
                                    </th>
                                    <th class="text-center text-uppercase  text-sm font-weight-bolder ">
                                        Aksi
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pelanggan as $item)
                                <tr>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->id_pelanggan}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->nama}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->alamat}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->nama_rt}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->nama_rw}}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$item->nama_dusun}}</p>
                                    </td>
                                    <td class="text-center">
                                    <a href="{{ route('meteran', $item->id_pelanggan,'id') }}" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Lihat Meteran" >
                                            <i class="fas fa-search text-secondary"></i>
                                        </a>
                                        <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Edit" onclick="addModal(null,'{{$item}}')">
                                            <i class="fas fa-user-edit text-secondary"></i>
                                        </a>
                                        <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Hapus" onclick="deleteData('{{$item->id_pelanggan}}')">
                                            <i class="cursor-pointer fas fa-trash text-secondary"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Pelanggan</h5>
                    <button type="button" class="btn close" onclick="addModal('close')">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" id="lblIdPelanggan" name="lblIdPelanggan" class="form-control-label">ID Pelanggan</label>
                                <input id="id_pelanggan" name="id_pelanggan" class="form-control" type="text" readonly >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Nama Pelanggan<sup class="text-danger">*</sup></label>
                                <input id="name" name="name" class="form-control" type="text" placeholder="Masukkan Nama Pelanggan" onkeypress="return validate(event)"  >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">No Telp<sup class="text-danger">*</sup></label>
                                <input id="no_telp" name="no_telp" class="form-control" type="text" placeholder="Masukkan No Telp" onkeypress="return validatePhone(event)"  >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Nama Dusun<sup class="text-danger">*</sup></label>
                                <select name="dusun" id="dusun" class="form-control">
                                    <option value="">== Pilih Dusun ==</option>
                                    @foreach ($dusun as $id => $nama_dusun)
                                        <option value="{{ $id }}">{{ $nama_dusun }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="form-control-label">RW<sup class="text-danger">*</sup></label>
                                <select name="rw" id="rw" class="form-control">
                                    <option value="">== Pilih RW ==</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="form-control-label">RT<sup class="text-danger">*</sup></label>
                                <select name="rt" id="rt" class="form-control">
                                    <option value="">== Pilih RT ==</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="form-control-label">Alamat<sup class="text-danger">*</sup></label>
                                <textarea id="alamat" name="alamat" class="form-control" type="text" placeholder="Masukkan Alamat" ></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submit()">Simpan</button>
                </div>
            </div>
        </div>
    </div>


</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<!-- https://code.jquery.com/jquery-3.5.1.js -->

<!-- https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js -->

<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($) {
        $.ajaxSetup({
         headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

        $('#tblUser').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
        });

        $('#dusun').on('change', function () {
            console.log(1);
            var data = new FormData();
            data.append("id", $(this).val());

            let url = '{{route("dropdown-rw")}}';

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                processData: false,
                contentType: false,
                data: data,
                success: function(response) {
                    console.log(response);
                    $('#rw').empty();
                    $('#rt').empty();

                    $('#rw').append(new Option("== Pilih RW ==", ""));
                    $('#rt').append(new Option("== Pilih RT ==", ""));

                    $.each(response, function (id, name) {
                        $('#rw').append(new Option(name, id))
                    })
                },
                error: function(e) {
                    console.log(e);
                    swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                }
            });

        });

        $('#rw').on('change', function () {
            var data = new FormData();
            data.append("id", $(this).val());

            let url = '{{route("dropdown-rt")}}';

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                processData: false,
                contentType: false,
                data: data,
                success: function(response) {
                    console.log(response);
                    $('#rt').empty();

                    $('#rt').append(new Option("== Pilih RT ==", ""));

                    $.each(response, function (id, name) {
                        $('#rt').append(new Option(name, id))
                    })
                },
                error: function(e) {
                    console.log(e);
                    swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                }
            });

        });

    });

    var idEdit = 0;

    function addModal(close, data) {
        if (close) {
            idEdit = 0;
            jQuery("#name").val("");
            jQuery("#no_telp").val("");
            jQuery("#alamat").val("");
            jQuery("#dusun").val("");

            jQuery('#rw').empty();
            jQuery('#rt').empty();
            
            jQuery("#modalUser").modal('hide');
        } else {
            idEdit = 0;
            jQuery("#modalUser").modal('show');
            if (data) {
                jQuery("#id_pelanggan").show();
                jQuery("#lblIdPelanggan").show();

                let edit = JSON.parse(data);
                jQuery("#name").val(edit.nama);
                jQuery("#no_telp").val(edit.no_telp);
                jQuery("#alamat").val(edit.alamat);
                jQuery("#dusun").val(edit.dusun_id);
                jQuery("#id_pelanggan").val(edit.id_pelanggan);

                bindingRW(edit.dusun_id,edit.rw_id);
                bindingRT(edit.rw_id,edit.rt_id);

               // jQuery("#email").attr("disabled", "disabled"); 

                idEdit = edit.id_pelanggan;
            }else{
                jQuery("#id_pelanggan").hide();
                jQuery("#lblIdPelanggan").hide();

                jQuery("#name").val("");
                jQuery("#no_telp").val("");
                jQuery("#alamat").val("");
                jQuery("#dusun").val("");

                jQuery('#rw').empty();
                jQuery('#rt').empty();
            }
        }
    }

    function validate(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode

        if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
        
        return false;
        
    }

    function validatePhone(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode > 31 && (charCode < 48 || charCode > 57))
        
        return false;
    }


    function submit() {
        let idPel = jQuery("#id_pelanggan").val();
        let names = jQuery("#name").val();
        let no_telp = jQuery("#no_telp").val();
        let alamat = jQuery("#alamat").val();
        let rt = jQuery("#rt").val();
        let rw = jQuery("#rw").val();

        let reg = /[0-9]/g;
        let result = names.match(reg);

        if (result != null) {
            swal.fire("Gagal !", "Nama Pelanggan Tidak Boleh Mengandung Angka", "error");
        }else{

            let reg = /[A-Za-z]/g;
            let result = no_telp.match(reg);

            if (result != null) {
                swal.fire("Gagal !", "No.Telp Tidak Boleh Mengandung Angka", "error");
            }else{

                if(idPel == "" || idPel == null){
                    if ((names == "" || names == null)  || (no_telp == "" || no_telp == null)
                    || (alamat == "" || alamat == null) || (rt == "" || rt == null)) {
                    swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");
                    } else {
                        var data = new FormData();
                        data.append("name", names);
                        data.append("no_telp", no_telp);
                        data.append("alamat", alamat);
                        data.append("rt", rt);
                        data.append("rw", rw);

                        let url = '{{route("newPelanggan")}}';
                        if (idEdit != 0) {
                            url = '{{route("editPelanggan" , ":id")}}';
                            url = url.replace(":id", idEdit);
                        }

                        console.log(url);

                        jQuery.ajax({
                            headers: {
                                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                            },
                            url: url,
                            method: "POST",
                            async: false,
                            cache: false,
                            processData: false,
                            contentType: false,
                            data: data,
                            success: function(data) {
                                console.log(data);
                                let message = "Data Berhasil Disimpan!";
                                if(data.message  !== "success"){
                                    swal.fire("Gagal !", data.message, "error").then((value) => {
                                    });
                                }else{
                                    swal.fire("Sukses!", message, "success").then((value) => {
                                        location.reload();
                                    });
                                }
                                
                            },
                            error: function(e) {
                                console.log(e);
                                swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                            }
                        });
                    }
                }else{
                    if ((names == "" || names == null)  || (no_telp == "" || no_telp == null)
                    || (alamat == "" || alamat == null) || (rt == "" || rt == null)) {
                    swal.fire("Gagal !", "Data bertanda(*) wajib diisi", "error");
                    } else {
                        var data = new FormData();
                        data.append("name", names);
                        data.append("no_telp", no_telp);
                        data.append("alamat", alamat);
                        data.append("rt", rt);
                        data.append("rw", rw);

                        let url = '{{route("newPelanggan")}}';
                        if (idEdit != 0) {
                            url = '{{route("editPelanggan" , ":id")}}';
                            url = url.replace(":id", idEdit);
                        }

                        console.log(url);

                        jQuery.ajax({
                            headers: {
                                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                            },
                            url: url,
                            method: "POST",
                            async: false,
                            cache: false,
                            processData: false,
                            contentType: false,
                            data: data,
                            success: function(data) {
                                console.log(data);
                                let message = "Data Berhasil Disimpan!";
                                if(data.message  !== "success"){
                                    swal.fire("Gagal !", data.message, "error").then((value) => {
                                    });
                                }else{
                                    swal.fire("Sukses!", message, "success").then((value) => {
                                        location.reload();
                                    });
                                }
                                
                            },
                            error: function(e) {
                                console.log(e);
                                swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                            }
                        });
                    }
                }
            }
        }
    }

    function deleteData(idData) {
        
        swal.fire({
            text: "Apakah anda yakin akan menghapus data ini?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.isConfirmed) {
                let url = '{{route("hapusPelanggan" , ":id")}}';
                url = url.replace(":id", idData);
                
                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        let message = "Data Berhasil Dihapus!";
                        if(data.message  !== "success"){
                            swal.fire("Gagal !", data.message, "error");
                        }else{
                            swal.fire("Sukses!", message, "success").then((value) => {
                                location.reload();
                            });
                        }
                    },
                    error: function(e) {
                        console.log(e);
                        swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                    }
                });
            }
        });
    }

    function bindingRW(id, rw_id){
            var data = new FormData();
            data.append("id", id);

            let url = '{{route("dropdown-rw")}}';

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                processData: false,
                contentType: false,
                data: data,
                success: function(response) {
                    console.log(response);

                    jQuery('#rw').empty();
                    jQuery('#rt').empty();

                    jQuery('#rw').append(new Option("== Pilih RW ==", ""));
                    jQuery('#rt').append(new Option("== Pilih RT ==", ""));

                    jQuery.each(response, function (id, name) {
                        jQuery('#rw').append(new Option(name, id))
                    })

                    jQuery("#rw").val(rw_id);
                },
                error: function(e) {
                    console.log(e);
                    swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                }
            });
    }

    function bindingRT(id,rt_id){
            var data = new FormData();
            data.append("id", id);

            let url = '{{route("dropdown-rt")}}';

            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                method: "POST",
                async: false,
                cache: false,
                processData: false,
                contentType: false,
                data: data,
                success: function(response) {
                    
                    jQuery('#rt').empty();

                    jQuery('#rt').append(new Option("== Pilih RT ==", ""));

                    jQuery.each(response, function (id, name) {
                        jQuery('#rt').append(new Option(name, id))
                    })

                    jQuery("#rt").val(rt_id);
                },
                error: function(e) {
                    console.log(e);
                    swal.fire("Gagal !", "Terjadi Kesalahan", "error");
                }
            });
    }
</script>
@endsection