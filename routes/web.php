<?php

use Illuminate\Support\Facades\Route;

use App\Http\Livewire\Auth\ForgotPassword;
use App\Http\Livewire\Auth\ResetPassword;
use App\Http\Livewire\Auth\SignUp;
use App\Http\Livewire\Auth\Login;

use App\Http\Livewire\LaravelExamples\UserManagement;

use App\Http\Controllers\LandingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LandingController::class, 'index']);
// Route::get('/', Login::class)->name('login');

Route::get('/sign-up', SignUp::class)->name('sign-up');
Route::get('/login', Login::class)->name('login');

Route::get('/login/forgot-password', ForgotPassword::class)->name('forgot-password');

Route::get('/reset-password/{id}', ResetPassword::class)->name('reset-password')->middleware('signed');

Route::middleware('auth')->group(function () {
    Route::get('/user-management', UserManagement::class)->name('user-management');

    //tambahan
    Route::get('/dusun', 'App\Http\Controllers\DusunController@index')->name('dusun');
    Route::post('/tambah-dusun', 'App\Http\Controllers\DusunController@add')->name('newDusun');
    Route::post('/edit-dusun/{id}', 'App\Http\Controllers\DusunController@edit')->name('editDusun');
    Route::post('/hapus-dusun/{id}', 'App\Http\Controllers\DusunController@delete')->name('hapusDusun');

    Route::get('/rw', 'App\Http\Controllers\RwController@index')->name('rw');
    Route::post('/tambah-rw', 'App\Http\Controllers\RwController@add')->name('newRw');
    Route::post('/edit-rw/{id}', 'App\Http\Controllers\RwController@edit')->name('editRw');
    Route::post('/hapus-rw/{id}', 'App\Http\Controllers\RwController@delete')->name('hapusRw');

    Route::get('/rt', 'App\Http\Controllers\RtController@index')->name('rt');
    Route::post('/tambah-rt', 'App\Http\Controllers\RtController@add')->name('newRt');
    Route::post('/edit-rt/{id}', 'App\Http\Controllers\RtController@edit')->name('editRt');
    Route::post('/hapus-rt/{id}', 'App\Http\Controllers\RtController@delete')->name('hapusRt');

    Route::get('/wilayah', 'App\Http\Controllers\WilayahController@index')->name('wilayah');
    Route::post('/tambah-wilayah', 'App\Http\Controllers\WilayahController@add')->name('newWilayah');
    Route::post('/edit-wilayah/{id}', 'App\Http\Controllers\WilayahController@edit')->name('editWilayah');
    Route::post('/hapus-wilayah/{id}', 'App\Http\Controllers\WilayahController@delete')->name('hapusWilayah');

    Route::get('/pengumuman', 'App\Http\Controllers\PengumumanController@index')->name('pengumuman');
    Route::post('/tambah-pengumuman', 'App\Http\Controllers\PengumumanController@add')->name('newPengumuman');
    Route::post('/edit-pengumuman/{id}', 'App\Http\Controllers\PengumumanController@edit')->name('editPengumuman');
    Route::post('/hapus-pengumuman/{id}', 'App\Http\Controllers\PengumumanController@delete')->name('hapusPengumuman');

    Route::get('/pemadaman', 'App\Http\Controllers\PemadamanController@index')->name('pemadaman');
    Route::post('/tambah-pemadaman', 'App\Http\Controllers\PemadamanController@add')->name('newPemadaman');
    Route::post('/edit-pemadaman/{id}', 'App\Http\Controllers\PemadamanController@edit')->name('editPemadaman');
    Route::post('/hapus-pemadaman/{id}', 'App\Http\Controllers\PemadamanController@delete')->name('hapusPemadaman');
    Route::get('/user-pemadaman', 'App\Http\Controllers\PemadamanController@pemadamanUser')->name('user-pemadaman');

    Route::get('/pelanggan', 'App\Http\Controllers\PelangganController@index')->name('pelanggan');
    Route::post('/tambah-pelanggan', 'App\Http\Controllers\PelangganController@add')->name('newPelanggan');
    Route::post('/edit-pelanggan/{id}', 'App\Http\Controllers\PelangganController@edit')->name('editPelanggan');
    Route::post('/hapus-pelanggan/{id}', 'App\Http\Controllers\PelangganController@delete')->name('hapusPelanggan');

    Route::post('/dropdown-rw', 'App\Http\Controllers\PelangganController@getDataRW')->name('dropdown-rw');
    Route::post('/dropdown-rt', 'App\Http\Controllers\PelangganController@getDataRT')->name('dropdown-rt');
    Route::post('/getPassword', 'App\Http\Controllers\PelangganController@getPassword')->name('getPassword');
    Route::post('/getPelanggan', 'App\Http\Controllers\CatatMeterController@getPelanggan')->name('getPelanggan');

    Route::get('/meteran/{id}', 'App\Http\Controllers\MeteranController@index')->name('meteran');
    Route::post('/tambah-meteran', 'App\Http\Controllers\MeteranController@add')->name('newMeteran');
    Route::post('/edit-meteran/{id}', 'App\Http\Controllers\MeteranController@edit')->name('editMeteran');
    Route::post('/hapus-meteran/{id}', 'App\Http\Controllers\MeteranController@delete')->name('hapusMeteran');

    Route::get('/wilayah-pemadaman/{id}', 'App\Http\Controllers\PemadamanController@viewWilayah')->name('wilayah-pemadaman');
    Route::post('/add-wilayah-pemadaman', 'App\Http\Controllers\PemadamanController@addWilayahPemadaman')->name('add-wilayah-pemadaman');
    Route::get('/edit-wilayah-pemadaman/{id}', 'App\Http\Controllers\PemadamanController@addWilayahPemadaman')->name('edit-wilayah-pemadaman');
    Route::post('/hapus-wilayah-pemadaman/{id}', 'App\Http\Controllers\PemadamanController@deleteWilayahPemadaman')->name('delete-wilayah-pemadaman');

    Route::get('/petugas', 'App\Http\Controllers\PetugasController@index')->name('petugas');
    Route::post('/tambah-petugas', 'App\Http\Controllers\PetugasController@add')->name('newPetugas');
    Route::post('/edit-petugas/{id}', 'App\Http\Controllers\PetugasController@edit')->name('editPetugas');
    Route::post('/hapus-petugas/{id}', 'App\Http\Controllers\PetugasController@delete')->name('hapusPetugas');

    Route::get('/wilayah-petugas/{id}', 'App\Http\Controllers\PetugasController@viewWilayahPetugas')->name('wilayah-petugas');
    Route::post('/add-wilayah-petugas', 'App\Http\Controllers\PetugasController@addWilayahPetugas')->name('add-wilayah-petugas');
    Route::get('/edit-wilayah-petugas/{id}', 'App\Http\Controllers\PetugasController@addWilayahPetugas')->name('edit-wilayah-petugas');
    Route::post('/hapus-wilayah-petugas/{id}', 'App\Http\Controllers\PetugasController@deleteWilayahPetugas')->name('delete-wilayah-petugas');

    Route::get('/catat-meter', 'App\Http\Controllers\CatatMeterController@index')->name('catat-meter');
    Route::post('/add-catat-meter', 'App\Http\Controllers\CatatMeterController@add')->name('add-catat-meter');
    Route::post('/edit-catat-meter', 'App\Http\Controllers\CatatMeterController@edit')->name('edit-catat-meter');
    Route::post('/hapus-catat-meter/{id}', 'App\Http\Controllers\CatatMeterController@delete')->name('hapus-catat-meter');
    Route::get('/view-catat-meter/{id}', 'App\Http\Controllers\CatatMeterController@editCatatMeter')->name('view-catat-meter');
    Route::get('/catat-meter/export', 'App\Http\Controllers\CatatMeterController@export_excel')->name('export-catat-meter');
    Route::post('/catat-meter-check', 'App\Http\Controllers\CatatMeterController@checkPrevMeteran')->name('catat-meter-check');
    Route::get('/update-pass-user', 'App\Http\Controllers\DashboardController@updatePassUser')->name('update-pass-user');

    Route::get('/komplain', 'App\Http\Controllers\KomplainController@index')->name('komplain');
    Route::post('/add-komplain', 'App\Http\Controllers\KomplainController@add')->name('add-komplain');
    Route::get('/feedback/{id}', 'App\Http\Controllers\KomplainController@feedback')->name('feedback');
    Route::get('/update-feedback-progress/{id}', 'App\Http\Controllers\KomplainController@updateOnProgress')->name('update-feedback-progress');
    Route::post('/add-feedback', 'App\Http\Controllers\KomplainController@addFeedback')->name('add-feedback');
    Route::post('/edit-feedback', 'App\Http\Controllers\KomplainController@editFeedback')->name('edit-feedback');

    Route::get('/pembayaran', 'App\Http\Controllers\PembayaranController@index')->name('pembayaran');
    // Route::post('/generate-tagihan/{id}', 'App\Http\Controllers\CatatMeterController@generateTagihan')->name('generate-tagihan');
    Route::get('/pembayaran/{id}', 'App\Http\Controllers\PembayaranController@edit')->name('edit-pembayaran');
    Route::post('/bayar', 'App\Http\Controllers\PembayaranController@bayarTagihan')->name('bayar');
    Route::get('/buat-pembayaran', 'App\Http\Controllers\PembayaranController@buatpembayaran')->name('buat-pembayaran');
    Route::post('/buat-tagihan', 'App\Http\Controllers\PembayaranController@generateTagihan')->name('buat-tagihan');
    Route::get('/pembayaran-data/export', 'App\Http\Controllers\PembayaranController@export_excel')->name('export-pembayaran');

    Route::get('/injectCatatMeteran', 'App\Http\Controllers\DashboardController@injectCatatMeteran')->name('injectCatatMeteran');
    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard');
    Route::get('/price', 'App\Http\Controllers\SettingPriceController@index')->name('price');
    Route::post('/new-price', 'App\Http\Controllers\SettingPriceController@add')->name('new-price');
    Route::post('/edit-price/{id}', 'App\Http\Controllers\SettingPriceController@edit')->name('edit-price');
    Route::post('/hapus-price/{id}', 'App\Http\Controllers\SettingPriceController@delete')->name('hapus-price');

});